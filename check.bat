@echo off
call "%~dp0\_tools\library\python\scripts\env_for_icons.bat"

call "%~dp0\_tools\library\python\python-3.11.1.amd64\python" "%~dp0.\_tools\library\import_csv_if_modified.py"
call "%~dp0\_tools\library\python\python-3.11.1.amd64\python" "%~dp0.\_tools\library\export_csv.py"
call "%~dp0\_tools\library\python\python-3.11.1.amd64\python" "%~dp0\_tools\library_tests\main.py"