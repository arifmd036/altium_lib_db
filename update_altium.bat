@echo off

set /p Build=<"%~dp0\_tools\library\_version.txt"

if exist "%~dp0\_tools\library\_version_now.txt" (
  set /p Now=<"%~dp0\_tools\library\_version_now.txt"
) else (
  set Now=0
)


if %Build% GTR %Now% (
  copy "%~dp0\_tools\library\_version.txt" "%~dp0\_tools\library\_version_now.txt"
  rmdir /q /s "%~dp0\_tools\library\python"
  %~dp0\setup.bat
) else (
  copy _export\__template_LibraryDatabase.MDB LibraryDatabase.MDB
  call "%~dp0\_tools\library\python\scripts\env_for_icons.bat"
  call "%~dp0\_tools\library\python\python-3.11.1.amd64\python" "%~dp0.\_tools\library\import_csv.py"
)