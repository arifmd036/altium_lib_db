# WSN AGH Altium Lib DB

## Setup guide:

- zainstalować git, MS Access oraz Alltium
- uruchomić skrypt `setup.bat` i upewnić się że wszystko jest w porządku (w razie błędów podrzucić log `%TEMP%\altiumdb_log.txt` do @ggajoch )

----

Warto zapoznać się/przerobić następujące instrukcje, aby zapoznać się z obsługą.
http://techdocs.altium.com/display/ADOH/Using+Components+Directly+from+Your+Company+Database
http://techdocs.altium.com/display/ADOH/Database+Library+Migration+Tools

Przydatne strony (oprócz dokumentacji online Altium):
http://blog.mbedded.ninja/electronics/general/altium/altium-tricks-and-standards
http://beta.ivc.no/wiki/index.php/Altium_Designer

## Struktura katalogów 
Schematy i footprinty trzymane są w osobnych katalogach:
- PCB
- SCH
A następnie łączone za pomocą bazy danych `LibraryDatabase.MDB`.

## Symbole

Katalog SCH zawiera symbole.

Dobrą praktyką jest naszkicowanie struktury wewnętrznej, ułatwiającej zobrazowanie schematu.

Konwencja oznaczeń symboli:

| **prefix pliku** | **opis** | **desygnator** |
|:---|:---|:---|
| ANT | antena | ANT? |
| BUZ| buzzer | BUZ? |
| C/CE/CT | kondensatory: ceramiczne/elektrolityczne/tantalowe | C? |
| CF | filtry ceramiczne | CF? |
| DS | dioda Schotkyego | D? |
| DZ | dioda Zenera | D? |
| DB | mostki prostownicze | D? |
| D | dioda | D? |
| EF | filtry EMI | EF? |
| ENC | enkodery | ENC? |
| F | bezpieczniki | F? |
| FB | Ferrite bead | FB? |
| GDT | gas discharge tube, iskiernik | GTD? |
| J | złącze żeńskie | J? |
| L | induktor, cewka, dławik | L?|
| LM | cokolwiek ze sprzężeniem indukcyjnym, transformato, balun, common mode | LM? |
|CORE| rdzenie(Ref:Własne elementy indukcyjne)|CORE?|
| MOD | moduł | MOD? |
| OC | transoptory | OC? |
| OL | LED | D? |
| P | złącze męskie  | P? |
| QNP | bipolar npn | Q? |
| QNS | Mosfet typu N z diodą schotkyego | Q? |
| QN | Mosfet typu N | Q? |
| QPN | bipolar pnp | Q? |
| QPS |  Mosfet typu P z diodą schotkyego | Q? |
| QP | Mosfet typu P | Q? |
| S | przyciski(switch) | S? |
| SHIELD | osłony EMI | SHIELD? |
| TR | tyrystory, diaki | TR? |
| TVS | transient voltage suppressor | TVS? |
| UP | mikroprocesory | U? |
| U | układy scalone, SAWy | U? |
| VL | Voltage regulator | U? |
| VS | Switching regulator jakikolwiek scalak kluczowany | U? |
| VR | Voltage reference |U?|
| X | kwarc | X? |
|RT_NTC| termistor NTC | RT? |
|RT_PTC| termistor PTC | RT? |
|MOV| warystor | MOV? |
|FR| fotorezystor | FR? |
|POT| potencjometr | P? |

### Rysowanie symbolu
Dla układów scalonych długość pinu powinna wynosić 200mils. 

Staramy się nie zostawiać pojedynczych pinów GND od dołu

<table>
  <tr>
    <td> ŹLE </td>
    <td> DOBRZE </td>
   </tr> 
   
  <tr>
    <td><img src="./img/wrong_GND.png" width = 300px></td>
    <td><img src="./img/good_GND.png" width = 300px ></td>
   </tr> 
</table>

Dla ujednolicenia schematów oznaczenie sprzężenie transformatorów umieszczamy w taki sposób:

![coupling](img/coupling.png)

### Wymagane parametry
Do każdego modelu schematycznego dodajemy 2 parametry o nazwach i wartościach :

|Name| Value|
|---|---|
|Value | *|
|SCH_Footprint|*|

Parametry umieszczamy tak, aby po rozwinięciu były czytelne i odznaczamy **Autoposition**

### Typy pinów
|||
|---|---|
|Diody| Passive|
|Tranzystory|Passive|
|GND|Power|
|Napięcie wejściowe stabilizatora/przetwornicy|Input|
|Napięcie wyjściowe stabilizatora/przetwornicy|Output|
|GPIO/ porty cyfrowe|Digital I/O|
|tranformatory|Passive|
|cewki|Passive|
|rezystory|Passive|
|kodensatory|Passive|
|załącza|Passive|
|Zasilanie IC|Input|
|NC/DNC jeśli nie da się ustalić|Passive|
|EPAD| zgodnie z przeznaczeniem|
|przełączniki, styki, przyciski, enkodery mechaniczne|Passive|

### Exposed PAD
Wyprowadzamy jako EP1,EP2... jako numer i nazywamy EPAD, bądź tak, jak opisuje to nota katalogowa (np. GND)


## Specyficzne ustalenia nazewnictwa symboli

### SAW
SAWy o jednej ilości pinów lądują w w jednej bibliotece SchLib (np. wszystkie 6-cio pinowe są w U_SAW6.SchLib).

U_SAW6_1_2 - 6 pinów, wejście na 1, wyjście na 2

U_SAW8_1_2wrt3 - 8 pinów, wejście single-ended na 1, wyjście różnicowe na 2 wrt. 3

U_SAW10_1wrt2_6wrt3 - 8 pinów, wejście równicowe 1 wrt 2, wyjście różnicowe 6 wrt. 3

### Transistors

Tranzystory mają zunifikowane symbole dla podstawowych typów.

`QN.Schlib` - tranzystor N MOSFET

itp - jak w tabeli wyżej

W każdym pliku schlib znajdują się różne warianty pinów:

QN_GDS - pin1-G; pin2-D; pin3-S

QN_GSD - pin1-G; pin2-S; pin3-D

Jeśli występuje tranzystor, który nie wpisuje się w powyższe, należy dodać symbol indywidualnie do niego np:

Library ref: QN_IRFS7430PBF_40V_195A

Part number: QN_IRFS7430PBF_40V_195A

Part number i Library ref zostają takie same. Po nazwie tranzystora dodawane jest napięcie dren źródło, prąd drenu, oraz obudowa.
Uzasadnienie jest takie, że łatwiej wyszukiwać w podręcznej wyszukiwarce Altiuma.


### Voltage regulators
Każdy symbol dla nowego elementu osobno. Najlepiej realizować poprzez kopiowanie istniejących.


### IC Other
Każdy symbol dla nowego elementu osobno. Najlepiej realizować poprzez kopiowanie istniejących.

Nazwa schematu jest najkrótsza, ale aby jednoznacznie określała element i jego obudowę.

Konkretny typ elementu specyfikujemy w `Part Number`.

### Diodes

Symbole są zunifikowane. (zwykłe - `D`, schottky'ego - `DS`).

Jeśli jest nietypowa dodawany jest dla niej indywidualny symbol np: `DS_BAT754S`

Dla zunifikowania footprintów następujące oznaczenia dla pinów są używane:

2 - Anoda (+)

1 - Katoda (-)


## Footprinty

**Footprinty nazywamy tak jak obudowa (według norm IPC, jeśli dotyczą), chyba że nie ma nazwy obudowy, wtedy jest ona przypisana do konkretnego elementu i nazwana jego symbolem.**

Jeśli używamy generatora footprintów to należy używać high-density.

Należy dodać rozmiary obudowy (X x Y).

Na przykład: QFN32_5x5

Jeśli dodany footprint do element THT, należy podać skrótową nazwę elementu, potem AXIAL, następnie odległość między padami, na końcu średnica pada.(w mm, wszystko oddzielone podkreślnikiem ) np. dla rezystora:
R_AXIAL_7.5_0.9



### Użycie warstw Mechanical

- Mechanical 1 - obrys płytki (można używać także warstwę Keep-Out, ale często stosujemy ją do odgradzania fragmentu płytki z elementów)
- Mechanical 2 - notatki dla producenta PCB (generowane w gerberach)
- Mechanical 3 - notatki na PCB, które nie będą przesyłane do producenta PCB (nie generowane w gerberach)
- Mechanical 11/12 - wymiary (top/bottom, dotyczy wymiarów płytki)
- Mechanical 13/14 - obrysy obudowy (bez nóżek i padów) i modele 3D (Component Body Information)
- Mechanical 15/16 - obrys całkowity elementu (z padami) i środek elementu
- Mechanical 17/18 - słowo kluczowe d(po to aby sobie po ułożeniu schematu zmienić na '.Designator') (dopasowane do obrysu z warstwy 13/14), apostrof przy słownie też ma być
- Mechanical 19/20 - słowo kluczowe -(' później zmieniamy na '.Comment) (dopasowane do obrysu z warstwy 13/14), apostrof przy słownie też ma być

### Generacja footprintu poprzez kreator

Altium posiada bardzo użyteczny generator IPC Compliant Footprint Wizard.
Pozwala on na utworzenie zgodnego z normami IPC footprintu wraz z warstwami mechanicznymi.
1. Gdy mamy otwarte okno edycji plików .PcbLib wybieramy: **Tools**->**IPC Compliant Footprint Wizard**
2. Wybieramy typ obudowy (jeśli nie ma dokładnie takiego jak potrzebujemy to wybieramy najbardziej podobny, który później zmodyfikujemy).
3. Uzupełniamy niezbędne informacje o obudowie (ilość nóżek, raster, tolerancje) - te informacje odczytujemy z noty katalogowej danego elementu.
4. Wybieramy "gęstość" czyli ile miejsca przeznaczamy na pady. Im większa gęstość tym pady mniejsze. Z reguły wybieramy "medium density". Dla dużych gęstości może wystąpić taka sytuacja, że pad na PCB jest mniejszy od padu na obudowie elementu
5. Szerokość obrysu Silkscreen na warstwie Overlay pozostawiamy równą 0.2mm
6. Zaznaczamy "Add Courtyard/Assembly/Component Body Information" i pozostawiamy domyślne obliczone wartości
7. Nadajemy nazwę utworzonemu modelowi wg szablonu:
    * **YYYY_ZZZZ_A.PcbLib**
    * **YYYY** - nazwa obudowy (np. 0603, DPAK, SOT23, MSOP16) Jeżeli footprint posiada kilka nazw, to podaje je się po podkreślnikiu( _ )
    * **ZZZZ** - wymiary zasadniczej części obudowy (bez nóżek) - XY - w mm (np. 10x10)
    * **A** - gęstość upakowania.
8. Na warstwie 17 dodajemy słowo kluczowe .Designator. Jeśli się nie mieści to zmniejszamy tekst pamiętając o proporcji wysokość/grubość = 5 (np. high 40, width 8)
9. Na warstwie 19 dodajemy słowo kluczowe .Comment
10. Jeśli posiadamy model 3D (plik .STEP) danego elementu mozemy go dodać poprzez opcję **Place**->**3D Body**.
	(tutorial: https://www.pcb-3d.com/knowledge-base/altium-import-3d-model-into-footprint/)

### W skrócie:
 - wybieramy typ: Generic STEP Model
 - Identyfikator pozostawiamy domyślny
 - Body Side: wybieramy wg właściwości danego elementu (w 99% przypadków pozostawiamy Top Side)
 - Layer: Mechanical 13
 - klikamy na przecisk **Embed STEP Model** i wybieramy plik .STEP
 - po dodaniu modelu niekiedy trzeba go obrócić i dostosować wysokość od podłoża wtedy klikamy 2x na model 3D i zmieniamy ostawienia Rotation i Standoff Height
 - Sprawdzić czy warstwy TOP solder i Top overlay nie nachodzą na siebie 

## Baza danych

W głównym katalogu znajduje się plik z bazą danych: `LibraryDatabase.MDB`.

Jeśli chcemy dodać nowy element dodajemy nowy wpis do bazy danych.

(WARNING) Po dodaniu elementu czasami trzeba w przeglądarce bibliotek kliknąć prawym i Refresh Library.

(WARNING) Przed zacommitowaniem należy zamknąć program MS Access.

Baza jest podzielona na tabele, gdzie każda tabela obrazuje inne typy elementów:
- Capacitors
- Connectors
- Crystals
- Diodes - diody prostownicze,zenera schotkyego, ograniczniki napięcia,TVSy
- IC Other
- Inductors - cewki, transformatory, koraliki ferrytowe
- Mechanical
- Microcontrollers
- Modules
- OpAmps
- Optoelectronics
- Passive Components - switche, bezpieczniki, iskierniki, termistory
- Resistors
- SAW
- Switches
- Transistors
- Triacs SCR
- Voltage regulators
- Switching regulators
- Voltage reference

### Opis kolumn wspólnych dla wszystkich tabel

*Part Number* - nazwa elementu, unikalna w ramach całej bazy danych

*Library Ref* - nazwa elementu schematycznego (wewnątrz `.SchLib`)

*Footprint Ref* - nazwa footprintu (wewnątrz `.PcbLib`)

*Value* - pojawi się na schemacie, zazwyczaj wartość lub typ elementu

*SCH_Footprint* - nazwa footprintu widoczna na schemacie

*Manufacturer* - producent

*Manufacturer Part Number* - pełna nazwa podawana przez producenta 

*HelpURL* - link do datasheet

W kolumnie Manufacturer przyjęto producentów wypisanych w tabeli //_Manufacturers//. Jeśli jest potrzeba dodania nowego należy uzupełnić tą tabelę.


### Opis kolumn specyficznych dla danej tabeli

#### Capacitors

  - Part Number = C_pojemność_napięcie_dielektryk_obudowa_tolerancja np: C_1nF_50V_C0G_0402_5, C_4.7nF_50V_C0G_0402_5

  - Value = wartość pojemności

  - Comment = określa dielektryk np:Ceramic X7R,Electrolytic Capacitor

  - Voltage = maksymalne dopuszczalne napięcie (np. 50V)

  - Dielectric = np:X7R,X5R,AL2O3

  - Tolerance = tolerancja wyrażona w procentach

Kondensatory (wartości po kolei, zapis tak jak powinien być z takimi sufixami):
 - 0.05pF - dowolnie mała wartość zawsze w pF
 - 0.05pF - 999pF
 - 1nF - 999nF
 - 1uF - 999uF
 - 1mF - 999mF
 - 1F+

Napięcia:
 - 6.3V
 - 50V - 999V
 - 1KV+

#### Resistors

Rezystory (napięcia jak przy kondensatorach, wartości poniżej):
 - dowolnie mała rezystancja - R 
 - 1R - 999R
 - 1k - 999k
 - 1M - 999M
 - 1G+

#### Inductors
Opór:
- dowolnie mała rezystancja w mR
- 1mR - 999mR
- 1R - 999R

Prąd:
- 1mA - 999mA
- 1A - 999A

Indukcyjność:
- 1nH
- 1nH - 999nH
- 1uH - 999uH
- 1mH - 999mH
- lub "NC" 

#### SAW
- Center Frequency - Częstotliwość środkowa (w MHz)
- Bandwidth - Pasmo (zdefiniowane jako 3 dB już odpowiadające datasheetowi) (w MHz)
- Max Power - maksymalna moc wejściowa (w dBm)
 
#### Switching regulators
- frequency - typowa częstotliwość pracy (w kHz)
- topology - topologia przetwornicy możliwe opcje: buck, boost, buck/bust, other
- Vinmax, Vinmin maksymalne i minimalne napięcie wejściowe
- Voutmax, Voutmin - minimalne i maksymalne napięcie wyjściowe, gdy stałe oba pola powinny zawierać to samo
- Imax - maksymalny prąd wyjściowy (gdy trudny do ustalenia zostawić puste)
- Iswitch - maksymalny prąd klucza

#### Voltage Regulators
- voltage - napięcie nominalne (lub ADJ)
- Current - prąd maksymalny
- Part Number:
  - VL_LP2980AIM5-5.0_5V_0A05- napierw jest model elementu, potem napięcie wyjściowe, oraz maksymalny prąd wyjściowy.
  - W przypadku układu regulowanego w pole napięcie wpisać ADJ.

#### Transistors
- Voltage- napięcie CE / DS
- Current- prąd CE / DS
- Type (N-MOS 2, P-MOS 2, NPN PreBias 2, NPN, PNP N-MOS, P-MOS, NPN 2, PNP 2)

## Dodawanie nietypowych elementów

### Transformatory, cewki:

Dodaje się je do bazy w tabeli Inductors.

W sytuacji gotowych transformatorów dodajemy je do bazy tworząc symbol jeśli uniwersalny nie pasuje, a footprint korzystamy z uniwersalnego, a jeśli się nie da to tworzymy nowy. Przykład filtru common mode:

Part Number: LM_RN112-0.8-02-10M

Symbol: LM_RNxxx

Footprint: RN112

### Własne elementy indukcyjne

Jeśli musimy dodać element, który wykonywany jest ręcznie to dodajemy do bazy elementy które da się kupić.

Z reguły jest to karkas oraz rdzeń.

Rdzeń: CORE\_{nazwa rdzenia}\_{materiał}

Karkas: LM\_{nazwa rdzenia}\_{ilość pinów}pin\_{numer}

Przykład:

Rdzeń: `CORE_E16_8_5_N87`

powiązany karkas: `LM_E16_8_5_8pin_1`.

### Gniazda USB

{J/JB}\_USB\_{TYP}\_{NAZWAPRODUCENTA}\_{SMD/THT}

Na przykład: J_USB_MINIB_ 54819-0519_THT - Żeńskie gniazdo USB typu B wersja THT


### Connection Matrix
Modyfikacje względem domyślnej macierzy błędów są następujące:
  - Power pin-Unconnected = Error
  - IO PIN - Output PIN  = No Error



