#!/usr/bin/python3

import os
import sys
import gitlab
from git import Repo
import shutil
import subprocess
from pprint import pprint, pformat

from numpy import imag
from paths import repo_path
import whatthepatch
import csv
import copy
import atexit
import traceback
import glob

failed = True


@atexit.register
def set_labels():
    def remove_label(label):
        if label in gl_merge_request.labels:
            gl_merge_request.labels.remove(label)

    def add_label(label):
        if label not in gl_merge_request.labels:
            gl_merge_request.labels.append(label)

    print("Pipeline failed?: ", failed)
    if failed:
        add_label('pipeline failing')
        remove_label('to review')
        out_description_exit = '# Pipeline failed!\r\n\r\n\r\n' + out_description
    else:
        add_label('to review')
        remove_label('pipeline failing')
        remove_label('to fix')
        out_description_exit = out_description

    gl_merge_request.description = out_description_exit
    gl_merge_request.save()

repo = Repo('.')

os.environ['DISPLAY'] = ':0'
OCTOPART_TOKEN = os.getenv('OCTOPART_TOKEN')
CI_SERVER_URL = os.getenv('CI_SERVER_URL')
CI_PROJECT_ID = os.getenv('CI_PROJECT_ID')
CI_MERGE_REQUEST_IID = os.getenv('CI_MERGE_REQUEST_IID')
CI_MERGE_REQUEST_TARGET_BRANCH_NAME = os.getenv('CI_MERGE_REQUEST_TARGET_BRANCH_NAME')
CI_MERGE_REQUEST_SOURCE_BRANCH_NAME = os.getenv('CI_MERGE_REQUEST_SOURCE_BRANCH_NAME')
GITLAB_PRIVATE_TOKEN = os.getenv('GITLAB_PRIVATE_TOKEN')

master_branch = CI_MERGE_REQUEST_TARGET_BRANCH_NAME
merging_branch = CI_MERGE_REQUEST_SOURCE_BRANCH_NAME

out_description = ''

gl = gitlab.Gitlab(CI_SERVER_URL, private_token=GITLAB_PRIVATE_TOKEN)
gl_project = gl.projects.get(CI_PROJECT_ID)
gl_merge_request = gl_project.mergerequests.get(CI_MERGE_REQUEST_IID)


sys.path.append(repo_path + '/_tools/library_parser')
sys.path.append(repo_path + '/_tools/partkeepr')
sys.path.append(repo_path + '/_tools/SVG_render')

from git_diff import diff_schlib, diff_pcblib
from SchLib.SchLib import SchLib
from render_svg import render as render_to_svg
import parse
import octopart_v4

octopart_v4.set_token(OCTOPART_TOKEN)

def mkdir(path):
    try:
        os.makedirs(path)
    except FileExistsError:
        pass


# test if library checks are OK
result = subprocess.run([repo_path + '/_tools/library_tests/main.py'], stdout=subprocess.PIPE,
                        stderr=subprocess.STDOUT)
print(result.stdout.decode())
if result.returncode != 0:
    out_description += '# Altium Library tests failed!\r\n\r\n'
    out_description += '```' + result.stdout.decode() + '```'
    out_description += "\r\n"
    gl_merge_request.description = out_description
    gl_merge_request.save()
    exit(result.returncode)

files_changed = repo.index.diff('origin/master').iter_change_type('M')
altium_libraries_changed = list(filter(lambda f: f.b_path.lower().endswith(('.schlib', '.pcblib')), files_changed))

files_added = repo.index.diff('origin/master').iter_change_type('D')
altium_libraries_added = list(filter(lambda f: f.b_path.lower().endswith(('.schlib', '.pcblib')), files_added))

mkdir("__tmp")

component_files_changed = repo.index.diff('origin/master', create_patch=True, unified=0, R=True).iter_change_type('M')
component_files_changed = filter(lambda f: f.b_path.lower().endswith('.csv'), component_files_changed)
component_files_changed = filter(lambda f: not os.path.basename(f.b_path.lower()).startswith('_'),
                                 component_files_changed)

parts_modified = []
for c in component_files_changed:
    for patch in whatthepatch.parse_patch(c.diff.decode()):
        for line in patch.changes:
            if line.new:
                text = list(csv.reader([line.line]))[0]
                parts_modified.append(text[0])

print("Altium libs changed: ", altium_libraries_changed)
print("Altium libs added: ", altium_libraries_added)
print("Parts modified: ", parts_modified)

RED = ' ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) '
GREEN = ' ![#00ff00](https://via.placeholder.com/15/00ff00/000000?text=+) '

for part in parts_modified:
    p = parse.parts[part]
    data = copy.deepcopy(p.row)
    print()

    try:
        octo = octopart_v4.OctopartElement(p.manufacturer_part_number, manufacturer=p.manufacturer)
    except:
        out_description += '# ' + str(p.manufacturer_part_number) + " - Octopart failed!\r\n"
        out_description += '```\n' + traceback.format_exc() + "\n```\n"

        octo = lambda: None
        octo.short_description = ''
        octo.manufacturer = p.manufacturer
        octo.datasheet = None
        octo.mpn = p.manufacturer_part_number

    txt = '# ' + str(p.part_number) + "\r\n"
    txt += octo.short_description + "\r\n\r\n"
    txt += '| Parameter | Database value | ? | Octopart cross-check |\r\n'
    txt += '| --------- | -------------- | -- |-------------------- |\r\n'


    def render(name, value_database, value_octopart, database_url=None, no_red=False, octopart_url=None,
               ignore_case=False):
        color = RED
        if value_database == value_octopart or (ignore_case and value_database.lower() == value_octopart.lower()):
            color = GREEN
        if value_octopart is None:
            color = ''
            value_octopart = ''
        if color == RED and no_red:
            color = ''
        if octopart_url:
            value_octopart = '[' + value_octopart + '](' + octopart_url + ')'
        if database_url:
            value_database = '[' + value_database + '](' + database_url.replace(' ', '%20') + ')'
        return '| ' + name + ' | ' + str(value_database) + ' | ' + color + ' | ' + value_octopart + " |\r\n"


    txt += render('Manufacturer', p.manufacturer, octo.manufacturer, ignore_case=True)
    txt += render('MPN', p.manufacturer_part_number, octo.mpn)
    txt += render('SchLib', p.schlib, None)
    txt += render('PcbLib', p.pcblib, None)
    txt += render('SCH Footprint', p.sch_footprint, None)
    if octo.datasheet:
        txt += render('HelpURL', '[Link](' + p.help_url + ')', '[Link](' + octo.datasheet + ')')
    else:
        txt += render('HelpURL', '[Link](' + p.help_url + ')', '')

    del data['Part Number']
    del data['Manufacturer']
    del data['Manufacturer Part Number']
    del data['Library Ref']
    del data['Footprint Ref']
    del data['HelpURL']
    del data['Supplier 1']
    del data['Supplier 2']
    del data['Supplier 3']
    del data['Supplier 4']
    del data['Supplier 5']
    del data['Supplier 6']
    del data['Supplier 7']
    del data['Supplier Part Number 1']
    del data['Supplier Part Number 2']
    del data['Supplier Part Number 3']
    del data['Supplier Part Number 4']
    del data['Supplier Part Number 5']
    del data['Supplier Part Number 6']
    del data['Supplier Part Number 7']

    for parameter in data:
        txt += render(parameter, data[parameter], '', no_red=True)

    search_url = {
        'TME': 'https://www.tme.eu/pl/katalog/?search={}',
        'RS Components': 'https://pl.rs-online.com/web/c/?sra=oss&r=t&searchTerm={}',
        'Farnell': 'https://pl.farnell.com/search?st={}',
        'Mouser': 'https://eu.mouser.com/Search/Refine?Keyword={}',
        'Digi-Key': 'https://www.digikey.com/products/en?keywords={}',
        'LCSC': 'https://lcsc.com/search?q={}',
        'Arrow Electronics': 'https://arrow.com/en/products/search?q={}',
        'Elfa Distrelec': 'https://www.elfadistrelec.pl/search?q={}',
        'Abacus Technologies': 'https://www.abacuselect.com/search/?q={}',
        'Avnet': 'https://www.avnet.com/shop/emea/search/{}',
    }

    alternative_names = {
        'RS Components': 'RS (Formerly Allied Electronics)',
        'Digi-Key': 'DigiKey',
    }

    for seller in set(range(1, 8)) - {6}:
        name = p.row['Supplier ' + str(seller)]
        if not name:
            continue

        try:
            octopart_sku = octo.sellers_by_name[name][0]['sku']
            octopart_url = octo.sellers_by_name[name][0]['click_url']
        except:
            octopart_sku = ''
            octopart_url = None

        if octopart_sku == '' and name in alternative_names:
            try:
                alternative_name = alternative_names[name]
                octopart_sku = octo.sellers_by_name[alternative_name][0]['sku']
                octopart_url = octo.sellers_by_name[alternative_name][0]['click_url']
            except:
                octopart_sku = ''
                octopart_url = None

        sku = p.row['Supplier Part Number ' + str(seller)]
        if name in search_url:
            url = search_url[name].format(sku)
        else:
            url = None
        txt += render(name, sku, octopart_sku, database_url=url, octopart_url=octopart_url)

    txt += '\r\n'
    out_description += txt


def print_pins_if_schlib(schlib_name):
    txt = ''
    if schlib_name.lower().endswith("schlib"):
        schlib = SchLib(schlib_name)

        txt += '| Designator | Name | Type | Hidden |\r\n'
        txt += '| ---------- | ---- | ---- | ------ |\r\n'

        for pin in schlib.libraries[0].pins:
            if pin.owner_part_display_mode != 0:
                continue
            hidden = ""
            if pin.pin_hidden:
                hidden = RED
            txt += '| {} | {} | {} | {} |\r\n'.format(pin.designator, pin.display_name, pin.electrical_type.name,
                                                      hidden)
    return txt

def print_pins_if_schlib_and_different(file_a, file_b):
    txt_a = print_pins_if_schlib(file_a)
    txt_b = print_pins_if_schlib(file_b)
    if txt_a != txt_b:
        return txt_a
    return 'Pin assignments are the same.'


def render_component(filepath):
    tmp_img_without_extension = os.getcwd() + '/img'
    tmp_img = tmp_img_without_extension + '.svg'

    images = {}
    def set_image(filepath):
        try:
            (_, part, alternate) = os.path.basename(filepath).split(".")[0].split("_")
        except ValueError as e:
            #  svg does not containg part/alternate mark
            part = 'A'
            alternate = '0'

        uploaded = gl_project.upload(basename + '.svg', filepath=filepath)

        if part not in images:
            images[part] = {}
        if alternate not in images[part]:
            images[part][alternate] = {}

        images[part][alternate] = uploaded['markdown']

    # assumption: when multiple parts/alternate modes
    # render produces svgs in pattern name_PART_MODE.svg
    # where PART = A, B, C, ....
    # MODE = 0, 1, 2, ....

    render_to_svg(filepath, tmp_img)
    files = glob.glob(tmp_img_without_extension + '*.svg')

    for f in files:
        set_image(f)
        os.remove(f)

    return images


def render_diffs(file_a, file_b):
    images_merged = {}
    
    def append_file(file, side):
        img = render_component(file)
        for part in img:
            if part not in images_merged:
                images_merged[part] = {}

            for alternate in img[part]:
                if alternate not in images_merged[part]:
                    images_merged[part][alternate] = {}

                images_merged[part][alternate][side] = img[part][alternate]
    
    append_file(file_a, 'a')
    append_file(file_b, 'b')

    #  fill empty ones
    for part in images_merged:
        for alternate in images_merged[part]:
            if 'a' not in images_merged[part][alternate]:
                images_merged[part][alternate]['a'] = ''
            if 'b' not in images_merged[part][alternate]:
                images_merged[part][alternate]['b'] = ''

    return images_merged


for diff in altium_libraries_changed:
    basename = os.path.basename(diff.a_path)
    name = diff.a_path

    file = os.getcwd() + '/' + diff.a_path
    print(diff.a_path, diff.b_path)
    print("Now doing: ", file)

    compare = os.getcwd() + '/__tmp/' + diff.b_path

    mkdir(os.path.dirname(compare))
    diff.b_blob.stream_data(open(compare, 'wb'))

    print("comparing: ", file, compare)

    if file.lower().endswith("schlib"):
        diff = pformat(diff_schlib(compare, file), width=50)
    if file.lower().endswith("pcblib"):
        diff = diff_pcblib(compare, file).pretty()

    ##  ------------- render images of the files -------------
    images = render_diffs(file, compare)
    
    out_description += '# ' + name + '\n'
    out_description += '| -------------------------------------------------- before ' \
                       '-------------------------------------------------- | ' \
                       '-------------------------------------------------- after ' \
                       '-------------------------------------------------- |\n '
    out_description += '| ------ | ------ |\n'

    # no modes/alternates
    if len(images) == 1 and len(images['A']) == 1:
        out_description += '| ' + images['A']['0']['b'] + ' | ' + images['A']['0']['a'] + ' |\n'
    else:
        for part in sorted(images):
            for mode in sorted(images[part]):
                txt_desc = []

                if len(images['A']) == 1: # only one alternate mode
                    txt_desc.append(f"Part {part}")

                if len(images) == 1: # only one part
                    txt_desc.append(f"Mode {mode}")

                out_description += '| ' + ', '.join(txt_desc) + ' | ' + ' |\n'
                out_description += '| ' + images[part][mode]['b'] + ' | ' + images[part][mode]['a'] + ' |\n'


    ## ----------- diffs of the files -------------
    out_description += '```\n' + diff + '\n```\n\n'

    ## ----------- schlib pins -------------
    out_description += print_pins_if_schlib_and_different(file, compare)
    out_description += "\n\n---------\n\n"

for name in altium_libraries_added:
    print('added:', name.a_path)
    basename = os.path.basename(name.a_path)

    tmp_img = os.getcwd() + '/img.svg'
    file = os.getcwd() + '/' + name.a_path

    print("Now: ", file)
    out_description += '# ' + name.a_path + '\r\n'
    
    images = render_component(file)
    for part in sorted(images):
        for mode in sorted(images[part]):
            out_description += '## ' + f"Part {part}, Mode {mode}" + '\r\n'
            out_description += images[part][mode] + '\r\n\r\n'

    out_description += print_pins_if_schlib(file) + '\r\n\r\n'

shutil.rmtree('./__tmp')
failed = False
