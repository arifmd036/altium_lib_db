import os
from glob import glob
from paths import repo_path

mdb_file = os.path.join(repo_path, 'LibraryDatabase.mdb')
csv_files = glob(os.path.join(repo_path, '_export', '[a-zA-Z]*.csv'))

last_mdb_modification = os.path.getmtime(mdb_file)
last_csv_modification = max([os.path.getmtime(i) for i in csv_files])

if last_csv_modification > last_mdb_modification:
    import import_csv
