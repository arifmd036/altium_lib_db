﻿Add-Type -AssemblyName PresentationFramework

$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
$repoPath = split-path -parent (split-path -parent $scriptPath)
$tempPath = [System.IO.Path]::GetTempPath()

function Get-RegistryValue($path, $name)
{
    $key = Get-Item -LiteralPath $path -ErrorAction SilentlyContinue
    if ($key) {
        $key.GetValue($name, $null)
    }
}

function DownloadFile($url, $outfile) {
    $p = [Enum]::ToObject([System.Net.SecurityProtocolType], 3072);
    [System.Net.ServicePointManager]::SecurityProtocol = $p;
    [System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

    (New-Object Net.WebClient).DownloadFile("$url", "$tempPath\$outfile") 

    if (!(Test-Path "$tempPath\$outfile" -PathType Leaf)) {
        [System.Windows.MessageBox]::Show('Error downloading.', 'Error')
        exit
    }
}

function Check_Program_Installed($programName)
{
    return (Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* | Where { $_.DisplayName -Match "Microsoft Access database engine" }) -ne $null
}

Write-Output ""
Write-Output ""
Write-Output "-------------------------------------------------------"
Write-Output "-------------------------------------------------------"
Write-Output "------------- Installing Altium libraries -------------"
Write-Output "-------------------------------------------------------"
Write-Output "--------- Allow couple of minutes to complete ---------"
Write-Output "-------------------------------------------------------"
Write-Output "-------------------------------------------------------"
Write-Output ""
Write-Output ""


if (($repoPath -match "[^\u0000-\u007F]") -or
    ($repoPath -match "\s")) {
    [System.Windows.MessageBox]::Show('Repository path ' + $repoPath + ' contains non-ascii or whitespace characters. This is not allowed!', 'Error')
    exit
}

# ----------------------- Check if altium is opened ----------------------------

$processes = Get-Process X2,DXP -ErrorAction SilentlyContinue

While($null -ne $processes) {
    [System.Windows.MessageBox]::Show('Please close Altium!', 'Warning')

    $processes = Get-Process X2,DXP -ErrorAction SilentlyContinue
}

# --------------------------- Python interpreter -------------------------------

$python_installed = Test-Path "$scriptPath\python"
if (!$python_installed) {
    Write-Output "Downloading python..."
    DownloadFile "https://github.com/winpython/winpython/releases/download/5.3.20221233/Winpython64-3.11.1.0dot.exe" "WinPython.exe"
    
    Write-Output "Installing python to `"$scriptPath\python`"..."
    $args = "-gm2 -y -o`"$scriptPath\python`""
    (Start-Process -FilePath "$tempPath\WinPython.exe" -ArgumentList $args -Pass).WaitForExit()

    Move-Item "$scriptPath\python\*\*" "$scriptPath\python\"
}

# --------------------- Install required python packages -----------------------

& "$scriptPath\python\scripts\WinPython_PS_Prompt.ps1"
& "$scriptPath\python\python-3.11.1.amd64\python.exe" -m pip install --upgrade pip
& "$scriptPath\python\python-3.11.1.amd64\Scripts\pip3.exe" install -r "$scriptPath\requirements.txt"

# ---------------------- Install Access Database Engine ------------------------

$database_engine_installed = Check_Program_Installed("Microsoft Access database engine")
if (!$database_engine_installed) {
    Write-Output "Downloading access database engine..."
    DownloadFile "https://download.microsoft.com/download/2/4/3/24375141-E08D-4803-AB0E-10F2E3A07AAA/AccessDatabaseEngine_X64.exe" "AccessDBEngine.exe"
    Write-Output "Installing access database engine..."
    (Start-Process -FilePath "$tempPath\AccessDBEngine.exe" -ArgumentList {"/quiet"} -Pass).WaitForExit()
} else {
    Write-Output "Access database engine already installed"
}

# ----------------- find MS Access and setup trusted location ------------------

$access_versions = @(16, 14, 12, 10)
$found=0

for ($i=0; $i -lt $access_versions.length; $i++) {
    $now = $access_versions[$i]
    $path = 'HKCU:\Software\Microsoft\Office\' + "$now" + '.0\Access\Security\Trusted Locations'

    If (Test-Path "$path") {
        $found=1
        break
    }
}

if ($found -eq 0) {
    $now = $access_versions[$i]
    $path = 'HKCU:\SOFTWARE\Microsoft\Office\' + "$now" + '.0\Access\Security\Trusted Locations'

    If (Test-Path "$path") {
        $found=1
        break
    }
}

if ($found -eq 0) {
    [System.Windows.MessageBox]::Show('Do you have MS Access installed?', 'Error')
    exit
}

Write-Output 'Found access:' $path

$found = 0
$already_done = 0

for ($i=1; $i -lt 1000; $i++) {
    $path2 = $path + "\Location" + "$i"

    If (Test-Path "$path2") {
        $val = Get-ItemProperty -Path $path2 -Name "Path"

        if ($val.Path -eq $repoPath) {
            $already_done = 1
            break
        }
    } Elseif ($found -eq 0) {
        $found = 1
        $path = $path2
    }
}

if (($already_done -eq 0) -and ($found -eq 0)) {
    [System.Windows.MessageBox]::Show('Ask admin for help.')
    exit
}

if ($already_done -eq 0) {
    New-Item -Path $path
    New-ItemProperty -Path $path -Name "Path" -Value "$repoPath" -PropertyType String
    New-ItemProperty -Path $path -Name "AllowSubfolders" -Value 00000001 -PropertyType DWORD
} else {
    Write-Output "Trusted location already applied"
}


# -------------------- Altium -----------------------------
function find_first_empty_or_equal_location($path, $prefix, $equal_to) {      
    if (!(Test-Path "$path")) {
        New-Item -Path $path
    }
        
    for ($i=0; $i -lt 1000; $i++) {
        $val = Get-RegistryValue $path "$prefix$i"

        if ($val -eq $equal_to) {
            return $i
        }
    }

    for ($i=0; $i -lt 1000; $i++) {
        $val = Get-RegistryValue $path "$prefix$i"

        if ($val -eq $none) {
            return $i
        }
    }
    
    [System.Windows.MessageBox]::Show('Something went wrong during searching ' + $path + '. Ask for help!')
    exit
}


if (!(Test-Path "HKCU:\Software\Altium")) {
    [System.Windows.MessageBox]::Show('Do you have Altium installed?', 'Error')
    exit
}

$table = Get-ChildItem "HKCU:\Software\Altium"
$db_lib_path = $repoPath + "\Library.DBLib"

foreach ($row in $table) {
    $found = $row.Name.IndexOf("Altium Designer {")
    if ($found -eq -1) {
        # not altium subfolder
        continue
    }

    $altium_path = "HKCU:\Software\Altium\" + $row.PSChildName + "\DesignExplorer\Preferences"

    # --------------------- install library ---------------------------------
    $reg_path = $altium_path + "\IntegratedLibrary\Loaded Libraries"
    $place = find_first_empty_or_equal_location $reg_path "Library" $db_lib_path

    Write-Output "Adding altium $reg_path : $place,$first_free"

    Remove-ItemProperty -Path $reg_path -Name "Library$place"
    Remove-ItemProperty -Path $reg_path -Name "LibraryActivated$place"
    Remove-ItemProperty -Path $reg_path -Name "LibraryRelativePath$place"
    Remove-ItemProperty -Path $reg_path -Name "LibraryViewSettings$place"

    New-ItemProperty -Path $reg_path -Name "Library$place" -Value "$db_lib_path" -PropertyType String
    New-ItemProperty -Path $reg_path -Name "LibraryActivated$place" -Value "1" -PropertyType String
    New-ItemProperty -Path $reg_path -Name "LibraryRelativePath$place" -Value "$db_lib_path" -PropertyType String
    New-ItemProperty -Path $reg_path -Name "LibraryViewSettings$place" -Value "" -PropertyType String


    # --------------------- install device sheets folder ---------------------------------

    $device_sheet_path = $repoPath + "\device_sheets\"
    $reg_path = $altium_path + "\IntegratedLibrary\Device Sheet Folders"

    $place = find_first_empty_or_equal_location $reg_path "DeviceSheetFolderPath" $device_sheet_path

    Write-Output "Adding altium device sheet path $reg_path : $place,$first_free"

    Remove-ItemProperty -Path $reg_path -Name "DeviceSheetFolderPath$place"
    Remove-ItemProperty -Path $reg_path -Name "DeviceSheetFolderSubfolder$place"

    New-ItemProperty -Path $reg_path -Name "DeviceSheetFolderPath$place" -Value "$device_sheet_path" -PropertyType String
    New-ItemProperty -Path $reg_path -Name "DeviceSheetFolderSubfolder$place" -Value "1" -PropertyType String

    # --------------------- install snippets folder ---------------------------------

    $snippets_path = $repoPath + "\snippets\"
    $reg_path = $altium_path + "\Client\InstalledCollections"

    $place = find_first_empty_or_equal_location $reg_path "Path" $snippets_path

    $val = Get-RegistryValue $reg_path "Count"
    Write-Output "Adding altium snippets path $reg_path : $place"

    Remove-ItemProperty -Path $reg_path -Name "Count"
    Remove-ItemProperty -Path $reg_path -Name "Path$place"    
    New-ItemProperty -Path $reg_path -Name "Path$place" -Value "$snippets_path" -PropertyType String

    $val = ((Get-ItemProperty "$reg_path").PSObject.Properties | measure).Count - 5
    New-ItemProperty -Path $reg_path -Name "Count" -Value "$val" -PropertyType DWORD
}

# --------------------------------- Install git hooks -------------------------------------
Copy-Item -Path "$scriptPath\_git_hooks\*" -Destination "$repoPath\.git\hooks\"

$Tab = [char]9
$nl = [char]13 + [char]10

if (@(Get-Content $repoPath\.git\config -ErrorAction SilentlyContinue | Where-Object { $_.Contains("altium_diff") }).Count -eq 0) {
    $line = $nl + '[diff "altium_diff"]' + $nl
    $line += $Tab + 'command = _tools/library/python/python-3.11.1.amd64/python.exe _tools/library_parser/git_diff.py' + $nl
    $line += $Tab + 'binary = true'
    $line | Add-Content $repoPath\.git\config
}

if (@(Get-Content $repoPath\.git\info\attributes -ErrorAction SilentlyContinue | Where-Object { $_.Contains("altium_diff") }).Count -eq 0) {

    $line = '*.SchLib diff=altium_diff' + $nl
    $line += '*.PcbLib diff=altium_diff'
    $line | Add-Content $repoPath\.git\info\attributes
}

# --------------------------- Remove Access file -------------------------------
Remove-Item "$repoPath\LibraryDatabase.MDB" -ErrorAction SilentlyContinue

# ----------------------------------- build database --------------------------------------

cmd.exe "/c `"`"$repoPath\update_altium.bat`" 2>&1`""
cmd.exe "/c `"`"$repoPath\update_altium.bat`" 2>&1`""

if (!(Test-Path "$repoPath\LibraryDatabase.MDB" -PathType Leaf)) {
    [System.Windows.MessageBox]::Show('Database file was not created.', 'Error')
    exit
}

# -------------------------------- Check for errors ---------------------------------------
cmd.exe "/c `"`"$repoPath\check.bat`" 2>&1`""

# --------------------------------------- Done --------------------------------------------
Write-Output ""
Write-Output ""
Write-Output "-------------------------------------------------------"
Write-Output "-------------------------------------------------------"
Write-Output "---------------- Finished successfully ----------------"
Write-Output "-------------------------------------------------------"
Write-Output "-------------------------------------------------------"
Write-Output ""
Write-Output ""

[System.Windows.MessageBox]::Show('Finished successfully.', 'OK')