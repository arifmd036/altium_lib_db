import svgwrite
from numpy import math

from PcbLib.PcbLib_Pad import Shape
from layer_info import layer_metadata


class SVG_PcbLib:
    def __init__(self, pcblib):
        self.pcblib = pcblib
        self.svg = svgwrite.Drawing()

    def _render_track(self, canvas, track, svg):
        color = layer_metadata[track.layer_id].color
        line = svg.line((track.X1, -track.Y1), (track.X2, -track.Y2),
                        stroke_linecap="round",
                        stroke=svgwrite.rgb(color.R, color.G, color.B),
                        stroke_width=track.width)
        canvas.add(line)
        xx = [track.X1, track.X2]
        yy = [-track.Y1, -track.Y2]
        return xx, yy

    def _render_arc(self, canvas, arc, svg):
        color = layer_metadata[arc.layer_id].color

        if arc.StartAngle == 0 and arc.EndAngle == 360:
            canvas.add(svg.circle(center=(arc.X, -arc.Y),
                               r=arc.Radius,
                               stroke=svgwrite.rgb(color.R, color.G, color.B),
                               stroke_width=arc.width,
                               fill="none",
                               ))

            xx = [arc.X - arc.Radius, arc.X + arc.Radius]
            yy = [arc.Y - arc.Radius, arc.Y + arc.Radius]
            return xx, yy

        # start_x = arc.X
        # start_y = -arc.Y
        radius = arc.Radius

        degree0 = arc.StartAngle
        degree1 = arc.EndAngle

        form = ""
        if (degree1 - degree0) < 0:
            form = "1,0"
        else:
            form = "0,0"

        m0 = arc.X1
        n0 = -arc.Y1
        m1 = arc.X2 - arc.X1
        n1 = arc.Y2 - arc.Y1

        w = svg.path(d="M {0},{1} a {2},{2} 0 {5} {3},{4}".format(m0, n0, radius, m1, n1, form),
                     stroke=svgwrite.rgb(color.R, color.G, color.B),
                     stroke_width=arc.width,
                     fill="none",
                     stroke_linecap="round",
                     )
        canvas.add(w)

        xx = [m0, n1]
        yy = [n0, m1]
        return xx, yy

    def _render_fill(self, canvas, fill, svg):
        color = layer_metadata[fill.layer_id].color

        loc_x = min(fill.X1, fill.X2)
        loc_y = min(-fill.Y1, -fill.Y2)

        width = abs(fill.X1 - fill.X2)
        height = abs(fill.Y1 - fill.Y2)

        rect = svg.rect(insert=(loc_x, loc_y),
                        size=(width, height),
                        fill=svgwrite.rgb(color.R, color.G, color.B),
                        transform='rotate(%s,%s, %s)' % (-int(fill.Rotation), loc_x + (width / 2), loc_y + (height / 2))
                        )
        canvas.add(rect)
        xx = [loc_x, loc_x + width]
        yy = [loc_y, loc_y + height]
        return xx, yy

    def _render_text(self, canvas, text, svg):
        color = layer_metadata[text.layer_id].color

        font_italic = ''
        if text.font_italic == 1:
            font_italic = 'font-style:italic;'

        font_weight = ''
        if text.font_bold:
            font_weight = 'font-weight:bold;'

        text_svg = svg.text(text.text,
                            insert=(text.X, -text.Y),
                            fill=svgwrite.rgb(color.R, color.G, color.B),
                            font_size=int(text.Height) * 0.8,
                            style="font-family:" + str(text.font_name) + ';' + font_weight + font_italic,
                            transform='rotate(%s,%s, %s)' % (-int(text.Rotation), text.X, - text.Y),
                            )
        canvas.add(text_svg)

        xx = [text.X, text.X]
        yy = [-text.Y, -text.Y]
        return xx, yy

    def _render_pad(self, pad_canvas, solder_canvas, solder_canvas_id, pad, svg):
        color = layer_metadata[pad.SizeAndShape.layer_id].color
        solder_color = layer_metadata[solder_canvas_id].color
        width = pad.SizeAndShape.XSize_Top
        height = pad.SizeAndShape.YSize_Top

        # TODO octagonal shape
        if pad.Shape == Shape.Rectangular:  # Shape rectangular
            rect = svg.rect(insert=(pad.SizeAndShape.X - (width / 2), -pad.SizeAndShape.Y - (height / 2)),
                            size=(width, height),
                            fill=svgwrite.rgb(color.R, color.G, color.B),
                            transform='rotate(%s,%s, %s)' % (
                                -int(pad.SizeAndShape.Rotation), pad.SizeAndShape.X, - pad.SizeAndShape.Y),
                            )
            pad_canvas.add(rect)

            solder_rect = svg.rect(
                insert=(pad.SizeAndShape.X - (width / 2) - 4, -pad.SizeAndShape.Y - (height / 2) - 4),
                size=(width + 8, height + 8),
                fill=svgwrite.rgb(solder_color.R, solder_color.G, solder_color.B),
                transform='rotate(%s,%s, %s)' % (
                    -int(pad.SizeAndShape.Rotation), pad.SizeAndShape.X, - pad.SizeAndShape.Y),
            )
            solder_canvas.add(solder_rect)

        if pad.Shape == Shape.RoundedRectangle:  # Shape_Round
            rd = 0
            solder_rd = 0
            if abs(pad.SizeAndShape.XSize_Top) < abs(pad.SizeAndShape.YSize_Top):
                rd = pad.SizeAndShapeByLayer.CornerRadius[0] / 100.0 * pad.SizeAndShape.XSize_Top / 2.
                solder_rd = pad.SizeAndShapeByLayer.CornerRadius[0] / 100.0 * (pad.SizeAndShape.XSize_Top + 8.) / 2.
            else:
                rd = pad.SizeAndShapeByLayer.CornerRadius[0] / 100.0 * pad.SizeAndShape.YSize_Top / 2.
                solder_rd = pad.SizeAndShapeByLayer.CornerRadius[0] / 100.0 * (pad.SizeAndShape.YSize_Top + 8.) / 2.

            rect = svg.rect(insert=(pad.SizeAndShape.X - (width / 2), -pad.SizeAndShape.Y - (height / 2)),
                            size=(width, height),
                            fill=svgwrite.rgb(color.R, color.G, color.B),
                            transform='rotate(%s,%s, %s)' % (
                                -int(pad.SizeAndShape.Rotation), pad.SizeAndShape.X, - pad.SizeAndShape.Y),
                            rx=rd,
                            ry=rd
                            )
            pad_canvas.add(rect)

            solder_rect = svg.rect(
                insert=(pad.SizeAndShape.X - (width / 2) - 4, -pad.SizeAndShape.Y - (height / 2) - 4),
                size=(width + 8, height + 8),
                fill=svgwrite.rgb(solder_color.R, solder_color.G, solder_color.B),
                transform='rotate(%s,%s, %s)' % (
                    -int(pad.SizeAndShape.Rotation), pad.SizeAndShape.X, - pad.SizeAndShape.Y),
                rx=solder_rd,
                ry=solder_rd
            )
            solder_canvas.add(solder_rect)

        if pad.Shape == Shape.Round:  # Shape_Round
            rd = 0
            solder_rd = 0
            if abs(pad.SizeAndShape.XSize_Top) == abs(pad.SizeAndShape.YSize_Top):
                rd = pad.SizeAndShape.XSize_Top / 2.
                solder_rd = (pad.SizeAndShape.XSize_Top + 8.) / 2.
            else:
                if abs(pad.SizeAndShape.XSize_Top) < abs(pad.SizeAndShape.YSize_Top):
                    rd = pad.SizeAndShape.XSize_Top / 2.
                    solder_rd = (pad.SizeAndShape.XSize_Top + 8.) / 2.
                else:
                    rd = pad.SizeAndShape.YSize_Top / 2.
                    solder_rd = (pad.SizeAndShape.YSize_Top + 8.) / 2.

            rect = svg.rect(insert=(pad.SizeAndShape.X - (width / 2), -pad.SizeAndShape.Y - (height / 2)),
                            size=(width, height),
                            fill=svgwrite.rgb(color.R, color.G, color.B),
                            transform='rotate(%s,%s, %s)' % (
                                -int(pad.SizeAndShape.Rotation), pad.SizeAndShape.X, - pad.SizeAndShape.Y),
                            rx=rd,
                            ry=rd
                            )
            pad_canvas.add(rect)

            solder_rect = svg.rect(
                insert=(pad.SizeAndShape.X - (width / 2) - 4, -pad.SizeAndShape.Y - (height / 2) - 4),
                size=(width + 8, height + 8),
                fill=svgwrite.rgb(solder_color.R, solder_color.G, solder_color.B),
                transform='rotate(%s,%s, %s)' % (
                    -int(pad.SizeAndShape.Rotation), pad.SizeAndShape.X, - pad.SizeAndShape.Y),
                rx=solder_rd,
                ry=solder_rd
            )
            solder_canvas.add(solder_rect)

        if pad.SizeAndShape.Shape_Top == 3:  # Shape_Octagon
            print("no Shape_Octagon implemented")

        xx = [pad.SizeAndShape.X - (width / 2), pad.SizeAndShape.X + (width / 2)]
        yy = [-pad.SizeAndShape.Y - (height / 2), -pad.SizeAndShape.Y + (height / 2)]
        return xx, yy

    def _render_pad_hole(self, canvas, pad, svg, color_layer_id):
        color = layer_metadata[color_layer_id].color

        circle = svg.circle(center=(pad.SizeAndShape.X, -pad.SizeAndShape.Y),
                            r=pad.SizeAndShape.HoleSize / 2,
                            fill=svgwrite.rgb(color.R, color.G, color.B),
                            stroke_width=0,
                            )
        canvas.add(circle)

    def _parse_footprint(self, canvas, footprint, layers_by_id):
        svg_footprint = self.svg.g(id=footprint.name.replace(')', '_').replace('(', '_'))
        svg_layers = {}
        xx = []
        yy = []
        for track in footprint.Track:
            if track.layer_id not in svg_layers:
                svg_layers[track.layer_id] = self.svg.g(id=track.layer_id)
            x, y = self._render_track(svg_layers[track.layer_id], track, self.svg)
            xx += x
            yy += y

        for arc in footprint.Arc:
            if arc.layer_id not in svg_layers:
                svg_layers[arc.layer_id] = self.svg.g(id=arc.layer_id)
            x, y = self._render_arc(svg_layers[arc.layer_id], arc, self.svg)
            xx += x
            yy += y

        for fill in footprint.Fill:
            if fill.layer_id not in svg_layers:
                svg_layers[fill.layer_id] = self.svg.g(id=fill.layer_id)
            x, y = self._render_fill(svg_layers[fill.layer_id], fill, self.svg)
            xx += x
            yy += y

        for text in footprint.Text:
            if text.layer_id not in svg_layers:
                svg_layers[text.layer_id] = self.svg.g(id=text.layer_id)
            x, y = self._render_text(svg_layers[text.layer_id], text, self.svg)
            xx += x
            yy += y

        for pad in footprint.Pad:
            solder_canvas = 0
            if layer_metadata[pad.SizeAndShape.layer_id].name in ["Top Layer", "Multi-Layer"]:
                solder_canvas = 16973834

            if layer_metadata[pad.SizeAndShape.layer_id].name in ["Bottom Layer"]:
                solder_canvas = 16973835

            if solder_canvas not in svg_layers:
                svg_layers[solder_canvas] = self.svg.g(id=solder_canvas)
            # if svg_layers[pad.SizeAndShape.layer_id].

            if pad.SizeAndShape.layer_id not in svg_layers:
                svg_layers[pad.SizeAndShape.layer_id] = self.svg.g(id=pad.SizeAndShape.layer_id)

            x, y = self._render_pad(svg_layers[pad.SizeAndShape.layer_id], svg_layers[solder_canvas], solder_canvas,
                                    pad,
                                    self.svg)
            xx += x
            yy += y

            color_layer_id = 16973846  # Pad Holes
            if pad.SizeAndShape.HoleSize > 0:
                if color_layer_id not in svg_layers:
                    svg_layers[color_layer_id] = self.svg.g(id=color_layer_id)

                self._render_pad_hole(svg_layers[color_layer_id], pad, self.svg, color_layer_id)

            designator_layer_id = 1
            if designator_layer_id not in svg_layers:
                svg_layers[1] = self.svg.g(id='designators')

            designator_size = pad.SizeAndShape.YSize_Top / 3

            svg_layers[1].add(self.svg.text(pad.Designator,
                                            insert=(pad.SizeAndShape.X, - pad.SizeAndShape.Y),
                                            fill=svgwrite.rgb(255, 227, 143) if pad.SizeAndShape.HoleSize > 0 else svgwrite.rgb(255, 181, 181),
                                            font_size=(designator_size),
                                            style="font-family: Arial; " + "text-anchor: middle;",
                                            transform='rotate(%s,%s, %s)' % (
                                                -int(pad.SizeAndShape.Rotation), pad.SizeAndShape.X,
                                                - pad.SizeAndShape.Y),
                                            dy=[designator_size/3],
                                            # dx=[dx]
                                            ))

        sorted_keys = sorted(svg_layers.keys(), key=lambda kv: layer_metadata[kv].draw_priority)
        for kay in sorted_keys:
            svg_footprint.add(svg_layers[kay])

        canvas.add(svg_footprint)

        return [min(xx), max(xx)], [min(yy), max(yy)]

    def render(self, filename):
        self.svg = svgwrite.Drawing(filename, fill=svgwrite.rgb(255, 255, 255))

        max_dimensions = []

        for footprint in self.pcblib.Footprints:
            max_dimensions.append(self._parse_footprint(self.svg, footprint, self.pcblib.layers_by_id))

        for lib in self.svg.elements[2:]:
            lib['visibility'] = 'hidden'

        first_dimension = max_dimensions[0]
        scalar = 0.1
        width = max(first_dimension[0][0], first_dimension[0][1]) - min(first_dimension[0][0], first_dimension[0][1])
        height = max(first_dimension[1][0], first_dimension[1][1]) - min(first_dimension[1][0], first_dimension[1][1])
        self.svg['viewBox'] = '{} {} {} {}'.format(first_dimension[0][0] - (width * scalar),
                                                   first_dimension[1][0] - (height * scalar),
                                                   width + (2 * width * scalar),
                                                   height + (2 * height * scalar))

        return self.svg
