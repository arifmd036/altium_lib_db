import sys
import pathlib
import os
import argparse
import glob

repo_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, os.pardir)
sys.path.append(repo_path + '/_tools/library_parser')
sys.path.append(repo_path + '/_tools/partkeepr')
sys.path.append(repo_path + '/_tools/SVG_render')

from PcbLib.PcbLib import PcbLib
from SchLib.SchLib import SchLib
from SVG_PcbLib import SVG_PcbLib
from SVG_SchLib import SVG_SchLib


def render(source_file, destination_file):
    extension = pathlib.Path(source_file).suffix.lower()

    if extension == ".schlib":
        svg_schlib = SVG_SchLib(SchLib(source_file))
        svg = svg_schlib.render(destination_file)
        svg_schlib.save()
    elif extension == ".pcblib":
        svg_pcblib = SVG_PcbLib(PcbLib(source_file))
        svg = svg_pcblib.render(destination_file)
        svg.save()
    else:
        raise Exception("Incorrect file extension! {}".format(extension))



def is_valid_file(parser, arg):
    if not os.path.exists(arg):
        parser.error("The file %s does not exist!" % arg)
    else:
        return arg

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Altium to SVG renderer.')
    if len(sys.argv) == 2 and sys.argv[1] == "--all":
        # parse all libraries
        files = glob.glob(os.path.join(repo_path, "SCH", "*.SchLib"), recursive=True)
        output = os.path.join(repo_path, "svgs")
        if not os.path.exists(output):
            os.makedirs(output)
        
        for f in files:
            render(f, os.path.join(output, os.path.basename(f) + ".svg"))
        exit(0)

    parser.add_argument(dest="filename",
                        help="input file with two matrices", metavar="FILE",
                        type=lambda x: is_valid_file(parser, x))
    parser.add_argument(dest="out",
                        help="output file", metavar="OUT_FILE")
    args = parser.parse_args()
    # print(args)
    render(args.filename, args.out)
