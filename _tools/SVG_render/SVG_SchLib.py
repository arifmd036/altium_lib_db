import svgwrite
import string
import math
import os
from numpy import math
from SchLib.SchLib_Pin import Pin_ElectricalType
from SchLib.SchLib_Properties import *

LineWidth = [1, 12, 30, 50]
LineDash = [[0, 0, 0, 0],
            [2, 25, 60, 100],
            ['1 1', '1 24', '1 60', '1 100'],
            ['1 2 2 2', '1 25 25 25', '1 60 60 60', '1 100 100 100']]


def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy


def getApproximateArialStringWidth(st):
    size = 0  # in milinches
    for s in st:
        if s in 'lij|\' ':
            size += 37
        elif s in '![]fI.,:;/\\t':
            size += 50
        elif s in '`-(){}r"':
            size += 60
        elif s in '*^zcsJkvxy':
            size += 85
        elif s in 'aebdhnopqug#$L+<>=?_~FZT' + string.digits:
            size += 95
        elif s in 'BSPEAKVXY&UwNRCHD':
            size += 112
        elif s in 'QGOMm%W@':
            size += 135
        else:
            size += 50
    return size * 6 / 1000.0  # Convert to picas


class SVG_SchLib:
    def __init__(self, schlib):
        self.schlib = schlib
        self.svg = svgwrite.Drawing()

        self._min_x = 1e10
        self._max_x = -1e10

        self._min_y = 1e10
        self._max_y = -1e10

    def _bound_x(self, *values):
        for arg in values:
            self._min_x = min(self._min_x, arg)
            self._max_x = max(self._max_x, arg)

    def _bound_y(self, *values):
        for arg in values:
            self._min_y = min(self._min_y, arg)
            self._max_y = max(self._max_y, arg)

    def _render_text_label(self, canvas, text_label, fonts=None):
        if fonts is None:
            fonts = self.schlib.fonts
        my_font = None
        for font in fonts:
            if str(font.id) == text_label.font_id:
                my_font = font
                break
        if my_font is None:
            return

        dy = 0
        anchor = ""

        if text_label.justification in [TextJustification.left_bottom, TextJustification.middle_bottom,
                                        TextJustification.right_bottom]:
            dy = -2 * int(my_font.size)

        if text_label.justification in [TextJustification.left_center, TextJustification.middle_center,
                                        TextJustification.right_center]:
            dy = int(my_font.size) * 2.5

        if text_label.justification in [TextJustification.left_top, TextJustification.middle_top,
                                        TextJustification.right_top]:
            dy = int(my_font.size) * 8

        if text_label.justification in [TextJustification.left_bottom, TextJustification.left_center,
                                        TextJustification.left_top]:
            anchor = 'start'

        if text_label.justification in [TextJustification.middle_bottom, TextJustification.middle_center,
                                        TextJustification.middle_top]:
            anchor = 'middle'

        if text_label.justification in [TextJustification.right_bottom, TextJustification.right_center,
                                        TextJustification.right_top]:
            anchor = 'end'

        font_weight = ''
        if my_font.bold:
            font_weight = 'font-weight:bold;'

        font_decoration = ''
        if my_font.underline:
            font_decoration = 'text-decoration:underline'

        if my_font.strikeout:
            if font_decoration != '':
                font_decoration += ' line-through;'
            else:
                font_decoration = 'text-decoration:line-through;'

        font_italic = ''
        if my_font.italic:
            font_italic = 'font-style:italic;'

        if hasattr(text_label, 'dx'):
            dx = int(text_label.dx)
        else:
            dx = 0

        text = self.svg.text(text_label.text,
                             insert=(text_label.X, - text_label.Y),
                             fill=svgwrite.rgb(text_label.color.R, text_label.color.G, text_label.color.B),
                             font_size=(my_font.size * 8),
                             style=f"font-family:{my_font.font_name};text-anchor:{anchor};{font_weight}{font_decoration}{font_italic}",
                             transform=f'rotate({- my_font.rotation},{text_label.X}, {-text_label.Y})',
                             dy=[dy],
                             dx=[dx]
                             )

        if text_label.is_hidden:
            text['visibility'] = 'hidden'
        canvas.add(text)

        start_x = text_label.X
        start_y = -text_label.Y

        width = getApproximateArialStringWidth(text_label.text) * (my_font.size) * 8
        height = (my_font.size * 8)

        rot = -math.radians(my_font.rotation)
        # rotate bounding box the same as text
        x1, y1 = rotate([text_label.X, -text_label.Y], [start_x, start_y - height], rot)
        x2, y2 = rotate([text_label.X, -text_label.Y], [start_x + width, start_y - height], rot)
        x3, y3 = rotate([text_label.X, -text_label.Y], [start_x + width, start_y], rot)
        x4, y4 = rotate([text_label.X, -text_label.Y], [start_x, start_y], rot)

        self._bound_x(x1, x2, x3, x4)
        self._bound_y(y1, y2, y3, y4)

    def _render_designator(self, canvas, library):
        self._render_text_label(canvas, library.designator)

    def _render_parameters(self, canvas, library):
        for parameter in library.parameters:
            self._render_text_label(canvas, parameter)

    def _render_pin(self, canvas, pin):
        pin_canvas = self.svg.g()
        pin_canvas['class'] = "pin_" + pin.designator
        direction = 1
        stop_x = 0
        stop_y = 0

        class Font:
            def __init__(self):
                if pin.orientation in [0, 90]:
                    self.rotation = pin.orientation
                else:
                    self.rotation = pin.orientation - 180

                self.font_name = "Times New Roman"
                self.size = 10
                self.bold = False
                self.underline = False
                self.strikeout = False
                self.italic = False
                self.id = '1'

        class Designator:
            def __init__(self):
                if pin.orientation in [0, 90]:
                    self.dx = 80
                    self.justification = TextJustification.left_bottom
                else:
                    self.dx = -80
                    self.justification = TextJustification.right_bottom

                self.X = pin.X
                self.Y = pin.Y
                self.font_id = '1'
                self.text = pin.designator
                self.is_hidden = None if pin.pin_hidden == True else (not pin.is_designator_visible())

                class Color:
                    def __init__(self):
                        self.R = 0
                        self.G = 0
                        self.B = 0

                self.color = Color()

        class Description:
            def __init__(self):
                if pin.orientation in [0, 90]:
                    self.dx = -50
                    self.justification = TextJustification.right_center
                else:
                    self.dx = 50
                    self.justification = TextJustification.left_center

                self.X = pin.X
                self.Y = pin.Y
                self.font_id = '1'
                self.text = pin.display_name.replace('\\', u'\u0305')
                self.is_hidden = None if pin.pin_hidden == True else (not pin.is_name_visible())

                class Color:
                    def __init__(self):
                        self.R = 0
                        self.G = 0
                        self.B = 0

                self.color = Color()

        if pin.orientation == 180 or pin.orientation == 270:
            direction = -1

        if pin.orientation == 90 or pin.orientation == 270:
            stop_y = pin.Y + (pin.length * direction)
            stop_x = pin.X
        else:
            stop_x = pin.X + (pin.length * direction)
            stop_y = pin.Y

        self._render_text_label(pin_canvas, Designator(), [Font()])
        self._render_text_label(pin_canvas, Description(), [Font()])

        pin_canvas.add(self.svg.line((pin.X, - pin.Y), (stop_x, - stop_y),
                                     stroke=svgwrite.rgb(0, 0, 0),
                                     stroke_width='10'))

        OutputArrowCordinates = [5, -15, 5, 15, 50, 0]
        InputArrowCordinates = [50, -15, 50, 15, 5, 0]
        IOArrowCordinates = [55, -15, 55, 15, 100, 0]

        if pin.electrical_type in [Pin_ElectricalType.Input, Pin_ElectricalType.IO]:
            points = []
            if pin.orientation == 90 or pin.orientation == 270:
                for i in range(0, len(InputArrowCordinates), 2):
                    points.append((InputArrowCordinates[i + 1] * direction + pin.X,
                                   - InputArrowCordinates[i] * direction - pin.Y))
            else:
                for i in range(0, len(InputArrowCordinates), 2):
                    points.append((InputArrowCordinates[i] * direction + pin.X,
                                   InputArrowCordinates[i + 1] * direction - pin.Y))
            pin_canvas.add(self.svg.polygon(points=points,
                                            stroke=svgwrite.rgb(0, 0, 0),
                                            stroke_width='1',
                                            fill=svgwrite.rgb(255, 255, 255)))

        if pin.electrical_type == Pin_ElectricalType.Output:
            points = []
            if pin.orientation == 90:
                for i in range(0, len(OutputArrowCordinates), 2):
                    points.append((OutputArrowCordinates[i + 1] * (-1) + pin.X,
                                   OutputArrowCordinates[i] * (-1) - pin.Y))
            elif pin.orientation == 270:
                for i in range(0, len(OutputArrowCordinates), 2):
                    points.append((OutputArrowCordinates[i + 1] + pin.X,
                                   OutputArrowCordinates[i] - pin.Y))
            else:
                for i in range(0, len(OutputArrowCordinates), 2):
                    points.append((OutputArrowCordinates[i] * direction + pin.X,
                                   OutputArrowCordinates[i + 1] * direction - pin.Y))
            pin_canvas.add(self.svg.polygon(points=points,
                                            stroke=svgwrite.rgb(0, 0, 0),
                                            stroke_width='1',
                                            fill=svgwrite.rgb(255, 255, 255)))

        if pin.electrical_type == Pin_ElectricalType.IO:
            points = []
            if pin.orientation == 90:
                for i in range(0, len(IOArrowCordinates), 2):
                    points.append((IOArrowCordinates[i + 1] * (-1) + pin.X,
                                   IOArrowCordinates[i] * (-1) - pin.Y))
            elif pin.orientation == 270:
                for i in range(0, len(IOArrowCordinates), 2):
                    points.append((IOArrowCordinates[i + 1] + pin.X,
                                   IOArrowCordinates[i] - pin.Y))
            else:
                for i in range(0, len(IOArrowCordinates), 2):
                    points.append((IOArrowCordinates[i] * direction + pin.X,
                                   IOArrowCordinates[i + 1] * direction - pin.Y))
            pin_canvas.add(self.svg.polygon(points=points,
                                            stroke=svgwrite.rgb(0, 0, 0),
                                            stroke_width='1',
                                            fill=svgwrite.rgb(255, 255, 255)))

        if pin.pin_hidden == True:
            pin_canvas['visibility'] = 'hidden'

        canvas.add(pin_canvas)
        self._bound_x(pin.X, stop_x)
        self._bound_y(-pin.Y, -stop_y)

    def _render_rectangle(self, canvas, rectangle):
        loc_x = min(rectangle.X, rectangle.corner_X)
        loc_y = min(-rectangle.Y, -rectangle.corner_Y)

        width = abs(rectangle.X - rectangle.corner_X)
        height = abs(rectangle.Y - rectangle.corner_Y)

        rx = 0
        if hasattr(rectangle, 'corner_X_radius'):
            rx = rectangle.corner_X_radius

        ry = 0
        if hasattr(rectangle, 'corner_Y_radius'):
            ry = rectangle.corner_X_radius

        canvas.add(self.svg.rect(insert=(loc_x, loc_y),
                                 size=(width, height),
                                 fill=svgwrite.rgb(rectangle.area_color.R, rectangle.area_color.G,
                                                   rectangle.area_color.B),
                                 stroke=svgwrite.rgb(rectangle.color.R, rectangle.color.G, rectangle.color.B),
                                 stroke_width=LineWidth[rectangle.line_width.value],
                                 opacity=0.5 if rectangle.transparent else 1,
                                 rx=rx,
                                 ry=ry
                                 ))
        self._bound_x(loc_x, loc_x + width)
        self._bound_y(loc_y, loc_y + height)

    def _render_line(self, canvas, line):
        for i in range(len(line.X) - 1):
            canvas.add(self.svg.line((line.X[i], -line.Y[i]), (line.X[i + 1], -line.Y[i + 1]),
                                     stroke_linecap="round",
                                     stroke=svgwrite.rgb(line.color.R, line.color.G, line.color.B),
                                     stroke_width=LineWidth[line.line_width.value],
                                     stroke_dasharray=LineDash[line.line_style.value][line.line_width.value]
                                     ))

            self._bound_x(line.X[i], line.X[i + 1])
            self._bound_y(-line.Y[i], -line.Y[i + 1])

    def _render_bezier(self, canvas, bezier):
        if len(bezier.X) <= 3:
            return self._render_line(canvas, bezier)

        temp_points = []
        all_points = []
        for i in range(len(bezier.X)):
            self._bound_x(bezier.X[i])
            self._bound_y(-bezier.Y[i])
            temp_points.append([bezier.X[i], -bezier.Y[i]])
            if i % 3 == 0 and i != 0:
                all_points.append(temp_points)
                temp_points = temp_points[3:]
        for points in all_points:
            canvas.add(self.svg.path(d='M{},{} C{},{}, {},{}, {},{}'.format(points[0][0], points[0][1],
                                                                            points[1][0], points[1][1],
                                                                            points[2][0], points[2][1],
                                                                            points[3][0], points[3][1]),
                                     stroke_linecap="round",
                                     stroke=svgwrite.rgb(bezier.color.R, bezier.color.G, bezier.color.B),
                                     stroke_width=LineWidth[bezier.line_width.value],
                                     fill="none",
                                     ))

    def _render_polygon(self, canvas, polygon):
        points = []
        for i in range(len(polygon.X)):
            self._bound_x(polygon.X[i])
            self._bound_y(-polygon.Y[i])
            points.append([polygon.X[i], -polygon.Y[i]])

        canvas.add(self.svg.polygon(points=points,
                                    stroke=svgwrite.rgb(polygon.color.R, polygon.color.G, polygon.color.B),
                                    stroke_width=LineWidth[polygon.line_width.value],
                                    fill=svgwrite.rgb(polygon.area_color.R, polygon.area_color.G, polygon.area_color.B),
                                    stroke_linejoin="round",
                                    opacity=0.5 if polygon.transparent else 1
                                    ))

    def _render_arc(self, canvas, arc):
        self._bound_x(arc.X - arc.radius, arc.X + arc.radius)
        self._bound_y(-(arc.Y - arc.radius), -(arc.Y + arc.radius))

        if arc.start_angle == 0 and arc.end_angle == 360:
            canvas.add(self.svg.circle(center=(arc.X, -arc.Y),
                                       r=arc.radius,
                                       stroke=svgwrite.rgb(arc.color.R, arc.color.G, arc.color.B),
                                       stroke_width=LineWidth[arc.line_width.value],
                                       fill="none",
                                       ))
            return

        start_x = arc.X
        start_y = -arc.Y
        radius = arc.radius

        degree0 = arc.start_angle
        degree1 = arc.end_angle

        form = ""
        if (degree1 - degree0) < 0:
            form = "1,0"
        else:
            form = "0,0"

        radians0 = math.radians(degree0)
        radians1 = math.radians(degree1)
        dx0 = radius * (math.sin(radians0))
        dy0 = radius * (math.cos(radians0))
        dx1 = radius * (math.sin(radians1))
        dy1 = radius * (math.cos(radians1))

        m0 = start_x + dy0
        n0 = start_y - dx0
        m1 = -dy0 + dy1
        n1 = dx0 - dx1

        w = self.svg.path(d="M {0},{1} a {2},{2} 0 {5} {3},{4}".format(m0, n0, radius, m1, n1, form),
                          stroke=svgwrite.rgb(arc.color.R, arc.color.G, arc.color.B),
                          stroke_width=LineWidth[arc.line_width.value],
                          fill="none",
                          stroke_linecap="round",
                          )
        canvas.add(w)

    def _render_primitives(self, canvas, primitives):
        for primitive in primitives:
            if isinstance(primitive, Pin):
                self._render_pin(canvas, primitive)

            if isinstance(primitive, SchLib_Rectangle) or isinstance(primitive, SchLib_RoundedRectangle):
                self._render_rectangle(canvas, primitive)

            if isinstance(primitive, SchLib_TextLabel):
                self._render_text_label(canvas, primitive)

            if isinstance(primitive, SchLib_Line) or isinstance(primitive, SchLib_SimpleLine):
                self._render_line(canvas, primitive)

            if isinstance(primitive, SchLib_Bezier):
                self._render_bezier(canvas, primitive)

            if isinstance(primitive, SchLib_Polygon):
                self._render_polygon(canvas, primitive)

            if isinstance(primitive, SchLib_Arc):
                self._render_arc(canvas, primitive)

    def render(self, filename):
        self.dwg = svgwrite.Drawing(filename, style="background-color: white")

        for library in self.schlib.libraries:
            xml_id = library.name
            xml_id = xml_id.replace(',', '_')  # comma is not valid in SVG id
            lib_svg = self.dwg.g(id=xml_id)

            part_count = int(library.part_count)

            part_primitives = {}
            for i in range(1, part_count + 1):

                for primitive in library.primitives:
                    if primitive.owner_part_id == i:
                        if primitive.owner_part_id not in part_primitives:
                            part_primitives[primitive.owner_part_id] = [primitive]
                        else:
                            part_primitives[primitive.owner_part_id].append(primitive)

            for part_key in part_primitives:
                part_svg = self.dwg.g(id="Part_" + chr(part_key + 64))

                mode_primitives = {}

                for primitive in part_primitives[part_key]:
                    if primitive.owner_part_display_mode not in mode_primitives:
                        mode_primitives[primitive.owner_part_display_mode] = [primitive]
                    else:
                        mode_primitives[primitive.owner_part_display_mode].append(primitive)

                for display_mode_key in mode_primitives:
                    mode_svg = self.dwg.g(id="Alternate_" + str(display_mode_key), visibility='hidden')
                    self._render_primitives(mode_svg, mode_primitives[display_mode_key])
                    self._render_parameters(mode_svg, library)
                    self._render_designator(mode_svg, library)

                    part_svg.add(mode_svg)
                lib_svg.add(part_svg)
            self.dwg.add(lib_svg)

        width = self._max_x - self._min_x
        height = self._max_y - self._min_y

        scale = 0.1
        width += width * scale
        height += width * scale
        min_x = self._min_x - width * scale / 2
        min_y = self._min_y - height * scale / 2

        self.dwg['viewBox'] = f'{min_x} {min_y} {width} {height}'

        # second element is schlib
        assert type(self.dwg.elements[1]) == svgwrite.container.Group

        return self.dwg

    def get_parts_len(self):
        return len(self.dwg.elements[1].elements)

    def get_alternate_modes_len(self):
        return len(self.dwg.elements[1].elements[0].elements)
    
    def set_visible_part_and_alternate_mode(self, part: int, alternate_mode: int):
        part = self.dwg.elements[1].elements[part]
        mode = part.elements[alternate_mode]

        mode['visibility'] = 'visible'

    def set_hidden_part_and_alternate_mode(self, part: int, alternate_mode: int):
        part = self.dwg.elements[1].elements[part]
        mode = part.elements[alternate_mode]

        mode['visibility'] = 'hidden'

    def save(self):
        # by default, first part and first alternate mode will be visible
        self.set_visible_part_and_alternate_mode(0, 0)
        self.dwg.save()
        self.set_hidden_part_and_alternate_mode(0, 0)

        if self.get_parts_len() == 1 and \
            self.get_alternate_modes_len() == 1:
            return

        filename = self.dwg.filename

        for part in range(self.get_parts_len()):
            for mode in range(self.get_alternate_modes_len()):
                name = os.path.splitext(filename)[0]

                part_id = chr(part + ord('A'))
                name += f'_{part_id}_{mode}.svg'

                try:
                    self.set_visible_part_and_alternate_mode(part, mode)
                except IndexError as e:
                    print(f"Part {self.dwg.elements[1].get_id()} has empty part {part_id}, alternate mode {mode}.")
                    print("It doesn't make any sens to have empty part/alternate mode.")
                    print("Fix this please!")
                    exit(1)

                self.dwg.saveas(name)
                self.set_hidden_part_and_alternate_mode(part, mode)
