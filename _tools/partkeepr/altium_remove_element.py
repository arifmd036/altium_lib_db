import sys
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(dir_path + '/..')

import traceback
import time
from library_parser.AltiumDatabase.AltiumDatabase import AltiumDatabase
from actions import check_part_and_add_stock

if len(sys.argv) == 1:
    altium_element = "test"
    altium_quantity = 0
else:
    altium_element = sys.argv[1]
    altium_quantity = int(sys.argv[2])

_altium_database_class = AltiumDatabase()
database = _altium_database_class.database

element = None

for i in database:
    if i.part_number == altium_element:
        element = i
        break

try:
    mpn = element.manufacturer_part_number if element else None
    check_part_and_add_stock(mpn, -altium_quantity, altium_element)
except:
    traceback.print_exc()
    time.sleep(100)
