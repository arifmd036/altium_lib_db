import os
import sys
import shutil

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(dir_path + '/..')
from library_parser.AltiumDatabase.AltiumDatabase import AltiumDatabase


def basket_generator(altium_map, parts, supplier_name, supplier_index, separator):
    supplier_string = ''
    errors = ''
    for key, value in parts.items():
        if key in altium_map:
            supplier_part_number = altium_map[key].supplier_part_number[supplier_index]
            if not supplier_part_number or supplier_part_number == '':
                errors += key + " : " + value + "\t\tNot found " + supplier_name + " part number in database, but octopart found. Force add to csv file \n"
            else:
                supplier_string += str(supplier_part_number) + separator + str(value) + "\n"
        else:
            errors += key + "\t" + value + "\t\tNot found in database \n"

    return supplier_string, errors


def basket_other(parts, separator):
    supplier_string = ''
    errors = ''

    for key, value in parts.items():
        supplier_string += str(key) + separator + str(value) + "\n"

    return supplier_string, errors


def save_file(directory, supplier_name, data):
    supplier_string = data[0]
    errors = data[1]
    if supplier_string != '':
        with open(directory + '\\' + supplier_name + '_list.txt', 'w') as file:
            file.write(supplier_string)

    if errors != '':
        with open(directory + '\\' + supplier_name + '_errors.txt', 'w') as file:
            file.write(errors)


_altium_database_class = AltiumDatabase()
database = _altium_database_class.database


altium_map = {}
for i in database:
    altium_map[i.manufacturer_part_number] = i

input_file_path = str(sys.argv[1])

directory = os.path.dirname(input_file_path) + "\\Suppliers_Lists"

if os.path.exists(directory):
    shutil.rmtree(directory)

os.mkdir(directory)

tme_list = {}
mouser_list = {}
rs_list = {}
farnell_list = {}
digi_list = {}
other_list = {}
with open(input_file_path, 'r', errors='ignore') as _file:
    try:
        for line in _file:
            line_data = line.split(';')
            if line_data[7] == 'TME':
                tme_list[line_data[10]] = line_data[8]
            if line_data[7] == 'Mouser':
                mouser_list[line_data[10]] = line_data[8]
            if line_data[7] == 'RS Components':
                rs_list[line_data[10]] = line_data[8]
            if line_data[7] == 'Farnell':
                farnell_list[line_data[10]] = line_data[8]
            if line_data[7] == 'Digi-Key':
                digi_list[line_data[10]] = line_data[8]
            if line_data[7] == 'Other':
                other_list[line_data[10]] = line_data[8]
    except AssertionError as error:
        print(error)

        print("except")

save_file(directory, 'tme', basket_generator(altium_map, tme_list, 'TME', 0, ' '))
save_file(directory, 'rs', basket_generator(altium_map, rs_list, 'RS Components', 1, ','))
save_file(directory, 'farnell', basket_generator(altium_map, farnell_list, 'Farnell', 2, '\t'))
save_file(directory, 'mouser', basket_generator(altium_map, mouser_list, 'Mouser', 3, ' '))
save_file(directory, 'digi', basket_generator(altium_map, digi_list, 'Digi-Key', 4, ','))
save_file(directory, 'other', basket_other(other_list, ' '))

os.startfile(directory)
