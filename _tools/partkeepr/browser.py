import configparser
import keyboard
import re
import os
import threading
dir_path = os.path.dirname(os.path.realpath(__file__))

config = configparser.ConfigParser()
config.read(dir_path + '/../../config.ini')

import selenium.common.exceptions
from selenium import webdriver
import time
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import traceback
from tk_messages import select_mode, part_action, steal_focus
from tkinter.messagebox import showerror
from selenium.webdriver.remote.webdriver import WebDriver
from tempfile import gettempdir
import pywinauto
import ctypes


def attach_to_session(executor_url, session_id):
    original_execute = WebDriver.execute
    def new_command_execute(self, command, params=None):
        if command == "newSession":
            # Mock the response
            return {'success': 0, 'value': None, 'sessionId': session_id}
        else:
            return original_execute(self, command, params)
    # Patch the function before creating the driver object
    WebDriver.execute = new_command_execute
    driver = webdriver.Remote(command_executor=executor_url, desired_capabilities={})
    driver.session_id = session_id
    # Replace the patched function with original function
    WebDriver.execute = original_execute
    return driver


class PartKeeprWeb:
    def background_check(self):
        try:
            # print("URL now:", self.wd.current_url)
            pass
        except:
            self.wd.quit()
            os.remove(gettempdir() + '/altium_chrome_session.txt')

        threading.Timer(2, self.background_check).start()

    def __init__(self):
        chrome_options = Options()
        chrome_options.add_argument("--start-maximized")
        try:
            with open(gettempdir() + '/altium_chrome_session.txt', 'r') as file:
                content = file.read().split('\n')

            executor_url = content[0]
            session_id = content[1]
            self.wd = attach_to_session(executor_url, session_id)

            print("URL now:", self.wd.current_url)
        except:
            try:
                self.wd = webdriver.Chrome(ChromeDriverManager(path=dir_path + '/webdrivers').install(), options=chrome_options)
                self.wd.get(config["partkeepr"]["url"])

                try:
                    show_window = ctypes.windll.user32.ShowWindow
                    window = pywinauto.findwindows.find_window(title_re='.+chromedriver.exe')
                    show_window(window, False)
                except:
                    pass
                    # showerror('', traceback.format_exc())

                executor_url = self.wd.command_executor._url
                session_id = self.wd.session_id

                print(executor_url)
                print(session_id)
                with open(gettempdir() + '/altium_chrome_session.txt', 'w') as file:
                    file.write(executor_url + '\n')
                    file.write(session_id)

                self.wd.implicitly_wait(30)
                threading.Timer(1, self.background_check).start()
                self.login()
            except:
                traceback.print_exc()
                showerror("Chrome not installed.", "Chrome is required to run.\nInstall chrome browser!")
                exit(1)

    def login(self):
        self.wd.find_element_by_xpath('//*[@id="textfield-1081-inputEl"]').send_keys(config["partkeepr"]["user"])
        self.wd.find_element_by_xpath('//*[@id="textfield-1082-inputEl"]').send_keys(config["partkeepr"]["pwd"])
        self.wd.find_element_by_xpath('//*[@id="button-1084"]').click()

    def add_part(self,
                 name,
                 initial_stock_level):
        self.wd.find_element_by_xpath('//*[text()="Add Part"]').click()
        time.sleep(1)

        self.main_window = self.wd.find_elements_by_css_selector('.x-window.x-layer.x-window-default.x-closable.x-window-closable.x-window-default-closable.x-border-box.x-resizable.x-window-resizable.x-window-default-resizable.x-unselectable')
        assert len(self.main_window) == 1
        self.main_window = self.main_window[0]
        main_window_id = self.main_window.get_attribute('id')

        id = int(re.sub('[^0-9]', '', main_window_id))
        print("Main window ID:", id)

        self.wd.find_element_by_id('textfield-{}-inputEl'.format(id + 2)).send_keys(name)
        self.wd.find_element_by_id('numberfield-{}-inputEl'.format(id + 54)).send_keys(str(initial_stock_level))

        self.wd.find_element_by_id('checkbox-{}-inputEl'.format(id + 71)).click()

        self.wd.find_element_by_id('button-{}-btnInnerEl'.format(id + 83)).click()

        windows = self.wd.find_elements_by_css_selector(
            '.x-window.x-layer.x-window-default.x-closable.x-window-closable.x-window-default-closable.x-border-box.x-resizable.x-window-resizable.x-window-default-resizable.x-unselectable')
        assert len(windows) == 2
        ids = [int(re.sub('[^0-9]', '', i.get_attribute('id'))) for i in windows]
        if ids[0] == id:
            octopart_id = ids[1]
        else:
            octopart_id = ids[0]
        print("Octopart window ID:", octopart_id)

        self.wd.implicitly_wait(5)
        try:
            self.wd.find_element_by_xpath('//*[@id="tableview-{}"]/div/table[1]/tbody/tr[1]/td[1]'.format(octopart_id + 11)).click()
            self.wd.find_element_by_id('button-{}-btnInnerEl'.format(octopart_id + 12)).click()
        except selenium.common.exceptions.NoSuchElementException:
            self.wd.find_element_by_id('tool-1439-toolEl'.format(octopart_id + 34)).click()

        self.wd.implicitly_wait(30)

    def find_part(self, name):
        self.wd.maximize_window()
        steal_focus(pywinauto.findwindows.find_window(title='PartKeepr - Google Chrome'))
        searchfield = self.wd.find_elements_by_xpath("//*[contains(@id,'partkeepr-searchfield-')]")
        trigger = None
        for i in searchfield:
            if i.get_attribute('id').find('-trigger-search') != -1:
                trigger = i
            if i.get_attribute('id').find('-inputEl') != -1:
                i.clear()
                i.send_keys(name)

        trigger.click()


partkeepr_web = PartKeeprWeb()


if __name__ == "__main__":
    time.sleep(10)
    partkeepr_web.find_part('led')
    time.sleep(10)
    partkeepr_web.find_part('kaktus')
    time.sleep(10)
    partkeepr_web.add_part("testowy part", 10)

    time.sleep(1000)
