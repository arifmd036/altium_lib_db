


Function _find_altium_path() : String;
Var
    LibType         : TLibraryType;
    AltiumLibPath   : String;
    Path            : String;
    i               : Integer;
    IntMan          : IIntegratedLibraryManager;
Begin
    IntMan := IntegratedLibraryManager;
    If IntMan = Nil Then Exit;
    AltiumLibPath := '';

    for i := 0 To IntMan.AvailableLibraryCount-1 Do
    Begin
        LibType := IntMan.AvailableLibraryType(i);
        Path := ExtractFilePath(IntMan.AvailableLibraryPath(i));
        if DirectoryExists(Path + '_tools\partkeepr') Then
        begin
            AltiumLibPath := Path;
            break
        end
    End;
    if AltiumLibPath = '' Then
    begin
        ShowMessage('Altium library not found!');
        Exit;
    end;
    Result := AltiumLibPath;
End;


Procedure RunWSN_TOOLS;
Begin
     WSN_TOOLS.ShowModal;
End;

procedure TWSN_TOOLS.ScannerClick(Sender: TObject);
Var
   AltiumLibPath: String;

begin
   AltiumLibPath := _find_altium_path();
   RunApplication(AltiumLibPath + '_tools\library\python\python-3.11.1.amd64\python.exe ' + AltiumLibPath + '_tools\partkeepr\barcode_scanner.py');

end;

procedure TWSN_TOOLS.SImpleScannerClick(Sender: TObject);
Var
   AltiumLibPath: String;

begin
   AltiumLibPath := _find_altium_path();
   RunApplication(AltiumLibPath + '_tools\library\python\python-3.11.1.amd64\python.exe ' + AltiumLibPath + '_tools\partkeepr\simple_barcode_scanner.py');

end;


procedure CopyComponentLoction;
Var
   AltiumLibPath: String;

begin
   AltiumLibPath := _find_altium_path();
   RunApplication(AltiumLibPath + '_tools\library\python\python-3.11.1.amd64\python.exe ' + AltiumLibPath + '_tools\_manual_tools\copy_component_location.py -c');

end;


