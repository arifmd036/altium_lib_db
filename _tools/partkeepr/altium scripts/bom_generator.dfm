object BOM_FORM: TBOM_FORM
  Left = 0
  Top = 0
  Caption = 'BOM generator'
  ClientHeight = 309
  ClientWidth = 374
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 360
    Height = 40
    AutoSize = False
    Caption = 'BOM GENERATOR'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    GlowSize = 3
    ParentFont = False
  end
  object new_name_edit: TLabel
    Left = 8
    Top = 56
    Width = 5
    Height = 19
    Caption = ' '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object bom_path: TLabel
    Left = 8
    Top = 96
    Width = 5
    Height = 19
    Caption = ' '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 40
    Width = 55
    Height = 13
    Caption = 'BOM name:'
  end
  object Label3: TLabel
    Left = 8
    Top = 80
    Width = 51
    Height = 13
    Caption = 'BOM path:'
  end
  object Button1: TButton
    Left = 8
    Top = 136
    Width = 75
    Height = 25
    Caption = 'New BOM'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 8
    Top = 176
    Width = 208
    Height = 25
    Caption = 'Add current PCB to BOM'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 8
    Top = 208
    Width = 72
    Height = 25
    Caption = 'Parse BOM'
    TabOrder = 2
    OnClick = Button3Click
  end
  object CSVParse: TButton
    Left = 8
    Top = 240
    Width = 208
    Height = 25
    Caption = 'Generate baskets form CSV'
    TabOrder = 3
    OnClick = CSVParseClick
  end
  object VerifyOrder: TButton
    Left = 8
    Top = 272
    Width = 208
    Height = 25
    Caption = 'Verify order'
    TabOrder = 4
    OnClick = VerifyOrderClick
  end
  object Button4: TButton
    Left = 88
    Top = 208
    Width = 128
    Height = 25
    Caption = 'Parse BOM no similar'
    TabOrder = 5
    OnClick = ParseBOMNoSimilar
  end
end
