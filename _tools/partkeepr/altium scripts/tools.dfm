object WSN_TOOLS: TWSN_TOOLS
  Left = 0
  Top = 0
  Caption = 'WSN TOOLS'
  ClientHeight = 125
  ClientWidth = 217
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Scanner: TButton
    Left = 21
    Top = 29
    Width = 171
    Height = 25
    Caption = 'Run Baracode Scanner'
    TabOrder = 0
    OnClick = ScannerClick
  end
  object SImpleScanner: TButton
    Left = 21
    Top = 69
    Width = 171
    Height = 25
    Caption = 'Run Simple Baracode Scanner'
    TabOrder = 1
    OnClick = SImpleScannerClick
  end
end
