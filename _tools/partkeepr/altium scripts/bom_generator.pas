Var
   currentBOMTarget : String;
   configFilePath : String;
    BOMDirPath : String;



Function _find_altium_path() : String;
Var
    LibType         : TLibraryType;
    AltiumLibPath   : String;
    Path            : String;
    i               : Integer;
    IntMan          : IIntegratedLibraryManager;
Begin
    IntMan := IntegratedLibraryManager;
    If IntMan = Nil Then Exit;
    AltiumLibPath := '';

    for i := 0 To IntMan.AvailableLibraryCount-1 Do
    Begin
        LibType := IntMan.AvailableLibraryType(i);
        Path := ExtractFilePath(IntMan.AvailableLibraryPath(i));
        if DirectoryExists(Path + '_tools\partkeepr') Then
        begin
            AltiumLibPath := Path;
            break
        end
    End;
    if AltiumLibPath = '' Then
    begin
        ShowMessage('Altium library not found!');
        Exit;
    end;
    Result := AltiumLibPath;
End;


Procedure Add_Components_To_BOM;
Var
    CurrentPCBBoard : IPCB_Board;
    Iterator        : IPCB_BoardIterator;
    Part            : IPCB_Component;
    NrOfParts       : Integer;
    myFile          : TextFile;
    AltiumLibPath   : String;
    TempPath        : String;
    BOM_directory   : String;
    BoardName : String;
Begin
    CurrentPCBBoard := PCBServer.GetCurrentPCBBoard;

    BoardName := extractfilename(CurrentPCBBoard.GetState_FileName);
    BoardName := ChangeFileExt(BoardName, '.txt');

    Iterator := CurrentPCBBoard.BoardIterator_Create;
    Iterator.AddFilter_ObjectSet(MkSet(eComponentObject));

    AltiumLibPath := _find_altium_path();

    AssignFile(myFile, BOMDirPath + '\' + BoardName);
    ReWrite(myFile);

    Part := Iterator.FirstPCBObject;
    While Part <> Nil Do
    Begin
        WriteLn(myFile, Part.SourceDesignator + ' ' + Part.SourceLibReference + ' "' + Part.Comment.Text + '"');

        Part := Iterator.NextPCBObject;
    End;
    CloseFile(myFile);
End;


Procedure Parse_BOM;
uses FileUtil;
Var
   TempPath        : String;
   BOM_directory   : String;
   pcbNamesList    : String;
   path            : string;
   SR              : TSearchRec;
   PascalFiles: TStringList;
   AltiumLibPath   : String;
Begin
     AltiumLibPath := _find_altium_path();
     RunApplication(AltiumLibPath + '_tools\library\python\python-3.11.1.amd64\python.exe ' + AltiumLibPath + '_tools\partkeepr\micro_bom_2.py' + ' "' + BOMDirPath + '"');
End;


Procedure Parse_BOM_No_Similar;
uses FileUtil;
Var
   TempPath        : String;
   BOM_directory   : String;
   pcbNamesList    : String;
   path            : string;
   SR              : TSearchRec;
   PascalFiles: TStringList;
   AltiumLibPath   : String;
Begin
     AltiumLibPath := _find_altium_path();
     RunApplication(AltiumLibPath + '_tools\library\python\python-3.11.1.amd64\python.exe ' + AltiumLibPath + '_tools\partkeepr\micro_bom_2.py' + ' "' + BOMDirPath + '" --no-similar');
End;


Procedure disable_window;
Begin
     new_name_edit.Enabled := False;
     Button2.Enabled := False;
     Button3.Enabled := False;
End;

Procedure enable_window;
Begin
     new_name_edit.Enabled := True;
     Button2.Enabled := True;
     Button3.Enabled := True;
End;

Procedure RunBOM_GENERATOR;
Var
   configFile : TextFile;
Begin
     configFilePath := SpecialFolder_TemporarySlash + 'AltiumBOMs\currentTargetFile.conf';

     if FileExists(configFilePath) then
     begin
          AssignFile(configFile, configFilePath );
          Reset(configFile);

           ReadLn(configFile, currentBOMTarget);
           CloseFile(configFile);
           new_name_edit.Caption := currentBOMTarget;
           enable_window();
           BOMDirPath := SpecialFolder_TemporarySlash + 'AltiumBOMs\' + currentBOMTarget;
           bom_path.Caption := BOMDirPath;
     end else begin
         new_name_edit.Caption := 'undefined';
         disable_window();
     end;
     BOM_FORM.ShowModal;
End;



procedure TBOM_FORM.Button1Click(Sender: TObject);
Var
   configFile     : TextFile;
   configFilePath : String;
begin
     currentBOMTarget := InputBox('Type new BOM name', 'Name', '');

     if currentBOMTarget = '' then
     begin
          showMessage('It is not valid name!');
     end else
     begin
          configFilePath := SpecialFolder_TemporarySlash + 'AltiumBOMs\currentTargetFile.conf';
          CreateDir(SpecialFolder_TemporarySlash + 'AltiumBOMs');
          // currentBOMTarget := 'BOM_' + DateToStr(date);
          CreateDir(SpecialFolder_TemporarySlash + 'AltiumBOMs\' + currentBOMTarget);

          AssignFile(configFile, configFilePath );
          ReWrite(configFile);
          WriteLn(configFile, currentBOMTarget);
          CloseFile(configFile);
          new_name_edit.Caption := currentBOMTarget;
          enable_window();
          BOMDirPath := SpecialFolder_TemporarySlash + 'AltiumBOMs\' + currentBOMTarget;
          bom_path.Caption := BOMDirPath;
     end;                                
end;


procedure TBOM_FORM.Button2Click(Sender: TObject);
begin
         Add_Components_To_BOM();
         showMessage('Done');
end;


procedure TBOM_FORM.Button3Click(Sender: TObject);
begin
            Parse_BOM();
end;


procedure TBOM_FORM.ParseBOMNoSimilar(Sender: TObject);
begin
            Parse_BOM_No_Similar();
end;

procedure TBOM_FORM.CSVParseClick(Sender: TObject);
Var
   openDialog: TOpenDialog;
   filePath: String;
   AltiumLibPath: String;
begin
     openDialog := TOpenDialog.Create(self);
     openDialog.InitialDir := 'C:\';

     // Allow only .csv files to be selected
     openDialog.Filter := 'CSV files (*.csv)|*.csv';
     // Display the open file dialog
     if openDialog.Execute
     then begin
          filePath := openDialog.FileName;
     end else
     begin
          openDialog.Free;
          Exit;
     end;
     // Free up the dialog
     openDialog.Free;

     AltiumLibPath := _find_altium_path();
     RunApplication(AltiumLibPath + '_tools\library\python\python-3.11.1.amd64\python.exe ' + AltiumLibPath + '_tools\partkeepr\csv_parser.py' + ' "' + filePath + '"');
end;



procedure TBOM_FORM.VerifyOrderClick(Sender: TObject);
Var
   openDialog: TOpenDialog;
   filePath: String;
   AltiumLibPath: String;
begin
     openDialog := TOpenDialog.Create(self);
     openDialog.InitialDir := 'C:\';

     // Allow only .csv files to be selected
     openDialog.Filter := 'CSV files (*.csv)|*.csv';
     // Display the open file dialog
     if openDialog.Execute
     then begin
          filePath := openDialog.FileName;
     end else
     begin
          openDialog.Free;
          Exit;
     end;
     // Free up the dialog
     openDialog.Free;

     AltiumLibPath := _find_altium_path();
     RunApplication(AltiumLibPath + '_tools\library\python\python-3.11.1.amd64\python.exe ' + AltiumLibPath + '_tools\partkeepr\verify_orders.py' + ' "' + filePath + '"');

end;

