Function _find_altium_path() : String;
Var
    LibType         : TLibraryType;
    AltiumLibPath   : String;
    Path            : String;
    i               : Integer;
    IntMan          : IIntegratedLibraryManager;
Begin
    IntMan := IntegratedLibraryManager;
    If IntMan = Nil Then Exit;
    AltiumLibPath := '';

    for i := 0 To IntMan.AvailableLibraryCount-1 Do
    Begin
        LibType := IntMan.AvailableLibraryType(i);
        Path := ExtractFilePath(IntMan.AvailableLibraryPath(i));
        if DirectoryExists(Path + '_tools\partkeepr') Then
        begin
            AltiumLibPath := Path;
            break
        end
    End;
    if AltiumLibPath = '' Then
    begin
        ShowMessage('Altium library not found!');
        Exit;
    end;
    Result := AltiumLibPath;
End;

// ----------------------------------------------------------------------------------------------------------

Function _find_selected(layer : TLayer) : String;
Var
    CurrentPCBBoard : IPCB_Board;
    Iterator        : IPCB_BoardIterator;
    Part            : IPCB_Component;
    NameOfElement   : String;
Begin
    CurrentPCBBoard := PCBServer.GetCurrentPCBBoard;
    Iterator := CurrentPCBBoard.BoardIterator_Create;
    Iterator.AddFilter_ObjectSet(MkSet(eComponentObject));
    Iterator.AddFilter_LayerSet(MkSet(layer));

    NameOfElement := '';

    Part := Iterator.FirstPCBObject;
    While Part <> Nil Do
    Begin
        If Part.Selected = True Then
        Begin
            NameOfElement := Part.SourceLibReference;
            Break;
        End;
        Part := Iterator.NextPCBObject;
    End;

    Result := NameOfElement;
End;


Function _find_layer_of_selected() : TLayer;
Var
    CurrentPCBBoard : IPCB_Board;
    Iterator        : IPCB_BoardIterator;
    Part            : String;
    layer           : TLayer;
Begin
    layer := eTopLayer;
    Part := _find_selected(layer);

    if Part = '' Then
    begin
         layer := eBottomLayer;
         Part := _find_selected(layer);
         if Part = '' Then
         begin
              ShowMessage('No part selected!');
              Exit
         end;
    end;
    Result := layer;
End;

// ----------------------------------------------------------------------------------------------------------

Function _highlight_and_count(layer : TLayer, Selected : String) : Integer;
Var
    CurrentPCBBoard : IPCB_Board;
    Iterator        : IPCB_BoardIterator;
    Part            : IPCB_Component;
    NrOfParts       : Integer;
Begin
    CurrentPCBBoard := PCBServer.GetCurrentPCBBoard;
    Iterator := CurrentPCBBoard.BoardIterator_Create;
    Iterator.AddFilter_ObjectSet(MkSet(eComponentObject));
    Iterator.AddFilter_LayerSet(MkSet(layer));

    Part := Iterator.FirstPCBObject;
    NrOfParts := 0;
    While Part <> Nil Do
    Begin
         If Part.SourceLibReference = Selected Then
         Begin
              CurrentPCBBoard.AddObjectToHighlightObjectList(Part);
              NrOfParts := NrOfParts+1;
              Part.Selected := True;
         End;

         Part := Iterator.NextPCBObject;
    End;

    // highlight
    CurrentPCBBoard.SetState_Navigate_HighlightObjectList (eHighlight_Zoom, False);
    ResetParameters;
    AddStringParameter('Action', 'Filtered');
    RunProcess('PCB:Zoom');

    Result := NrOfParts;
End;

// ----------------------------------------------------------------------------------------------------------

Procedure find_and_partkeepr;
var
    CurrentPCBBoard : IPCB_Board;
    NrOfParts       : Integer;
    layer           : TLayer;
    Part            : String;
    LibType         : TLibraryType;
    AltiumLibPath   : String;
Begin
    CurrentPCBBoard := PCBServer.GetCurrentPCBBoard;
    If CurrentPCBBoard = Nil Then Exit;

    AltiumLibPath := _find_altium_path();
    layer := _find_layer_of_selected();
    Part := _find_selected(layer);
    if Part = '' Then
        Exit;
    NrOfParts := _highlight_and_count(layer, Part);

    // showMessage(FloatToStr(NrOfParts) + '  ' + Part);

    //RunApplication(AltiumLibPath + '_tools\library\python\python-3.11.1.amd64\pythonw.exe ' + AltiumLibPath + '_tools\partkeepr\altium_remove_element.py ' + Part + ' ' + FloatToStr(NrOfParts) + ' ' + CurrentModuleName);

    RunApplication(AltiumLibPath + '_tools\library\python\python-3.11.1.amd64\pythonw.exe ' + AltiumLibPath + '_tools\partkeepr\tk_remove_part.py ' + Part + ' ' + FloatToStr(NrOfParts));
End;

Procedure remove_selected;
var
    CurrentPCBBoard : IPCB_Board;
    Iterator        : IPCB_BoardIterator;
    layer           : TLayer;
    Part            : IPCB_Component;
Begin
    CurrentPCBBoard := PCBServer.GetCurrentPCBBoard;
    If CurrentPCBBoard = Nil Then Exit;

    layer := _find_layer_of_selected();
    Part := _find_selected(layer);

    Iterator := CurrentPCBBoard.BoardIterator_Create;
    If Iterator = Nil Then Exit;
    Iterator.AddFilter_ObjectSet(MkSet(eComponentObject));

    Try
       PCBServer.PreProcess;

        Part := Iterator.FirstPCBObject;
        While Part <> Nil Do
        Begin
             If Part.Selected = True Then
             Begin
                  CurrentPCBBoard.RemovePCBObject(Part);
             End;

             Part := Iterator.NextPCBObject;
        End;
    Finally
        PCBServer.PostProcess;
    End;

    Client.SendMessage('PCB:RunQuery', 'Clear=True' , 255, Client.CurrentView);

    ResetParameters;
    AddStringParameter('Action', 'All');
    RunProcess('PCB:Zoom');

    CurrentPCBBoard.ViewManager_FullUpdate;
    Client.SendMessage('PCB:Zoom', 'Action=Redraw' , 255, Client.CurrentView);
End;

// ----------------------------------------------------------------------------------------------------------

Procedure BOM_Parse_Partkeepr;
Var
    CurrentPCBBoard : IPCB_Board;
    Iterator        : IPCB_BoardIterator;
    Part            : IPCB_Component;
    NrOfParts       : Integer;
    myFile          : TextFile;
    AltiumLibPath   : String;
Begin
    CurrentPCBBoard := PCBServer.GetCurrentPCBBoard;
    Iterator := CurrentPCBBoard.BoardIterator_Create;
    Iterator.AddFilter_ObjectSet(MkSet(eComponentObject));

    AltiumLibPath := _find_altium_path();

    AssignFile(myFile, AltiumLibPath + 'tmp.txt');
    ReWrite(myFile);

    Part := Iterator.FirstPCBObject;
    While Part <> Nil Do
    Begin
        WriteLn(myFile, Part.SourceDesignator + ' ' + Part.SourceLibReference);

        Part := Iterator.NextPCBObject;
    End;
    CloseFile(myFile);

    RunApplication(AltiumLibPath + '_tools\library\python\python-3.11.1.amd64\python.exe ' + AltiumLibPath + '_tools\partkeepr\micro_bom.py');
End;

Procedure flip_elements;
Var
    CurrentPCBBoard : IPCB_Board;
    Iterator        : IPCB_BoardIterator;
    Part            : IPCB_Component;
    Part1           : IPCB_Component;
    Part2           : IPCB_Component;
    layer           : TLayer;

    X1 : Integer;
    Y1 : Integer;

    X2 : Integer;
    Y2 : Integer;
begin
     X1 := 0;
     Y1 := 0;

     X2 := 0;
     Y2 := 0;
     Part1 := Nil;
     Part2 := Nil;

     layer := _find_layer_of_selected();

     CurrentPCBBoard := PCBServer.GetCurrentPCBBoard;
     Iterator := CurrentPCBBoard.BoardIterator_Create;
     Iterator.AddFilter_ObjectSet(MkSet(eComponentObject));
     Iterator.AddFilter_LayerSet(MkSet(layer));

     If CurrentPCBBoard = Nil Then Exit;

     Part := Iterator.FirstPCBObject;
     While Part <> Nil Do
     Begin
          If Part.Selected = True Then
          Begin
               Part1 := Part;

               X1 := Part.X;
               Y1 := Part.Y;
               Part := Iterator.NextPCBObject;
               Break;
          End;  
          Part := Iterator.NextPCBObject;
    End;

    While Part <> Nil Do
     Begin
          If Part.Selected = True Then
          Begin
               Part2 := Part;
               X2 := Part.X;
               Y2 := Part.Y;


               Part2.X := X1;
               Part2.Y := Y1;

               Part1.X := X2;
               Part1.Y := Y2;

               Part2.SetState_xSizeySize();
               Part1.SetState_xSizeySize();

               Break;
          End;
          Part := Iterator.NextPCBObject;
    End;
end;
