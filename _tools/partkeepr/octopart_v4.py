import sys

from six.moves import urllib
import json
import os

from termcolor import colored

main_suppliers_list = ['TME', 'RS Components', 'Farnell', 'Mouser', 'Digi-Key', 'LCSC']
extended_suppliers_list = ['RS (Formerly Allied Electronics)', 'DigiKey', 'Maritex', 'Arrow Electronics', 'Abacus Technologies', 'Avnet']
token = ''


# copy pasted from: https://github.com/prisma-labs/python-graphql-client/blob/master/graphqlclient/client.py
class GraphQLClient:
    def __init__(self, endpoint):
        self.endpoint = endpoint
        self.token = None
        self.headername = None

    def execute(self, query, variables=None):
        return self._send(query, variables)

    def inject_token(self, token, headername='token'):
        self.token = token
        self.headername = headername

    def _send(self, query, variables):
        data = {'query': query,
                'variables': variables}
        headers = {'Accept': 'application/json',
                   'Content-Type': 'application/json'}

        if self.token is not None:
            headers[self.headername] = '{}'.format(self.token)

        req = urllib.request.Request(self.endpoint, json.dumps(data).encode('utf-8'), headers)

        try:
            response = urllib.request.urlopen(req)
            return response.read().decode('utf-8')
        except urllib.error.HTTPError as e:
            print((e.read()))
            print('')
            raise e


def match_prices(client, mpn, manufacturer=None):
    # https://octopart.com/api/v4/reference
    dsl = '''
    query {
      # The playground will complain about missing required search arguments,
      # however, these are given default values in the schema.
      # Look at the "DOCS" tab on the right side of the screen for more details.
      multi_match(queries: [{mpn: "''' + str(mpn) + '''"}]) {
        hits
        reference
        parts {
          median_price_1000 {
            price
            currency
          }
          mpn
          manufacturer {
            name
          }
          short_description
          # best_datasheet {
          #   url
          # }
          # Brokers are non-authorized dealers. See: https://octopart.com/authorized
          sellers(include_brokers: true) {
            company {
              name
            }
            offers {
              sku
              click_url
              inventory_level
              packaging
              moq
              prices {
                price
                currency
                quantity
              }
            }
          }
        }
      }
    }
    '''
    resp = client.execute(dsl)
    resp = json.loads(resp)['data']['multi_match'][0]
    print("Resp [", mpn, "]:", resp)
    if resp['hits'] == 0:
      return []
    if resp['hits'] > 1:
        # some characters such as '-' are skipped during search - make sure MPN is correct
        resp['parts'] = [i for i in resp['parts'] if i['mpn'] == mpn]
        resp['hits'] = len(resp['parts'])
    if resp['hits'] > 1 and manufacturer:
        # sometimes MPN can be the same across manufacturers - chose one with most sellers, as usually other is garbage
        parts = [(len(i['sellers']), i) for i in resp['parts']]
        parts = sorted(parts, key=lambda tup: tup[0], reverse=True)
        selected_part = parts[0][1]
        print("Selected Manufacturer: ", selected_part['manufacturer']['name'])
        resp['parts'] = [selected_part]
        resp['hits'] = len(resp['parts'])

    part_stock = {}

    for part in resp['parts']:
        part_availability = 0
        for seller in part['sellers']:
            if len(seller['offers']) > 0:
                part_availability += seller['offers'][0]['inventory_level']
        part_stock[part_availability] = part

    part_stock = sorted(part_stock.items(), key=lambda x: x[0], reverse=True)

    return [part_stock[0][1]]


class OctopartElement:
    def __init__(self, mpn, manufacturer=None, token=None):
        if token:
            set_token(token)

        self.mpn = ''
        self.manufacturer = ''
        self.short_description = ''
        self.sellers = []
        self.sellers_by_name = dict()
        self.datasheet = ''

        self.octopart_fill(mpn, manufacturer=manufacturer)
        self.remove_multiple_skus_from_the_same_supplier()


    def octopart_fill(self, mpn, manufacturer=None):
        assert token != ''
        client = GraphQLClient('https://octopart.com/api/v4/endpoint')
        client.inject_token(token)

        prices = match_prices(client, mpn, manufacturer=manufacturer)
        if len(prices) == 0:
            return

        assert len(prices) == 1
        part = prices[0]

        self.mpn = part['mpn']
        self.short_description = part['short_description']
        self.manufacturer = part['manufacturer']['name']
        try:
            self.datasheet = part['best_datasheet']['url']
        except:
            pass

        for seller in part['sellers']:
            name = seller['company']['name']
            if (name in main_suppliers_list) or (name in extended_suppliers_list):
                self.sellers.append(seller)

    def remove_multiple_skus_from_the_same_supplier(self):
        # sellers_to_remove = []
        for seller in self.sellers:
            if len(seller['offers']) == 1:
                # with only one offer - do not remove anything
                continue
            
            # remove when more than 100 items are required to buy
            seller['offers'][:] = [tup for tup in seller['offers'] if type(tup['moq']) != int or tup['moq'] <= 100]

            # how many offers with 'Cut Tape' packaging?
            offers_with_cut_tape = [i['packaging'] for i in seller['offers']].count('Cut Tape')

            if offers_with_cut_tape == 0:
                # do nothing, as we cannot determine which is better
                continue
            elif offers_with_cut_tape == 1:
                seller['offers'][:] = [tup for tup in seller['offers'] if tup['packaging'] == 'Cut Tape']
            else:
                assert False, "Cannot reduce offers from " + str(seller['company']['name'])
            
        self.sellers = [i for i in self.sellers if len(i['offers']) > 0]

        for i in self.sellers:
            self.sellers_by_name[i['company']['name']] = i['offers']


def set_token(tok: str):
    global token
    token = tok


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage: <app name> <octopart api token v4>')
        input()
        exit(0)
    else:
        token = str(sys.argv[1])
        set_token(token)
        element = OctopartElement(sys.argv[2])

        for supplier in main_suppliers_list:
            print(supplier, end=': ')
            if supplier in element.sellers_by_name:
                print(element.sellers_by_name[supplier][0]['sku'])
        # OctopartElement('')
