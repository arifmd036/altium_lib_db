import configparser
import sys
import os
import requests
from requests.auth import HTTPBasicAuth
import re
from decimal import Decimal

config = configparser.ConfigParser()
dir_path = os.path.dirname(os.path.realpath(__file__))
config.read(dir_path + '/../../config.ini')

sys.path.append(dir_path + '/..')
from library_parser.AltiumDatabase.AltiumDatabase import AltiumDatabase


class PartKeeprElement:
    def __init__(self):
        self.id = None
        self.mpn = None
        self.location = None
        self.stock_level = None

    def __str__(self):
        return "Part: {} {} [{}, {}]".format(self.mpn, self.id, self.location, self.stock_level)


def pk_api_call(method, url, **kwargs):
    """calls Partkeepr API

    :method: requst method
    :url: part of the url to call (without base)
    :data: tata to pass to the request if any
    :returns: requests object

    """
    pk_user = config["partkeepr"]["user"]
    pk_pwd = config["partkeepr"]["pwd"]
    pk_url = config["partkeepr"]["url"]

    try:
        r = requests.request(
            method,
            pk_url + url,
            **kwargs,
            auth=HTTPBasicAuth(pk_user, pk_pwd),
        )
        r.raise_for_status()
    except requests.exceptions.HTTPError as err:
        print(err)
        sys.exit(1)

    return r


def get_parts_from_json(rj):
    parts = []
    for part_in_db in rj["hydra:member"]:
        p = PartKeeprElement()
        # print(part_in_db)

        p.id = part_in_db["@id"]
        p.mpn = part_in_db['name']
        p.description = part_in_db['description']
        p.footprint = part_in_db['footprint']
        p.location = part_in_db['storageLocation']['name']
        p.stock_level = part_in_db['stockLevel']
        p.category_path = part_in_db['categoryPath']
        p.tolerance = ''
        p.case = ''
        p.dielectric = ''
        p.voltage = ''

        for parameter in part_in_db['parameters']:
            name = parameter['name']
            if name == 'Capacitance Tolerance':
                p.tolerance = parameter['stringValue']
            elif name == 'Case/Package':
                p.case = parameter['stringValue']
            elif name == 'Dielectric Characteristic':
                p.dielectric = parameter['stringValue']
            elif name == 'Voltage Rating (DC)':
                p.voltage = parameter['value']

        if p.category_path.find("eval boards") == -1:
            parts.append(p)

    return parts


def find_part(mpn: str):
    if mpn.strip() == '' or mpn.strip() == '-':
        return []

    params = {
        'filter':
        '{{"property":"name","operator":"LIKE","value":"%{}%"}}'.format(mpn)
    }
    rj = pk_api_call('get', '/api/parts', params=params).json()
    return get_parts_from_json(rj)


def get_query_or(value: str):
    return '''{"subfilters": [{"subfilters": [], "property": "name", "value": "%''' + value + '''%", "operator": "like"},
     {"subfilters": [], "property": "description", "value": "%''' + value + '''%", "operator": "like"}], "type": "OR"}'''


def get_query_and(values):
    query = ''
    for value in values:
        query += get_query_or(value)

        if value != values[-1]:
            query += ','
    return query


def get_categories(categories):
    if categories is None:
        return ''

    query = '{"subfilters":[],"property":"category","operator":"IN","value":['
    for category in categories:
        query += '"/api/part_categories/' + str(category) + '"'

        if category != categories[-1]:
            query += ','

    query += ']}'
    return query


def find_similar(query, categories=None):
    dot = ''
    if categories is not None:
        dot = ','

    params = {
        'filter':
        '[{"subfilters": [' + get_query_and(query) + '], "type": "AND"}' + dot + get_categories(categories) + ']'
    }

    by_description = pk_api_call('get', '/api/parts', params=params).json()

    return get_parts_from_json(by_description)


def part_stock_change(part: PartKeeprElement, stock_action: int, comment=''):
    pk_api_call(
        'put',
        '{}/addStock'.format(part.id),
        data={
            'quantity': stock_action,
            'comment': comment
        })


def format_number(num):
    try:
        dec = round(Decimal(num), 3)
    except:
        return 'bad'
    tup = dec.as_tuple()
    delta = len(tup.digits) + tup.exponent
    digits = ''.join(str(d) for d in tup.digits)
    if delta <= 0:
        zeros = abs(tup.exponent) - len(tup.digits)
        val = '0.' + ('0'*zeros) + digits
    else:
        val = digits[:delta] + ('0'*tup.exponent) + '.' + digits[delta:]
    val = val.rstrip('0')
    if val[-1] == '.':
        val = val[:-1]
    if tup.sign:
        return '-' + val
    return val


def check_tolerance(part, tolerances):
    if tolerances is None:
        return True

    if not isinstance(tolerances, list):
        tolerances = [format_number(tolerances)]

    for tolerance in tolerances:
        tolerance = str(tolerance).replace('%', '')

        if part.tolerance == '':
            if bool(re.search(tolerance + '%', str(part.description), re.IGNORECASE)):
                return True
        else:
            if bool(re.search(tolerance, str(part.tolerance), re.IGNORECASE)):
                return True
    return False


def check_dielectric(part, source_dielectric):
    if source_dielectric is None:
        return True

    if not isinstance(source_dielectric, list):
        source_dielectric = [source_dielectric]

    for dielectric in source_dielectric:
        if not isinstance(dielectric, str):
            continue

        if part.dielectric == '':
            if bool(re.search(dielectric, str(part.description), re.IGNORECASE)):
                return True
        else:
            if bool(re.search(dielectric, str(part.dielectric), re.IGNORECASE)):
                return True
    return False


def check_case(part, source_case):
    if source_case is None:
        return True

    if not isinstance(source_case, list):
        source_case = [source_case]

    cases = []
    for case in source_case:
        if isinstance(case, str):
            if case.find('_') > 0:
                cas = case.split('_')
                cases.append(cas[0])
                cases.append(cas[1])
            else:
                cases.append(case)
        else:
            cases.append(str(case))

    for case in cases:
        if part.case == '':
            if bool(re.search(str(case), str(part.description), re.IGNORECASE)):
                return True
        else:
            if bool(re.search(str(case), str(part.case), re.IGNORECASE)):
                return True
    return False


def check_voltage(part, voltages):
    if voltages is None:
        return True

    if isinstance(voltages, str):
        voltages = [voltages]

    for voltage in voltages:
        voltage = str(voltage).replace('V', '')

        if part.voltage == '':
            if bool(re.search(voltage + '[\s]?V', str(part.description), re.IGNORECASE)):
                return True
        else:
            if voltage == str(part.voltage):
                return True
    return False


def find_inductor(inductance: str, case=None, unwanted_ids=None):
    ind_info = re.split('([0-9|.]+)', inductance)
    inductance = ind_info[1]
    inductance_format = ind_info[2].replace(' ', '')

    possible_inductance_string = []
    if inductance_format in ['nH', 'NH', 'nh']:
        possible_inductance_string.append([inductance, 'nH'])
        possible_inductance_string.append([format_number(float(inductance) / 1000), 'uH'])

    if inductance_format in ['uh', 'uH', 'UH']:
        possible_inductance_string.append([inductance, 'uH'])
        possible_inductance_string.append([format_number(float(inductance) * 1000), 'nH'])

    best_parts = []
    second_parts = []
    # third_parts = []
    # fourth_parts = []

    id_list = []

    if not isinstance(unwanted_ids, list):
        unwanted_mpn = [unwanted_ids]

    for possible_inductance in possible_inductance_string:

        if possible_inductance[0] == '0':
            continue

        possible_inductance.append(case.split('_')[0])

        fond_parts = find_similar(possible_inductance)
        for part in fond_parts:
            desc = part.description
            if bool(re.search(possible_inductance[0] + '[\s]?' + possible_inductance[1], desc, re.IGNORECASE)):
                if part.id not in id_list:
                    # tolerance_match = check_tolerance(part, tolerance)
                    # dielectric_match = check_dielectric(part, dielectric)
                    case_match = check_case(part, case)
                    # voltage_match = check_voltage(part, voltage)

                    if (unwanted_ids is not None and part.id not in unwanted_ids) or unwanted_ids is None:
                        if case_match:
                            best_parts.append(part)
                        else:
                            second_parts.append(part)
                    id_list.append(part.id)

    return {'best_parts': best_parts, 'second_parts': second_parts}



def find_capacitor(capacitance: str, case=None, voltage=None, dielectric=None, tolerance=None, unwanted_ids=None):
    cap_info = re.split('([0-9|.]+)', capacitance)
    capacitance = cap_info[1]
    capacitance_format = cap_info[2].replace(' ', '')

    possible_capacitance_string = []

    if capacitance_format in ['nf', 'nF', 'NF']:
        possible_capacitance_string.append([capacitance, 'nF'])
        possible_capacitance_string.append([format_number(float(capacitance) / 1000), 'uF'])
        possible_capacitance_string.append([format_number(float(capacitance) * 1000), 'pF'])

    if capacitance_format in ['pf', 'pF', 'PF']:
        possible_capacitance_string.append([capacitance, 'pF'])
        possible_capacitance_string.append([format_number(float(capacitance) / 1000), 'nF'])
        possible_capacitance_string.append([format_number(float(capacitance) / 1000000), 'uF'])

    if capacitance_format in ['uf', 'uF', 'UF']:
        possible_capacitance_string.append([capacitance, 'uF'])
        possible_capacitance_string.append([format_number(float(capacitance) * 1000), 'nF'])
        possible_capacitance_string.append([format_number(float(capacitance) * 1000000), 'pF'])

    best_parts = []
    second_parts = []
    third_parts = []
    fourth_parts = []

    id_list = []

    if not isinstance(unwanted_ids, list):
        unwanted_mpn = [unwanted_ids]

    for possible_capacitance in possible_capacitance_string:

        if possible_capacitance[0] == '0':
            continue

        possible_capacitance.append(case.split('_')[0])

        fond_parts = find_similar(possible_capacitance)
        for part in fond_parts:
            desc = part.description
            if bool(re.search(possible_capacitance[0] + '[\s]?' + possible_capacitance[1], desc, re.IGNORECASE)):
                if part.id not in id_list:
                    tolerance_match = check_tolerance(part, tolerance)
                    dielectric_match = check_dielectric(part, dielectric)
                    case_match = check_case(part, case)
                    voltage_match = check_voltage(part, voltage)

                    if (unwanted_ids is not None and part.id not in unwanted_ids) or unwanted_ids is None:
                        if case_match and voltage_match and dielectric_match and tolerance_match:
                            best_parts.append(part)
                        elif case_match and voltage_match and dielectric_match:
                            second_parts.append(part)
                        elif case_match and voltage_match:
                            third_parts.append(part)
                        elif case_match:
                            fourth_parts.append(part)

                    id_list.append(part.id)

    return {'best_parts': best_parts, 'second_parts': second_parts, 'third_parts': third_parts, 'fourth_parts': fourth_parts}


def find_resistor(resistance, case=None, voltage=None, tolerance=None, unwanted_ids=None):
    try:
        resistance = int(resistance)
    except:
        resistance = resistance

    if not isinstance(unwanted_ids, list):
        unwanted_mpn = [unwanted_ids]

    res_info = []
    resistance_format = []
    if isinstance(resistance, str):
        res_info = re.split('([0-9|.]+)', resistance)
        resistance = res_info[1]
        resistance_format = res_info[2]
    else:
        if resistance < 1:
            resistance = format_number(resistance * 1000)
            resistance_format = 'm'
        elif resistance >= 1000000:
            resistance = format_number(resistance / 1000000)
            resistance_format = 'M'
        elif resistance >= 1000:
            resistance = format_number(resistance / 1000)
            resistance_format = 'k'
        else:
            resistance_format = 'R'

    possible_resistance_string = []

    if bool(re.match('(\s+)?R|Ohm|Om|Ω', resistance_format, re.IGNORECASE)):
        possible_resistance_string.append(format_number(float(resistance)) + 'R')
    elif bool(re.match('(\s+)?k|kOhm|kOm|kΩ', resistance_format, re.IGNORECASE)):
        possible_resistance_string.append(format_number(float(resistance)) + 'k')
    elif bool(re.match('(\s+)?M', resistance_format)) and bool(re.match('(\s+)?M|MOhm|MOm|MΩ', resistance_format, re.IGNORECASE)):
        possible_resistance_string.append(format_number(float(resistance)) + 'M')
    elif bool(re.match('(\s+)?m', resistance_format)) and bool(re.match('(\s+)?m|mOhm|mOm|mΩ', resistance_format, re.IGNORECASE)):
        possible_resistance_string.append([format_number(float(resistance)), 'mR'])
        possible_resistance_string.append([format_number(float(resistance)), 'mΩ'])

    best_parts = []
    second_parts = []
    third_parts = []
    fourth_parts = []

    id_list = []

    for possible_resistance in possible_resistance_string:
        if not isinstance(possible_resistance, list):
            possible_resistance = [possible_resistance]

        fond_parts = find_similar(possible_resistance, [38, 44])

        possible_resistance_joined = str(''.join(possible_resistance))

        index = re.search("mΩ|mR", possible_resistance_joined)
        if index is not None:
            index = index.start()

            possible_resistance_joined = possible_resistance_joined[:index] + '\s?' + possible_resistance_joined[index:]
            query = '\s' + possible_resistance_joined + '\s?'
        else:
            index = re.search("k|R|M", possible_resistance_joined).start()
            if index > 0:

                possible_resistance_joined = possible_resistance_joined[:index] + '\s?' + possible_resistance_joined[index:]

                query = '\s' + possible_resistance_joined + '?\s?(ohm|Ω)'
        for part in fond_parts:
            desc = part.description

            if bool(re.search(query, desc, re.IGNORECASE)):
                tolerance_match = check_tolerance(part, tolerance)
                case_match = check_case(part, case)
                voltage_match = check_voltage(part, voltage)

                if (unwanted_ids is not None and part.id not in unwanted_ids) or unwanted_ids is None:
                    if case_match and voltage_match and tolerance_match:
                        best_parts.append(part)
                    elif case_match and voltage_match:
                        second_parts.append(part)
                    elif case_match:
                        third_parts.append(part)
                    else:
                        fourth_parts.append(part)

                # else
                id_list.append(part.id)

    return {'best_parts': best_parts, 'second_parts': second_parts, 'third_parts': third_parts, 'fourth_parts': fourth_parts}


if __name__ == '__main__':
    # p1 = find_capacitor('100nF', case=['0603', '0402'], voltage=[16, 50])
    # # # p1 = find_similar_capacitor('0.001uF')
    # # # p2 = find_similar_capacitor('1nF',      '0402', '50', 'C0G', '5')
    # # # p3 = find_similar_capacitor('1000pF',   '0402', '50V', 'C0G', '5%')
    # #
    # print('\nFound as 0.001uF')
    # for name, math in p1.items():
    #     print('_______ ' + name + ' _______')
    #     for part in math:
    #         print(str(part.mpn) + '   ' + str(part.description))


    _altium_database_class = AltiumDatabase()
    database = _altium_database_class.database

    number_from_library = ('C_470nF_10V_X5R_0402_10').replace('NF_', 'nF_').replace('UF_', 'uF_').replace('PF_', 'pF_')

    altium_map = {}
    for i in database:
        altium_map[i.part_number] = i

    part = altium_map[number_from_library]
    # print(part)
    foundElements = find_part(part.manufacturer_part_number)
    print(foundElements)

    # foundElements = foundElements[0]
    # c = find_capacitor(capacitance=)

    # print(foundElements[0].mpn)
    #
    # # find_resistor('1k')
    # # find_resistor('11.8K OHM')
    # # find_resistor(1000000)
    # p1 = find_resistor('1k', case=['0603'], voltage='50V', tolerance='1%', unwanted_ids=str(foundElements[0].mpn))
    # p1 = find_capacitor(capacitance='470nF', case='0402', voltage='10V', tolerance='10%', dielectric='X5R')
    p1 = find_inductor(inductance='18 nH', case='0402')
    # print('\nFound as 1k')
    for name, math in p1.items():
        print('_______ ' + name + ' _______')
        for part in math:
            print(str(part.mpn) + '   ' + str(part.description))

