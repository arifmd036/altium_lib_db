import serial
import serial.tools.list_ports
import keyboard
import time
import pyperclip
from pprint import pprint
from tkinter.messagebox import showerror
import re

from actions import check_part_and_add_stock_or_add_element
from tk_messages import select_mode
import partkeepr_api

scanner_comport = None
for i in serial.tools.list_ports.comports():
    if i.hwid.find('080C:0400') != -1:
        scanner_comport = i.device

if scanner_comport is None:
    print("No scanner found!")
    showerror("Barcode scanner not found!", "Barcode scanner not found!")
    exit(1)

print("Found {}".format(scanner_comport))
scanner = serial.Serial(scanner_comport)

passthrough_mode = not select_mode()


while True:
    print('wait for scan')
    string = scanner.readline()
    data = [str(i.decode()) for i in string.split(b'\x1d')]
    print(data)
    mpn = None
    quantity = None
    for i in data:
        print(i[0])
        if i[0] == 'Q':
            string = i[1:].strip()
            string = re.sub('[^0-9]', '', string)
            quantity = int(string)
        elif i[0:2] == '1P':
            mpn = i[2:].strip()
            mpn = re.sub('[^ -~]', '', mpn)

    print("MPN:|{}|".format(mpn))
    print("Quantity: ", quantity)

    if not mpn or not quantity:
        print("QR code not recognised!")
        mpn = str(string.strip().decode())
        mpn = re.sub('[^ -~]', '', mpn)
        if mpn[0:2] == "1P":
            mpn = mpn[2:]
        print("MPN:|{}|".format(mpn))

    if passthrough_mode:
        pyperclip.copy(mpn)
        keyboard.send('ctrl+a')
        keyboard.write(mpn)
        keyboard.send('enter')
    else:
        check_part_and_add_stock_or_add_element(mpn, quantity)
