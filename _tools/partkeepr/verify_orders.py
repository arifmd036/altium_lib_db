import os
import sys
from shutil import copyfile
import re
import os.path
from subprocess import TimeoutExpired

import serial.tools.list_ports
import fileinput
from threading import Timer


import select
def sve_not_found_part(mpn, additional_parts_file_path):
    if os.path.exists(additional_parts_file_path):
        new_data_file = ''

        with open(additional_parts_file_path, 'r') as _file:
            found = False
            for line in _file:
                data_line = line.split(' ')
                iterations = 0
                try:
                    iterations = int(data_line[-1])
                    current_mpn = ' '.join(data_line[:-1])
                except:
                    current_mpn = ' '.join(data_line)

                if current_mpn.replace('\n', '') == mpn:
                    found = True
                    if iterations > 1:
                        new_data_file += mpn + ' ' + str(iterations + 1) + '\n'
                    else:
                        new_data_file += mpn + ' 2\n'
                else:
                    new_data_file += line

            if not found:
                new_data_file += mpn + '\n'

        with open(additional_parts_file_path, "w") as _file:
            _file.write(new_data_file)

    else:
        with open(additional_parts_file_path, "w") as _file:
            _file.write(mpn + '\n')


def verify_part(csv_file_path, mpn, order_quantity):
    new_data_file = ''
    found = False
    additional_parts_file_path = os.path.dirname(csv_file_path) + '\\additional_parts.txt'
    with open(csv_file_path, 'r') as _file:
        for line in _file:
            line_data = line.split(';')
            if len(line_data) > 10:
                if line_data[10] == mpn:
                    found = True
                    print("Successfully found " + mpn + "! Ordered in " + line_data[7])
                else:
                    new_data_file += line

    if found:
        with open(csv_file_path, "w") as _file:
            _file.write(new_data_file)
    else:
        print("Part " + mpn + " not found in file")

        new_mpn = ''
        print('Write mpn manually, if not just press enter:')
        sys.stdin.flush()
        new_mpn = input()

        if new_mpn == '':
            sve_not_found_part(mpn, additional_parts_file_path)
        else:
            verify_part(csv_file_path, new_mpn, order_quantity)


def get_data(scanner):
    print('wait for scan')
    scanner.read_all()
    string = scanner.readline()
    data = [str(i.decode()) for i in string.split(b'\x1d')]
    # print(data)
    mpn = None
    quantity = None
    for i in data:
        # print(i[0])
        if i[0] == 'Q':
            string = i[1:].strip()
            string = re.sub('[^0-9]', '', string)
            quantity = int(string)
        elif i[0:2] == '1P':
            mpn = i[2:].strip()
            mpn = re.sub('[^ -~]', '', mpn)

    print("MPN:|{}|".format(mpn))
    print("Quantity: ", quantity)

    if not mpn or not quantity:
        print("QR code not recognised!")
        mpn = str(string.strip().decode())
        mpn = re.sub('[^ -~]', '', mpn)
        if mpn[0:2] == "1P":
            mpn = mpn[2:]
        print("MPN:|{}|".format(mpn))
    # else:
        # print(len(data))

    return mpn, quantity


def verify(scanner, csv_file_path):
    while True:
        print('-------------')
        mpn, quantity = get_data(scanner)
        verify_part(csv_file_path, mpn, quantity)


if __name__ == '__main__':
    scanner_comport = None
    for i in serial.tools.list_ports.comports():
        if i.hwid.find('080C:0400') != -1:
            scanner_comport = i.device

    if scanner_comport is None:
        print("No scanner found!")
        input()
        exit(1)

    if len(sys.argv) < 2:
        print('Usage: <app name> <csv_source_file_path>')
        input()
        exit(1)
    else:
        scanner = serial.Serial(scanner_comport)

        csv_source_file_path = str(sys.argv[1])

        directory = os.path.dirname(csv_source_file_path) + "\\VerifyOrders"
        csv_file_path = directory + '\\VerifyingFile.csv'
        if os.path.exists(directory):
            print('Verifying order task continue...')
            print('-------------')
        else:
            print('New verifying order task')
            print('-------------')
            os.mkdir(directory)
            copyfile(csv_source_file_path, csv_file_path)

        verify(scanner, csv_file_path)
