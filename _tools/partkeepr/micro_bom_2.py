import progressbar
from tkinter.messagebox import showerror
import sys
import os
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from enum import Enum
import partkeepr_api
import re

from octopart_v4 import OctopartElement
from octopart_v4 import set_token as octopart_set_token
from octopart_v4 import main_suppliers_list
from currency import convert as currency_convert

from typing import Optional
from xlsxwriter.worksheet import (Worksheet, cell_number_tuple, cell_string_tuple)

from termcolor import colored

from partkeepr.partkeepr_api import find_resistor, find_capacitor, find_inductor

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(dir_path + '/..')
from library_parser.AltiumDatabase.AltiumDatabase import AltiumDatabase

usd_to_pln = currency_convert(1, 'USD', 'PLN')
eur_to_pln = currency_convert(1, 'EUR', 'PLN')
gbp_to_pln = currency_convert(1, 'GBP', 'PLN')

error_log = ''

suppliers_column_gap = 9
row_counter = 1
board_description_column_start = suppliers_column_gap + 7

column_names = main_suppliers_list + ['SKU', 'Selected Supplier', "Order quantity", "Part Number",
                                      "Manufacturer Part Number", "ALL Required quantity", "Available quantity",
                                      "Storage location", "Extra quantity"]


def octopart_get_sellers(mpn):
    try:
        element = OctopartElement(mpn)
        return element.sellers, ''
    except:
        return [], "cannot find element " + str(mpn) + "!"


def fill_suppliers(worksheet, row_counter, suppliers):
    if suppliers == None:
        return

    sku = {}

    for supplier in suppliers:
        price_string = ''
        comment_string = ''
        quantity = []

        company = supplier['company']['name']
        offers = supplier['offers'][0]

        url = offers['click_url']
        inventory_level = offers['inventory_level']

        prices = offers['prices']

        order_quantity = xl_rowcol_to_cell(row_counter, 8)
        for price in (prices):
            value = 0
            if price['currency'] == 'PLN':
                if price['quantity'] not in quantity:
                    quantity.append(price['quantity'])
                    value = price['price']
                else:
                    continue
            if price['currency'] == 'USD':
                if price['quantity'] not in quantity:
                    quantity.append(price['quantity'])
                    value = price['price'] * usd_to_pln
                else:
                    continue
            elif price['currency'] == 'EUR':
                if price['quantity'] not in quantity:
                    quantity.append(price['quantity'])
                    value = price['price'] * eur_to_pln
                else:
                    continue
            elif price['currency'] == 'GBP':
                if price['quantity'] not in quantity:
                    quantity.append(price['quantity'])
                    value = price['price'] * gbp_to_pln
                else:
                    continue
            else:
                continue

            price_string = 'IF({} >= {}, {}, {})'.format(order_quantity, price['quantity'], value, price_string)
            comment_string += str(price['quantity']) + ':  \t' + format(value, '.3f') + ' PLN\n'

        if price_string == '':
            price_string = '0'

        price_string = 'IF(AND(ABS({}) < {}, {} > 0), HYPERLINK("{}", {} * ABS({})), {}) '.format(order_quantity,
                                                                                                  inventory_level,
                                                                                                  order_quantity, url,
                                                                                                  price_string,
                                                                                                  order_quantity,
                                                                                                  '\"-\"')

        if company in main_suppliers_list:
            worksheet.write_formula(row_counter, main_suppliers_list.index(company), price_string)
            worksheet.write_comment(row_counter, main_suppliers_list.index(company), comment_string)


def fill_boards_quantity(worksheet, row_counter, source_files):
    requied_quantity = "="
    i = 0

    for part in source_files:
        worksheet.write(row_counter, board_description_column_start + i * 3, source_files[part])
        requied_quantity += "+ {} * {}".format(xl_rowcol_to_cell(0, board_description_column_start + i * 3 + 1),
                                               xl_rowcol_to_cell(row_counter, board_description_column_start + i * 3))
        all_boards = "={} * {}".format(xl_rowcol_to_cell(0, board_description_column_start + i * 3 + 1),
                                       xl_rowcol_to_cell(row_counter, board_description_column_start + i * 3))
        worksheet.write(row_counter, board_description_column_start + i * 3 + 1, all_boards)
        i += 1

    worksheet.write(row_counter, suppliers_column_gap + 2, requied_quantity)


def fill_column_names(worksheet, column_names, row_counter):
    bold_format = workbook.add_format({
        'bold': 1
    })

    for name in column_names:
        worksheet.write(row_counter, column_names.index(name), name, bold_format)


def fill_separation_column(worksheet, column_index, row_max_index):
    separation_column_format = workbook.add_format({
        'bold': 1,
        'align': 'center',
        'valign': 'left',
        'fg_color': '#99ccff'
    })

    for j in range(0, row_max_index):
        worksheet.write(j, column_index, ' ', separation_column_format)


def fill_boards_name(worksheet, input_files, row_max_index):
    i = 7
    for file in input_files:
        fill_separation_column(worksheet, suppliers_column_gap + i - 1, row_max_index)

        worksheet.write(0, suppliers_column_gap + i + 1, '1', bold_format)
        worksheet.write(0, suppliers_column_gap + i, file, bold_format)
        worksheet.write(1, suppliers_column_gap + i, "One board", bold_format)
        worksheet.write(1, suppliers_column_gap + i + 1, "All boards", bold_format)
        i += 3

    fill_separation_column(worksheet, suppliers_column_gap + i - 1, row_max_index)


def calculate_max_column_index(input_files):
    return len(input_files) * 3 + board_description_column_start - 1


def fill_part_info(worksheet, row_start, row_stop, part_number, mpn, available_quantity, storage_location,
                   background=None):
    worksheet.data_validation(row_start, suppliers_column_gap - 2, row_start, suppliers_column_gap - 2,
                              {'validate': 'list', 'source': main_suppliers_list + ['Other']})

    worksheet.write(row_start, suppliers_column_gap + 0, part_number, background)
    worksheet.write(row_start, suppliers_column_gap + 1, mpn, background)

    worksheet.write(row_start, suppliers_column_gap + 4, storage_location, background)

    if row_start == row_stop:
        worksheet.write(row_start, suppliers_column_gap + 3, available_quantity, background)

    else:
        sumif = '={} + SUMIF({}:{},"Use",{}:{})'.format(available_quantity,
                                                        xl_rowcol_to_cell(row_start + 1, suppliers_column_gap - 2),
                                                        xl_rowcol_to_cell(row_stop, suppliers_column_gap - 2),
                                                        xl_rowcol_to_cell(row_start + 1, suppliers_column_gap + 3),
                                                        xl_rowcol_to_cell(row_stop, suppliers_column_gap + 3))

        worksheet.write(row_start, suppliers_column_gap + 3, sumif, background)


def end_part(worksheet, row):
    row = row + 1
    worksheet.set_row(row, 5, workbook.add_format({'fg_color': '#AAAAAA'}))
    return row + 1


def find_similar_resistors(mpn, partkeepr_element):
    resistors = []
    altium_part = altium_map[mpn]

    unwanted_ids = []
    if len(partkeepr_element) > 0:
        unwanted_ids = partkeepr_element[0].id

    if altium_part.parameters['category'] != 'Resistors':
        return resistors

    similar_resistors = find_resistor(resistance=altium_part.parameters['Value'],
                                      case=altium_part.pcblib,
                                      voltage=altium_part.parameters['Value'],
                                      tolerance=altium_part.parameters['Tolerance'],
                                      unwanted_ids=unwanted_ids)

    resistors += similar_resistors['best_parts']
    resistors += similar_resistors['second_parts']
    resistors += similar_resistors['third_parts']
    return resistors


def find_similar_capacitors(mpn, partkeepr_element):
    capacitors = []
    altium_part = altium_map[mpn]
    if altium_part.parameters['category'] != 'Capacitors':
        return capacitors

    unwanted_ids = []
    if len(partkeepr_element) > 0:
        unwanted_ids = partkeepr_element[0].id

    similar_capacitors = find_capacitor(capacitance=altium_part.parameters['Value'],
                                        case=altium_part.pcblib,
                                        voltage=altium_part.parameters['Voltage'],
                                        dielectric=altium_part.parameters['Dielectric'],
                                        tolerance=altium_part.parameters['Tolerance'],
                                        unwanted_ids=unwanted_ids)

    capacitors += similar_capacitors['best_parts']
    capacitors += similar_capacitors['second_parts']
    capacitors += similar_capacitors['third_parts']
    capacitors += similar_capacitors['fourth_parts']

    return capacitors


def find_similar_inductor(mpn, partkeepr_element):
    inductors = []
    altium_part = altium_map[mpn]
    if altium_part.parameters['category'] != 'Inductors':
        return inductors

    unwanted_ids = []
    if len(partkeepr_element) > 0:
        unwanted_ids = partkeepr_element[0].id

    similar_capacitors = find_inductor(inductance=altium_part.parameters['Value'], case=altium_part.pcblib)

    inductors += similar_capacitors['best_parts']
    inductors += similar_capacitors['second_parts']

    return inductors


def fill_similar_elements(worksheet, elements, row_counter, background):
    for element in elements:
        worksheet.write(row_counter, 0, element.description)
        worksheet.write(row_counter, suppliers_column_gap + 1, element.mpn, background)
        worksheet.write(row_counter, suppliers_column_gap + 3, element.stock_level, background)
        worksheet.write(row_counter, suppliers_column_gap + 4, element.location, background)

        worksheet.data_validation(row_counter, suppliers_column_gap - 2, row_counter, suppliers_column_gap - 2,
                                  {'validate': 'list', 'source': ['Use', 'No']})

        row_counter += 1
    return row_counter

no_similar = False
def find_similar_elements(part):
    similar_elements = []
    if no_similar:
        return similar_elements

    if re.match('^R_', part['part_number']):
        similar_elements = find_similar_resistors(part['part_number'], part['partkeepr_element'])

    elif re.match('^C_', part['part_number']):
        similar_elements = find_similar_capacitors(part['part_number'], part['partkeepr_element'])

    elif re.match('^L_', part['part_number']):
        similar_elements = find_similar_inductor(part['part_number'], part['partkeepr_element'])
    return similar_elements


print('Number of arguments:', len(sys.argv), 'arguments.')

working_dir = ''
input_files = []
if len(sys.argv) < 2:
    print('Usage: <app name> <BOM dir path> <--no-similar>')
    input()
    exit(0)
else:
    working_dir = str(sys.argv[1])
    print('working dir: ' + working_dir)
    temp_files = os.listdir(str(sys.argv[1]))

    if len(sys.argv) == 3 and sys.argv[2] == "--no-similar":
        no_similar = True


    for file in temp_files:
        if file == 'error_log.txt':
            continue
        if file.endswith('.txt'):
            input_files.append(file)

    print(input_files)

print("Write token api to octopart, if do not use press enter")
token = input()
octopart_set_token(token)

_altium_database_class = AltiumDatabase()
database = _altium_database_class.database

output_file = working_dir + '\\tmp.xlsx'

max_column = 0

parts = {}

for input_file in input_files:
    input_file_path = working_dir + '\\' + input_file
    with open(input_file_path, 'r') as _file:
        for line in _file:
            quotes = re.findall('"([^"]*)"', line)

            for quote in quotes:
                line = line.replace('"' + quote + '"', '')

            l = line.strip().split(' ')

            if len(l) == 2:
                designator = l[0]
                part_name = l[1].replace('NF_', 'nF_').replace('UF_', 'uF_').replace('PF_', 'pF_')
                altium_mpn = ''

                if re.match('^CMP-', part_name):
                    altium_mpn = quotes[0]

                if part_name not in parts:
                    parts[part_name] = [{'file': input_file, 'desc': designator, 'altium_mpn': altium_mpn}]
                else:
                    parts[part_name].append({'file': input_file, 'desc': designator, 'altium_mpn': altium_mpn})
            else:
                log = 'Part "' + line + '" with no MPN in file ' + input_file + ', part omitted.'
                error_log += log + '\r\n'
                print(colored(log, 'yellow'))

max_column = calculate_max_column_index(input_files)

altium_map = {}
for i in database:
    altium_map[i.part_number] = i


class PartState(Enum):
    OK = 0
    EmptyMPN = 1
    NotFoundInLocalAltium = 2
    NotFoundInPartKeepr = 3
    AmbiguousInPartKeepr = 4
    PartFromGlobalAltiumLib = 5


part_output = {
    PartState.OK: [],
    PartState.EmptyMPN: [],
    PartState.NotFoundInLocalAltium: [],
    PartState.NotFoundInPartKeepr: [],
    PartState.AmbiguousInPartKeepr: [],
    PartState.PartFromGlobalAltiumLib: [],
}

for i in progressbar.progressbar(parts):
    source_files = {}

    for file in input_files:
        source_files[file] = 0

    for part in parts[i]:
        source_files[part['file']] += 1

    if i not in altium_map:
        if parts[i][0]['altium_mpn'] != '':
            mpn = parts[i][0]['altium_mpn']

            sellers, log = octopart_get_sellers(mpn)

            if log != '':
                error_log += log + '\n\r'

            part_output[PartState.PartFromGlobalAltiumLib].append([
                i, len(parts[i]), source_files, mpn, sellers
            ])
        else:
            part_output[PartState.NotFoundInLocalAltium].append([
                i, len(parts[i]), source_files
            ])
    else:
        mpn = altium_map[i].manufacturer_part_number

        sellers, log = octopart_get_sellers(mpn)

        if log != '':
            error_log += log + '\n\r'

        if mpn.strip() == '' or mpn.strip() == '-':
            part_output[PartState.EmptyMPN].append([
                i, mpn, len(parts[i]), source_files, sellers
            ])
            continue

        partkeepr_element = partkeepr_api.find_part(mpn)
        if len(partkeepr_element) == 0:
            part_output[PartState.NotFoundInPartKeepr].append({
                'part_number': i,
                'mpn': mpn,
                'parts_count': len(parts[i]),
                'source_files': source_files,
                'partkeepr_element': partkeepr_element,
                'sellers': sellers
            })
        elif len(partkeepr_element) == 1:
            part_output[PartState.OK].append({
                'part_number': i,
                'mpn': mpn,
                'parts_count': len(parts[i]),
                'source_files': source_files,
                'partkeepr_element': partkeepr_element,
                'sellers': sellers
            })
        else:
            part_output[PartState.AmbiguousInPartKeepr].append({
                'part_number': i,
                'mpn': mpn,
                'parts_count': len(parts[i]),
                'source_files': source_files,
                'partkeepr_element': partkeepr_element,
                'sellers': sellers
            })

workbook = xlsxwriter.Workbook(output_file)
worksheet = workbook.add_worksheet('elements')

# Create a format to use in the merged range.
merge_format = workbook.add_format({
    'bold': 1,
    'align': 'center',
    'valign': 'left',
    'fg_color': '#99ccff'
})

blank_format = workbook.add_format({
    'align': 'left',
    'valign': 'vcenter',
})

bold_format = workbook.add_format({
    'bold': 1
})

fill_column_names(worksheet, column_names, row_counter)

row_counter += 1

worksheet.merge_range(row_counter, 0, row_counter, max_column, 'Not found in Altium library', merge_format)
row_counter += 1

background = workbook.add_format()
background.set_bg_color('red')

part_output[PartState.NotFoundInLocalAltium].sort(key=lambda x: x[0])
for part in part_output[PartState.NotFoundInLocalAltium]:
    fill_part_info(worksheet, row_counter, row_counter, part[0], part[1], 0, 'NOT FOUND IN ALTIUM LIBRARY', background)
    fill_boards_quantity(worksheet, row_counter, part[2])

    order_quantity = "={} + {}".format(xl_rowcol_to_cell(row_counter, suppliers_column_gap + 2),
                                       xl_rowcol_to_cell(row_counter, suppliers_column_gap + 5))
    worksheet.write(row_counter, suppliers_column_gap - 1, order_quantity)

    row_counter = end_part(worksheet, row_counter)

worksheet.merge_range(row_counter, 0, row_counter, max_column, 'From global Altium library', merge_format)
row_counter += 1

part_output[PartState.PartFromGlobalAltiumLib].sort(key=lambda x: x[0])
for part in part_output[PartState.PartFromGlobalAltiumLib]:
    fill_suppliers(worksheet, row_counter, part[4])
    fill_part_info(worksheet, row_counter, row_counter, part[0], part[3], 0, '-', background)
    fill_boards_quantity(worksheet, row_counter, part[2])

    order_quantity = "={} + {}".format(xl_rowcol_to_cell(row_counter, suppliers_column_gap + 2),
                                       xl_rowcol_to_cell(row_counter, suppliers_column_gap + 5))
    worksheet.write(row_counter, suppliers_column_gap - 1, order_quantity)

    row_counter = end_part(worksheet, row_counter)


worksheet.merge_range(row_counter, 0, row_counter, max_column, 'Not found in PartKeepr', merge_format)
row_counter += 1

background = workbook.add_format()
background.set_bg_color('orange')

part_output[PartState.NotFoundInPartKeepr].sort(key=lambda x: x['part_number'])
print("\nSearch similar elements")
for part in progressbar.progressbar(part_output[PartState.NotFoundInPartKeepr]):
    fill_suppliers(worksheet, row_counter, part['sellers'])
    fill_boards_quantity(worksheet, row_counter, part['source_files'])

    order_quantity = "=-({} - {} - {})".format(xl_rowcol_to_cell(row_counter, suppliers_column_gap + 3),
                                               xl_rowcol_to_cell(row_counter, suppliers_column_gap + 2),
                                               xl_rowcol_to_cell(row_counter, suppliers_column_gap + 5))
    worksheet.write(row_counter, suppliers_column_gap - 1, order_quantity)

    similar_elements = find_similar_elements(part)
    fill_part_info(worksheet, row_counter, row_counter + len(similar_elements), part['part_number'], part['mpn'], 0,
                   'NOT FOUND IN PARTKEEPR',
                   background)
    row_counter += 1
    row_counter = fill_similar_elements(worksheet, similar_elements, row_counter, background)
    row_counter = end_part(worksheet, row_counter - 1)

worksheet.merge_range(row_counter, 0, row_counter, max_column, 'Ambiguous in PartKeepr', merge_format)
row_counter += 1

background = workbook.add_format()
background.set_bg_color('yellow')

part_output[PartState.AmbiguousInPartKeepr].sort(key=lambda x: x['part_number'])
for part in part_output[PartState.AmbiguousInPartKeepr]:
    fill_suppliers(worksheet, row_counter, part['sellers'])
    fill_boards_quantity(worksheet, row_counter, part['source_files'])

    row_counter_start = row_counter
    row_counter_stop = row_counter

    color = '#FFFF00'
    for p in part['partkeepr_element']:
        if color == '#FFAA00':
            color = '#FFFF00'
        else:
            color = '#FFAA00'
        background = workbook.add_format()
        background.set_bg_color(color)
        worksheet.write(row_counter, suppliers_column_gap + 1, str(p.mpn), background)
        worksheet.write(row_counter, suppliers_column_gap + 4, str(p.location), background)
        worksheet.write(row_counter, suppliers_column_gap + 3, p.stock_level, background)
        row_counter_stop = row_counter
        row_counter += 1

    order_quantity = "=-(SUM({}:{}) - {} - {})".format(xl_rowcol_to_cell(row_counter_start, suppliers_column_gap + 3),
                                                       xl_rowcol_to_cell(row_counter_stop, suppliers_column_gap + 3),
                                                       xl_rowcol_to_cell(row_counter_start, suppliers_column_gap + 2),
                                                       xl_rowcol_to_cell(row_counter_start, suppliers_column_gap + 5))
    worksheet.write(row_counter_start, suppliers_column_gap - 1, order_quantity)

    worksheet.merge_range(row_counter_start, suppliers_column_gap + 0, row_counter_stop, suppliers_column_gap + 0,
                          part['part_number'], blank_format)
    row_counter = end_part(worksheet, row_counter - 1)

worksheet.merge_range(row_counter, 0, row_counter, max_column, 'Empty manufacturer part number', merge_format)
row_counter += 1

background = workbook.add_format()
background.set_bg_color('#FFBB00')

part_output[PartState.EmptyMPN].sort(key=lambda x: x[0])
for part in part_output[PartState.EmptyMPN]:
    fill_part_info(worksheet, row_counter, row_counter, part[0], "-", 0, 'EMPTY MPN', background)
    fill_boards_quantity(worksheet, row_counter, part[3])
    row_counter = end_part(worksheet, row_counter)

worksheet.merge_range(row_counter, 0, row_counter, max_column, 'Found parts in PartKeepr', merge_format)
row_counter += 1

background = workbook.add_format()
background.set_bg_color('red')

part_output[PartState.OK].sort(key=lambda x: x['partkeepr_element'][0].stock_level - x['parts_count'])
print("\nSearch similar elements")
for part in progressbar.progressbar(part_output[PartState.OK]):
    fill_suppliers(worksheet, row_counter, part['sellers'])
    fill_boards_quantity(worksheet, row_counter, part['source_files'])

    order_quantity = "=-({} - {} - {})".format(xl_rowcol_to_cell(row_counter, suppliers_column_gap + 3),
                                               xl_rowcol_to_cell(row_counter, suppliers_column_gap + 2),
                                               xl_rowcol_to_cell(row_counter, suppliers_column_gap + 5))
    worksheet.write(row_counter, suppliers_column_gap - 1, order_quantity)

    similar_elements = find_similar_elements(part)
    fill_part_info(worksheet, row_counter, row_counter + len(similar_elements), part['part_number'], part['mpn'], part['partkeepr_element'][0].stock_level, part['partkeepr_element'][0].location)
    row_counter += 1
    row_counter = fill_similar_elements(worksheet, similar_elements, row_counter, None)
    row_counter = end_part(worksheet, row_counter - 1)

worksheet.merge_range(row_counter, 0, row_counter, max_column, ' ', merge_format)
row_counter += 1

worksheet.conditional_format('I4:I9999', {'type': 'cell', 'criteria': '>', 'value': 0, 'format': background})

fill_boards_name(worksheet, input_files, row_counter - 1)


def get_column_width(worksheet: Worksheet, column: int) -> Optional[int]:
    """Get the max column width in a `Worksheet` column."""
    strings = getattr(worksheet, '_ts_all_strings', None)
    if strings is None:
        strings = worksheet._ts_all_strings = sorted(
            worksheet.str_table.string_table,
            key=worksheet.str_table.string_table.__getitem__)
    lengths = set()
    for row_id, colums_dict in worksheet.table.items():  # type: int, dict
        data = colums_dict.get(column)
        if not data:
            continue
        if type(data) is cell_string_tuple:
            iter_length = len(strings[data.string])
            if not iter_length:
                continue
            lengths.add(iter_length)
            continue
        if type(data) is cell_number_tuple:
            iter_length = len(str(data.number))
            if not iter_length:
                continue
            lengths.add(iter_length)
    if not lengths:
        return None
    return max(lengths)


def set_column_autowidth(worksheet: Worksheet, column: int):
    """
    Set the width automatically on a column in the `Worksheet`.
    !!! Make sure you run this function AFTER having all cells filled in
    the worksheet!
    """
    maxwidth = get_column_width(worksheet=worksheet, column=column)
    if maxwidth is None:
        return
    worksheet.set_column(first_col=column, last_col=column, width=1.1 * maxwidth)


worksheet.set_column(first_col=0, last_col=7, width=10)

for i in range(suppliers_column_gap, 60):
    set_column_autowidth(workbook.get_worksheet_by_name('elements'), i)

source = main_suppliers_list + ['Other']
# worksheet.data_validation(2, suppliers_column_gap - 2, row_counter, suppliers_column_gap - 2,
#                           {'validate': 'list', 'source': main_suppliers_list + ['Other']})

worksheet.conditional_format(2, suppliers_column_gap - 2, row_counter - 1, suppliers_column_gap - 2,
                             {'type': 'formula', 'criteria': 'True',
                              'format': workbook.add_format({'left': 2, 'right': 2})})

worksheet.conditional_format(2, suppliers_column_gap + 5, row_counter - 1, suppliers_column_gap + 5,
                             {'type': 'formula', 'criteria': 'True',
                              'format': workbook.add_format({'left': 2, 'right': 2})})

for supplier in main_suppliers_list:
    sumif = '=SUMIF(H3:{},"{}",{}:{})'.format(xl_rowcol_to_cell(row_counter - 1, suppliers_column_gap - 2), supplier,
                                              xl_rowcol_to_cell(2, main_suppliers_list.index(supplier)),
                                              xl_rowcol_to_cell(row_counter - 1, main_suppliers_list.index(supplier)))
    worksheet.write(0, main_suppliers_list.index(supplier), sumif)

worksheet.write(0, 7, '=SUM(A1:F1)')

file = open(working_dir + '\\error_log.txt', 'w')
file.write(error_log)
file.close()

try:
    workbook.close()
    os.system('start "excel" "{}"'.format(output_file))
except:
    showerror("", "Cannot save excel output file!\nIs it opened already?")

enter = input()
