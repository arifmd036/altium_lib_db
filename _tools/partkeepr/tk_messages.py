from tkinter.messagebox import askyesno, showerror
import tkinter as tk
import pyperclip
from functools import partial
from partkeepr_api import PartKeeprElement
from typing import List

root = tk.Tk()
root.withdraw()

import tkinter as tk
import ctypes

#   store some stuff for win api interaction
set_to_foreground = ctypes.windll.user32.SetForegroundWindow
keybd_event = ctypes.windll.user32.keybd_event

alt_key = 0x12
extended_key = 0x0001
key_up = 0x0002


def steal_focus(window):
    keybd_event(alt_key, 0, extended_key | 0, 0)
    set_to_foreground(window)
    keybd_event(alt_key, 0, extended_key | key_up, 0)


class MessageBox(object):
    def __init__(self, mpn, quantity_to_add, partkeepr_elements: List[PartKeeprElement], part_number=None):
        root = self.root = tk.Tk()

        self.mpn = mpn
        if mpn is None:
            assert part_number is not None
            self.mpn = part_number
            part_number = None

        self.part_number = str(part_number) if part_number else None
        self.partkeepr_elements = partkeepr_elements

        root.title(self.mpn)

        frm_1 = tk.Frame(root)
        frm_1.pack(ipadx=2, ipady=2)

        # the message
        tk.Label(frm_1, text="MPN: " + self.mpn, font="Consolas 80 bold").pack()

        if part_number:
            tk.Label(frm_1, text="Part number: " + part_number, font="Consolas 80 bold").pack()

        if mpn.strip() == '' or mpn.strip() == '-':
            tk.Label(frm_1, text="Empty Manufacturer Part number!", font="Consolas 60 bold", bg='red').pack()
            self.partkeepr_elements = []
            partkeepr_elements = []

        choices = []
        self.actual_choice = 0

        def select_part(nr, event=None):
            self.actual_choice = nr
            for i in range(len(partkeepr_elements)):
                if i == nr:
                    choices[i].configure(bg='red')
                else:
                    choices[i].configure(bg='SystemButtonFace')

        def select_higher(event=None):
            select_part(max(0, self.actual_choice - 1))

        def select_lower(event=None):
            select_part(min(len(partkeepr_elements) - 1, self.actual_choice + 1))

        for i in range(len(partkeepr_elements)):
            l = tk.Label(frm_1, text="{} in {}: {} pcs".format(partkeepr_elements[i].mpn,
                                                               partkeepr_elements[i].location,
                                                               partkeepr_elements[i].stock_level),
                         font="Consolas 50 bold", justify='center')
            l.pack(pady=10)
            choices.append(l)

        select_part(0)

        if mpn is None:
            tk.Label(frm_1, text="Part not found in Altium Library!", font="Consolas 50 bold", justify='center').pack(
                pady=10)
        elif len(partkeepr_elements) == 0:
            tk.Label(frm_1, text="Part not found in PartKeepr!", font="Consolas 50 bold", justify='center').pack(pady=10)

        tk.Label(frm_1, text="Stock action:").pack()

        quantity_to_add = int(quantity_to_add)

        # if entry=True create and set focus
        self.entry = tk.Entry(frm_1, font="Consolas 80 bold", justify='center')
        self.entry.insert(tk.END,  "{0:+d}".format(quantity_to_add))
        self.entry.pack()
        self.entry.bind('<KeyPress-Return>', func=self.ok_action)
        self.entry.bind('<KeyPress-Escape>', func=self.cancel_action)
        self.entry.bind('<KeyPress-F1>', func=self.find_action)
        self.entry.bind("<KeyPress-Down>", select_lower)
        self.entry.bind("<KeyPress-Up>", select_higher)

        def increase(event=None):
            try:
                now = int(self.entry.get())
                self.entry.delete(0, 'end')
                self.entry.insert(tk.END, "{0:+d}".format(now + 1))
            except:
                pass

        def decrease(event=None):
            try:
                now = int(self.entry.get())
                self.entry.delete(0, 'end')
                self.entry.insert(tk.END, "{0:+d}".format(now - 1))
            except:
                pass

        self.entry.bind("<Prior>", increase)  # PageUp
        self.entry.bind("<Next>", decrease)  # PageDown

        if len(partkeepr_elements) == 0:
            self.entry.config(state='disabled')

        button_frame = tk.Frame(frm_1)
        button_frame.pack(padx=4, pady=4)

        btn_ok = tk.Button(button_frame, width=8, text='OK')
        btn_ok['command'] = self.ok_action
        btn_ok.pack(side='left')
        btn_ok.bind('<KeyPress-Return>', func=self.ok_action)

        if len(partkeepr_elements):
            btn_cancel = tk.Button(button_frame, width=8, text='Cancel')
            btn_cancel['command'] = self.cancel_action
            btn_cancel.pack(side='left')
            btn_cancel.bind('<KeyPress-Return>', func=self.cancel_action)

        if len(partkeepr_elements):
            tk.Label(frm_1, text="<Enter> to accept").pack()
            tk.Label(frm_1, text="<Escape> to cancel and copy name to clipboard").pack()
            tk.Label(frm_1, text="<F1> to search in partkeepr").pack()
        else:
            tk.Label(frm_1, text="<Enter/Escape> to close window and copy name to clipboard").pack()
            tk.Label(frm_1, text="<F1> to search in partkeepr").pack()

        # call self.close_mod when the close button is pressed
        root.protocol("WM_DELETE_WINDOW", self.close_mod)

        # a trick to activate the window (on windows 7)
        root.deiconify()

        steal_focus(root.winfo_id())
        self.entry.focus_set()

    def ok_action(self, event=None):
        if len(self.partkeepr_elements) == 0:
            self.cancel_action()
        else:
            self.returning = (int(self.entry.get()), self.actual_choice)
            self.root.quit()

    def cancel_action(self, event=None):
        pyperclip.copy(self.mpn)
        self.returning = 'none'
        self.root.quit()

    def find_action(self, event=None):
        pyperclip.copy(self.mpn)
        self.returning = 'find'
        self.root.quit()

    def close_mod(self):
        self.cancel_action()


def part_action(mpn, action_quantity, location_and_quantity, part_number=None):
    if action_quantity is None:
        action_quantity = 0
    msgbox = MessageBox(mpn, action_quantity, location_and_quantity, part_number)
    msgbox.root.mainloop()
    # the function pauses here until the mainloop is quit
    msgbox.root.destroy()
    return msgbox.returning


class ModeSelectBox(object):
    def __init__(self):
        root = self.root = tk.Tk()
        root.title("Select mode")

        # main frame
        frm_1 = tk.Frame(root)
        frm_1.pack(ipadx=2, ipady=2)

        # the message
        message = tk.Label(frm_1, text="Select mode!", font="Consolas 20 bold")
        message.pack()

        # button frame
        button_frame = tk.Frame(frm_1)
        button_frame.pack(padx=4, pady=4)

        # buttons
        btn_ok = tk.Button(button_frame, width=20, text='Normal mode')
        btn_ok['command'] = self.normal
        btn_ok.pack(side='left')
        btn_ok.bind('<KeyPress-Return>', func=self.normal)
        btn_ok.focus_set()

        btn_cancel = tk.Button(button_frame, width=20, text='Text only passthrough')
        btn_cancel['command'] = self.passthrough
        btn_cancel.pack(side='left')
        btn_cancel.bind('<KeyPress-Return>', func=self.passthrough)

        # call self.close_mod when the close button is pressed
        root.protocol("WM_DELETE_WINDOW", self.close_mod)

        # a trick to activate the window (on windows 7)
        root.deiconify()

    def normal(self, event=None):
        self.returning = True
        self.root.quit()

    def passthrough(self, event=None):
        self.returning = False
        self.root.quit()

    def close_mod(self):
        exit(0)


def select_mode():
    msgbox = ModeSelectBox()
    msgbox.root.mainloop()
    # the function pauses here until the mainloop is quit
    msgbox.root.destroy()
    return msgbox.returning


if __name__ == "__main__":
    print(select_mode())
    # for i in range(1):
    print(part_action('MCP6512', 10, [PartKeeprElement("Inne elementy", 20),
                                      PartKeeprElement("ABC", 10),
                                      PartKeeprElement("A18", 1)]))

    print(part_action('MCP6512', 10, []))
    print(part_action('MCP6512', 10, [], part_number='abc'))
    print(part_action(None, 10, [], part_number='abc'))
