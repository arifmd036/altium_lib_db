from currency_converter import CurrencyConverter

c = CurrencyConverter('http://www.ecb.europa.eu/stats/eurofxref/eurofxref.zip')


def convert(value, v1, v2):
    return c.convert(value, v1, v2)
