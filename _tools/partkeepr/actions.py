import partkeepr_api
from browser import partkeepr_web
from tk_messages import part_action
from tkinter.messagebox import askokcancel


def ask_user(mpn, action_quantity, location_and_quantity, part_number=None):
    user_action = part_action(mpn, action_quantity, location_and_quantity, part_number=part_number)

    if mpn is None:
        mpn = part_number

    if user_action == 'find':
        partkeepr_web.find_part(mpn)
        return None
    elif user_action == 'none':
        print('cancel')
        return None
    else:
        # change stock
        return user_action


def check_part_and_add_stock(mpn, quantity, part_number=None):
    parts = partkeepr_api.find_part(mpn)

    user_action = ask_user(mpn, quantity, parts, part_number=part_number)
    if user_action:
        part = parts[user_action[1]]
        quantity = user_action[0]
        print(part, quantity)
        partkeepr_api.part_stock_change(part, quantity)


def check_part_and_add_stock_or_add_element(mpn, quantity):
    parts = partkeepr_api.find_part(mpn)

    if len(parts) > 0:
        user_action = ask_user(mpn, quantity, parts)
        if user_action:
            part = parts[user_action[1]]
            quantity = user_action[0]
            print(part, quantity)
            partkeepr_api.part_stock_change(part, quantity)
    else:
        if askokcancel("Add element?", "Add element {} to partkeepr?".format(mpn)):
            partkeepr_web.add_part(mpn, quantity)
