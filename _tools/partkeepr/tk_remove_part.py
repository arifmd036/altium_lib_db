import os
import sys
from tkinter.messagebox import askyesno, showerror
import tkinter as tk
import pyperclip
from functools import partial
from partkeepr_api import PartKeeprElement, find_part, find_resistor, find_capacitor, part_stock_change
from typing import List

dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(dir_path + '/..')
from library_parser.AltiumDatabase.AltiumDatabase import AltiumDatabase

# root = tk.Tk()
# root.withdraw()

import tkinter as tk
import ctypes

#   store some stuff for win api interaction
set_to_foreground = ctypes.windll.user32.SetForegroundWindow
keybd_event = ctypes.windll.user32.keybd_event

alt_key = 0x12
extended_key = 0x0001
key_up = 0x0002


def steal_focus(window):
    keybd_event(alt_key, 0, extended_key | 0, 0)
    set_to_foreground(window)
    keybd_event(alt_key, 0, extended_key | key_up, 0)


class RemovePart(object):
    def __init__(self, number_from_library, quantity):
        self.quantity = quantity
        _altium_database_class = AltiumDatabase()
        database = _altium_database_class.database

        number_from_library = number_from_library.replace('NF_', 'nF_').replace('UF_', 'uF_').replace('PF_', 'pF_')

        self.altium_map = {}
        for i in database:
            self.altium_map[i.part_number] = i

        try:
            self.part = self.altium_map[number_from_library]
        except:
            self.part = None

        self.foundElements = []

        self.similarParts = {}

        if self.part is not None and self.part.manufacturer_part_number != "-":
            self.foundElements = find_part(self.part.manufacturer_part_number)

            unwanted_ids = []
            for element in self.foundElements:
                if element.id not in unwanted_ids:
                    unwanted_ids.append(element.id)

            if number_from_library.startswith('R_'):
                self.similarParts = find_resistor(resistance=self.part.parameters['Value'],
                                                  case=self.part.pcblib.replace('R_', ''),
                                                  voltage=self.part.parameters['Voltage'],
                                                  tolerance=self.part.parameters['Tolerance'],
                                                  unwanted_ids=unwanted_ids
                                                  )

            elif number_from_library.startswith('C_'):
                self.similarParts = find_capacitor(capacitance=self.part.parameters['Value'],
                                                   case=self.part.pcblib.replace('C_', ''),
                                                   voltage=self.part.parameters['Voltage'],
                                                   dielectric=self.part.parameters['Dielectric'],
                                                   tolerance=self.part.parameters['Tolerance'],
                                                   unwanted_ids=unwanted_ids
                                                   )
                print(self.part.parameters['Value'])
                print(self.part.pcblib.replace('C_', ''))
                print(self.part.parameters['Voltage'])
                print(self.part.parameters['Dielectric'])
                print(self.part.parameters['Tolerance'])
                print(self.part.manufacturer_part_number)


        self.window = tk.Tk()
        self.window.bind('<KeyPress-Escape>', func=self.cancel_action)
        self.window.bind('<KeyPress-Return>', func=self.window_enter_action)

        self.window.title("Remove elements")
        self.window.minsize(width=1600, height=800)

        self.window.rowconfigure([0, 1, 2, 3, 4, 5, 6], minsize=100, weight=1)
        self.window.columnconfigure(0, minsize=800, weight=1)

        tk.Label(self.window, borderwidth=1, text=number_from_library + '    ' + str(quantity), font="Consolas 50 bold").grid(row=0, column=0)

        self.found_elements_frame = tk.Frame(master=self.window, borderwidth=2, relief=tk.RAISED)
        self.found_elements_frame.grid(row=1, column=0, rowspan=3, sticky='NSEW')
        self.found_elements_frame.rowconfigure([0, 1, 2, 3, 5], minsize=20, weight=1)
        tk.Label(self.found_elements_frame, text='Found parts in PartKeeps:', font="Consolas 30 bold").grid(row=0, sticky='NW')

        self.similar_elements_frame = tk.Frame(master=self.window, borderwidth=2, relief=tk.RAISED)
        self.similar_elements_frame.grid(row=4, column=0, rowspan=3, sticky='NSEW')
        self.similar_elements_frame.rowconfigure([0, 1, 2, 3, 5], minsize=20, weight=1)
        tk.Label(self.similar_elements_frame, text='Similar parts in PartKeeps:', font="Consolas 30 bold").grid(row=0, sticky='NW')

        def print_part_info(master_frame, part, index, background=None, focus=False):
            element_frame = tk.Frame(master=master_frame, borderwidth=2, relief=tk.SUNKEN, bg=background)
            element_frame.grid(sticky='ESW')

            tk.Label(master=element_frame, text=str(index), font="Consolas 20 bold", width=2).grid(column=0, row=0, padx=5)
            tk.Label(master=element_frame, text=part.mpn, font="Consolas 20 bold", justify=tk.LEFT, width=20).grid(column=1, row=0, padx=5, sticky='E')
            tk.Label(master=element_frame, text=part.description, font="Consolas 10", justify='left', width=100).grid(column=2, row=0, padx=5, sticky='EW')

            stock_level = tk.Label(master=element_frame, text=part.stock_level, font="Consolas 20 bold", justify='left', width=4)
            stock_level.grid(column=3, row=0, padx=10, sticky='EW')

            tk.Label(master=element_frame, text=part.location, font="Consolas 20 bold", justify='left', width=20).grid(column=4, row=0, padx=5, sticky='EW')

            e1 = tk.Entry(master=element_frame, bd=5, width=10, font="Consolas 20 bold")
            e1.grid(column=5, row=0, padx=5, sticky='W')
            e1.bind('<KeyPress-Escape>', func=self.cancel_action)
            e1.bind('<FocusIn>', func=(lambda event=None, frame=element_frame: self.focusIn(frame)))
            e1.bind('<FocusOut>', func=(lambda event=None, frame=element_frame: self.focusOut(frame)))

            if focus:
                e1.focus()
                e1.insert(0, str(-int(self.quantity)))

            e1.bind('<KeyPress-Return>', func=(lambda event=None, el=part, entry=e1, stock=stock_level: self.changed(el, entry, stock)))

        # Found parts in partkeeper
        backgrounds = {
            'best_parts': 'green',
            'second_parts': 'yellow',
            'third_parts': 'orange',
            'fourth_parts': 'red'
        }

        self.focused_first_element = False
        # self.foundElements = []
        if self.part is None:
            if len(self.foundElements) == 0:
                tk.Label(master=self.found_elements_frame, text='No parts in Partkeepr', font="Consolas 50 bold", justify='left', fg="red").grid(row=1, sticky='EW')
            else:
                tk.Label(master=self.found_elements_frame, text='No parts in Altium Database', font="Consolas 50 bold", justify='left', fg="red").grid(row=1, sticky='EW')
        else:
            for element in self.foundElements:

                print_part_info(self.found_elements_frame, element, self.foundElements.index(element), backgrounds['best_parts'], not self.focused_first_element)
                if not self.focused_first_element:
                    self.focused_first_element = not self.focused_first_element

        if self.similarParts is not None:
            for key, value in self.similarParts.items():
                if value is not None:
                    for element in value:
                        print_part_info(self.similar_elements_frame, element, value.index(element), backgrounds[key], not self.focused_first_element)
                        if not self.focused_first_element:
                            self.focused_first_element = not self.focused_first_element




        # a trick to activate the window (on windows 7)
        self.window.deiconify()

        steal_focus(self.window.winfo_id())

        # call self.close_mod when the close button is pressed
        self.window.protocol("WM_DELETE_WINDOW", self.close_mod)

        # a trick to activate the window (on windows 7)
        self.window.deiconify()

        self.enter_in_entry = False
        self.oldBackground = ''

    def changed(self, element, entry, stock):
        self.enter_in_entry = True

        quantity = int(entry.get())
        part_stock_change(element, quantity)
        stock.config(text=(str(int(stock.cget('text')) + quantity)))

        self.cancel_action()

    def cancel_action(self, event=None):
        self.returning = 'none'
        self.window.quit()

    def close_mod(self):
        self.cancel_action()

    def window_enter_action(self, event=None):
        if self.enter_in_entry:
            self.enter_in_entry = False
            return

        # print('from window')

    def focusOut(self, frame):
        frame.config(bg=self.oldBackground)
        # print('left')

    def focusIn(self, frame):
        self.oldBackground = frame.cget('bg')
        frame.config(bg='blue')
        # print('Clicked')


def remove_part(number_from_library, quantity):
    msgbox = RemovePart(number_from_library, quantity)
    msgbox.window.mainloop()
    # the function pauses here until the mainloop is quit
    msgbox.window.destroy()
    return msgbox.returning


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print('Usage: <app name> <Altium part number> <Quantity>')
        input()
        exit(0)
    else:

        altium_name = str(sys.argv[1])
        quantity = str(sys.argv[2])
        remove_part(altium_name, int(quantity))


    # remove_part("CMP-2000-06453-1", 10)
    # remove_part("C_GENERIC_1210", 10)

