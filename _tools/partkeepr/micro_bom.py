import progressbar
from tkinter.messagebox import showerror
import sys
import os
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from enum import Enum
import partkeepr_api


dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(dir_path + '/..')
from altium_parser.AltiumDatabase.AltiumDatabase import AltiumDatabase

_altium_database_class = AltiumDatabase()
database = _altium_database_class.database


input_file = dir_path + '/../../tmp.txt'
output_file = dir_path + '/../../tmp.xlsx'

parts = {}

with open(input_file, 'r') as _file:
    for line in _file:
        l = line.strip().split(' ')
        designator = l[0]
        part_name = l[1]

        if part_name not in parts:
            parts[part_name] = [designator]
        else:
            parts[part_name].append(designator)

altium_map = {}
for i in database:
    altium_map[i.part_number] = i


class PartState(Enum):
    OK = 0
    EmptyMPN = 1
    NotFoundInAltium = 2
    NotFoundInPartKeepr = 3
    AmbiguousInPartKeepr = 4


part_output = {
    PartState.OK: [],
    PartState.EmptyMPN: [],
    PartState.NotFoundInAltium: [],
    PartState.NotFoundInPartKeepr: [],
    PartState.AmbiguousInPartKeepr: [],
}

for i in progressbar.progressbar(parts):
    if i not in altium_map:
        # print("Part {} not in altium library! {}".format(i, parts[i]))
        part_output[PartState.NotFoundInAltium].append([
            i, len(parts[i]),
        ])
    else:
        mpn = altium_map[i].manufacturer_part_number

        if mpn.strip() == '' or mpn.strip() == '-':
            part_output[PartState.EmptyMPN].append([
                i, mpn, len(parts[i])
            ])
            continue

        partkeepr = partkeepr_api.find_part(mpn)
        if len(partkeepr) == 0:
            part_output[PartState.NotFoundInPartKeepr].append([
                i, mpn, len(parts[i])
            ])
        elif len(partkeepr) == 1:
            part_output[PartState.OK].append([
                i, mpn, len(parts[i]), partkeepr[0].stock_level, partkeepr[0].location,
            ])
        else:
            part_output[PartState.AmbiguousInPartKeepr].append([
                i, mpn, len(parts[i]), partkeepr
            ])

workbook = xlsxwriter.Workbook(output_file)
worksheet = workbook.add_worksheet('elements')

row_counter = 0
worksheet.write(row_counter, 0, "Part Number")
worksheet.write(row_counter, 1, "Manufacturer Part Number")
worksheet.write(row_counter, 2, "Required quantity")
worksheet.write(row_counter, 3, "Available quantity")
worksheet.write(row_counter, 4, "Storage location")
worksheet.write(row_counter, 5, "Margin")

worksheet.write('I1', "Nr of boards to assemble")
worksheet.write('J1', 1)
worksheet.write('I2', "Elements margin")
worksheet.write('J2', 0)
row_counter += 1

background = workbook.add_format()
background.set_bg_color('red')

part_output[PartState.NotFoundInAltium].sort(key=lambda x: x[0])
for part in part_output[PartState.NotFoundInAltium]:
    worksheet.write(row_counter, 0, part[0], background)
    worksheet.write(row_counter, 1, "NOT FOUND IN ALTIUM LIBRARY", background)
    worksheet.write(row_counter, 2, part[1], background)
    row_counter += 1

background = workbook.add_format()
background.set_bg_color('orange')

part_output[PartState.NotFoundInPartKeepr].sort(key=lambda x: x[0])
for part in part_output[PartState.NotFoundInPartKeepr]:
    worksheet.write(row_counter, 0, part[0], background)
    worksheet.write(row_counter, 1, part[1], background)
    worksheet.write(row_counter, 2, part[2], background)
    worksheet.write(row_counter, 3, "NOT FOUND IN PARTKEEPR", background)
    row_counter += 1

background = workbook.add_format()
background.set_bg_color('yellow')

part_output[PartState.AmbiguousInPartKeepr].sort(key=lambda x: x[0])
for part in part_output[PartState.AmbiguousInPartKeepr]:
    worksheet.write(row_counter, 0, part[0], background)
    worksheet.write(row_counter, 1, part[1], background)
    worksheet.write(row_counter, 2, part[2], background)
    worksheet.write(row_counter, 3, "AMBIGUOUS PART IN PARTKEEPR", background)
    column = 4
    color = '#FFFF00'
    for p in part[3]:
        if color == '#FFAA00':
            color = '#FFFF00'
        else:
            color = '#FFAA00'
        background = workbook.add_format()
        background.set_bg_color(color)
        worksheet.write(row_counter, column, str(p.mpn), background)
        worksheet.write(row_counter, column+1, str(p.location), background)
        worksheet.write(row_counter, column+2, p.stock_level, background)
        column += 3
    row_counter += 1

background = workbook.add_format()
background.set_bg_color('#FFBB00')

part_output[PartState.EmptyMPN].sort(key=lambda x: x[0])
for part in part_output[PartState.EmptyMPN]:
    worksheet.write(row_counter, 0, part[0], background)
    worksheet.write(row_counter, 1, part[1], background)
    worksheet.write(row_counter, 2, part[2], background)
    worksheet.write(row_counter, 3, "EMPTY MPN", background)
    row_counter += 1

background = workbook.add_format()
background.set_bg_color('red')

part_output[PartState.OK].sort(key=lambda x: x[3]-x[2])
for part in part_output[PartState.OK]:
    margin = "={}-$J$1*{} - $J$2".format(xl_rowcol_to_cell(row_counter, 3), xl_rowcol_to_cell(row_counter, 2))

    worksheet.write(row_counter, 0, part[0])
    worksheet.write(row_counter, 1, part[1])
    worksheet.write(row_counter, 2, part[2])
    worksheet.write(row_counter, 3, part[3])
    worksheet.write(row_counter, 4, part[4])
    worksheet.write(row_counter, 5, margin)
    row_counter += 1

worksheet.conditional_format('F1:F9999',
                             {'type': 'cell',
                              'criteria': '<',
                              'value': 0,
                              'format': background})

from typing import Optional
from xlsxwriter.worksheet import (
    Worksheet, cell_number_tuple, cell_string_tuple)


def get_column_width(worksheet: Worksheet, column: int) -> Optional[int]:
    """Get the max column width in a `Worksheet` column."""
    strings = getattr(worksheet, '_ts_all_strings', None)
    if strings is None:
        strings = worksheet._ts_all_strings = sorted(
            worksheet.str_table.string_table,
            key=worksheet.str_table.string_table.__getitem__)
    lengths = set()
    for row_id, colums_dict in worksheet.table.items():  # type: int, dict
        data = colums_dict.get(column)
        if not data:
            continue
        if type(data) is cell_string_tuple:
            iter_length = len(strings[data.string])
            if not iter_length:
                continue
            lengths.add(iter_length)
            continue
        if type(data) is cell_number_tuple:
            iter_length = len(str(data.number))
            if not iter_length:
                continue
            lengths.add(iter_length)
    if not lengths:
        return None
    return max(lengths)


def set_column_autowidth(worksheet: Worksheet, column: int):
    """
    Set the width automatically on a column in the `Worksheet`.
    !!! Make sure you run this function AFTER having all cells filled in
    the worksheet!
    """
    maxwidth = get_column_width(worksheet=worksheet, column=column)
    if maxwidth is None:
        return
    worksheet.set_column(first_col=column, last_col=column, width=1.25*maxwidth)

for i in range(0, 30):
    set_column_autowidth(workbook.get_worksheet_by_name('elements'), i)

try:
    workbook.close()
    os.system('start "excel" "{}"'.format(output_file))
except:
    showerror("", "Cannot save excel output file!\nIs it opened already?")
