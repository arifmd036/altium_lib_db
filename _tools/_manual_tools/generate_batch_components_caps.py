import os, csv
import webbrowser
from altium_parser.AltiumDatabase.AltiumDatabase import AltiumDatabase
import pyperclip
import msvcrt
from search_octopart import octopart_search

elements_raw = \
"""
GRM1555C1E103JE01D	25	10000
GRM1555C1H102JA01D	50	1000
GRM1555C1H122JA01D	50	1200
GRM1555C1H152JA01D	50	1500
GRM1555C1H182JA01D	50	1800
GRM1555C1H222JA01D	50	2200
GRM1555C1H271JA01D	50	270
GRM1555C1H272JE01D	50	2700
GRM1555C1H331JA01D	50	330
GRM1555C1H332JE01D	50	3300
GRM1555C1H391JA01D	50	390
GRM1555C1H392JE01D	50	3900
GRM1555C1H471JA01D	50	470
GRM1555C1H472JE01D	50	4700
GRM1555C1H561JA01D	50	560
GRM1555C1H562JE01D	50	5600
GRM1555C1H681JA01D	50	680
GRM1555C1H682JE01D	50	6800
GRM1555C1H821JA01D	50	820
GRM1555C1H822JE01D	50	8200
GRM1555C2A102JE01D	100	1000
GRM1555C2A121JE01D	100	120
GRM1555C2A151JE01D	100	150
GRM1555C2A181JE01D	100	180
GRM1555C2A221JE01D	100	220
GRM1555C2A271JE01D	100	270
GRM1555C2A331JE01D	100	330
GRM1555C2A391JE01D	100	390
GRM1555C2A471JE01D	100	470
GRM1555C2A561JE01D	100	560
GRM1555C2A681JE01D	100	680
GRM1555C2A821JE01D	100	820
GRM1555CYA103JE01D	35	10000
"""

elements_raw = elements_raw.strip()

with open('../_export/Capacitors.csv', newline='') as f:
    reader = csv.reader(f)
    columns = next(reader)  # gets the first line

# elements = []
for line in elements_raw.split('\n'):
    name, voltage, capacitance = line.split()

    capacitance = float(capacitance)
    capacitance_suffix = 'p'
    if capacitance >= 1000:
        capacitance /= 1000
        capacitance_suffix = 'n'
    if capacitance >= 1000:
        capacitance /= 1000
        capacitance_suffix = 'u'

    capacitance = str(capacitance) + capacitance_suffix + 'F'
    voltage = str(int(voltage)) + "V"

    element = {}
    element["Part Number"] = "C_" + capacitance + "_" + voltage + "C0G_0402_5"
    element["SCH_Footprint"] = "0402"
    element["Comment"] = "Ceramic C0G"
    element["Voltage"] = voltage
    element["Value"] = capacitance
    element["Dielectric"] = 'C0G'
    element["Tolerance (%)"] = "5"
    element["HelpURL"] = "https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c02e.ashx"
    element["Library Ref"] = "C"
    element["Footprint Ref"] = "C_0402_1005"
    element["Manufacturer"] = "Murata"
    element["Manufacturer Part Number"] = name
    element["Supplier 1"] = "TME"
    element["Supplier Part Number 1"] = ""
    element["Supplier 2"] = "RS Components"
    element["Supplier Part Number 2"] = ""
    element["Supplier 3"] = "Farnell"
    element["Supplier Part Number 3"] = ""
    element["Supplier 4"] = "Mouser"
    element["Supplier Part Number 4"] = ""
    element["Supplier 5"] = "Digi-Key"
    element["Supplier Part Number 5"] = ""    
    element["Supplier 6"] = ""
    element["Supplier Part Number 6"] = ""

    offers = octopart_search(name)

    for i in range(1, 7):
        try:
            offer = offers[element["Supplier {}".format(i)]]
            if len(offer) > 1:
                print("More than one of {}!".format(name))
                print(offer)
            element["Supplier Part Number {}".format(i)] = next(iter(offer))
        except:
            pass

    for i in element:
        if element[i] != "":
            element[i] = "\"" + element[i] + "\""

    print(",".join([element[c] for c in columns]))
