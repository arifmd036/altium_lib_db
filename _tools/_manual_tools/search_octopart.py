import os
os.environ['OCTOPART_API_KEY'] = 'f9b9d8b8be068aaf2a60'
import octopart_v4
import copy

def octopart_search(mpn):
    results = octopart_v4.match([mpn])
    
    assert len(results) == 1

    offers = {}
    for parts in results[0].parts:
        for i in parts.offers:
            if i.seller not in ["TME", "RS Components", "Farnell", "Mouser", "Digi-Key"]:
                continue
             
            if i.sku[-2:] == "RL":
                continue

            if i.seller == "RS Components" and i.sku[-1] == 'P':
                continue

            if type(i.moq) is int and i.moq > 100:
                continue

            if i.seller not in offers:
                offers[i.seller] = set()
            offers[i.seller].add(i)
    
    for seller in offers:
        # select only 'cut tape' 
        if len(offers[seller]) > 1:
            # print(seller)
            next = set()
            for offer in offers[seller]:
                # print(offer.packaging)
                if offer.packaging == "Cut Tape":
                    next.add(offer)
            
            if len(next) == 0:
                print("fail {} @ {}!!!".format(mpn, seller))
            else:
                offers[seller] = next
        
        skus = set()
        for offer in offers[seller]:
            skus.add(offer.sku)
        offers[seller] = skus

    return offers

if __name__ == "__main__":
    print(octopart_search("0805CS-680XJLC"))