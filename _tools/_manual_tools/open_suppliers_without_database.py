import webbrowser

# Open suppliers without chacking database

print("Part number: ", end='')
part_number = input()

# TME
url = 'https://www.tme.eu/pl/katalog/#search=' + (part_number)
webbrowser.open_new_tab(url)

# RSComponents
url = 'https://pl.rs-online.com/web/c?searchTerm=' + (part_number)
webbrowser.open_new_tab(url)

# Farnell
url = 'https://pl.farnell.com/search?st=' + (part_number)
webbrowser.open_new_tab(url)

# Mouser
url = 'https://eu.mouser.com/Search/Refine.aspx?Keyword=' + (part_number)
webbrowser.open_new_tab(url)

# Digi-Key
url = 'https://www.digikey.com/products/en?keywords=' + (part_number)
webbrowser.open_new_tab(url)

# LCSC
url = "https://lcsc.com/search?q=" + (part_number)
webbrowser.open_new_tab(url)

# octopart
url = "https://octopart.com/search?q=" + (part_number)
webbrowser.open_new_tab(url)


