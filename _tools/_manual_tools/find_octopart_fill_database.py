import os
import sys
import configparser
import keyboard, time, pyperclip
from tkinter import Tk

repo_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, os.pardir)
sys.path.append(os.path.join(repo_path, '_tools', 'partkeepr'))

config = configparser.ConfigParser()
config.read(os.path.join(repo_path, 'config.ini'))
config = config["octopart"]

import octopart_v4


def octopart_search(mpn):
    element = octopart_v4.OctopartElement(mpn, token=config['api'])
    return element.sellers_by_name

if __name__ == "__main__":
    time.sleep(2)
    
    keyboard.send('home')
    keyboard.send('left')
    keyboard.send('right')
    keyboard.send('end')
    for i in range(15): keyboard.send('left')
    time.sleep(0.1)
    keyboard.send('ctrl+c')
    time.sleep(0.1)
    name = Tk().clipboard_get()
    
    part = octopart_search(name)
    
    for name in ['TME', 'RS Components', 'Farnell', 'Mouser', 'Digi-Key']:
        time.sleep(0.1)
        keyboard.send('right')
        keyboard.send('right')
        time.sleep(0.1)
        if name in part:
            print(name, part[name])
            keyboard.write(part[name][0]['sku'])
