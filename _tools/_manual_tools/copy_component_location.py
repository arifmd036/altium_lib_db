import os
import sys
import argparse
import tempfile

import numpy
from pathlib import Path
import clipboard


class Component:
    def __init__(self, description, id):
        self.id = id
        self.component_comment = description[8]
        self.footprint = description[12]
        self.channel_offset = description[13]

        self.x = description[2]
        self.y = description[3]
        self.rotation = description[14]

        self.object_kind = description[0]
        self.layer = description[1]
        self.height = description[4]
        self.locked = description[5]
        self.designator = description[6]

    def __str__(self):
        dictionary = dict(id=self.id,
                          component_comment=self.component_comment,
                          footprint=self.footprint,
                          channel_offset=self.channel_offset
                          )
        return str(dictionary)

    def __repr__(self):
        return str(self)


def main(argv):
    print("Go and watch https://www.youtube.com/watch?v=wgdCJ1rbNnA before using")
    parser = argparse.ArgumentParser(description='Copy component PCB locations. '
                                                 'Based on https://www.youtube.com/watch?v=wgdCJ1rbNnA')
    parser.add_argument('-s', '-source', type=str, help='Source file path')
    parser.add_argument('-d', '-destination', type=str, help='Destination file path')
    parser.add_argument('-x', '--X', type=int, default=0, help='Move destination components in x')
    parser.add_argument('-y', '--Y', type=int, default=0, help='Move destination components in y')
    parser.add_argument('-c', '--C', action='store_true', help='Copy mode')

    args = vars(parser.parse_args(argv))

    copy_mode = bool(args['C'])

    if copy_mode:
        source_file_path = input("Paste source file path: ")
        destination_file_path = input("Paste destination file path: ")
        x_move = int(input("X move: "))
        y_move = int(input("Y move: "))

    else:
        if args['s'] is None or args['d'] is None:
            print("You have to select copy mode or add source and destination file path. \nUse -h (--help) for more info")
            return

        source_file_path = args['s']
        destination_file_path = args['d']
        x_move = int(args['X'])
        y_move = int(args['Y'])

    source_file = open(source_file_path, 'r')
    destination_file = open(destination_file_path, 'r')
    source_lines = source_file.readlines()
    destination_lines = destination_file.readlines()

    source_components = []
    destination_components = []

    for line in source_lines:
        description = line.split('\t')
        if description[0] == 'Component':
            source_components.append(Component(description, source_lines.index(line)))

    for line in destination_lines:
        description = line.split('\t')
        if description[0] == 'Component':
            destination_components.append(Component(description, destination_lines.index(line)))

    if len(source_components) != len(destination_components):
        print("Components lists are not equal!")
        return

    source_components = sorted(source_components, key=lambda e: (e.footprint, e.component_comment, e.channel_offset))
    destination_components = sorted(destination_components,
                                    key=lambda e: (e.footprint, e.component_comment, e.channel_offset))

    for i in range(len(source_components)):
        assert source_components[i].footprint == destination_components[i].footprint
        destination_components[i].x = numpy.format_float_positional(float(source_components[i].x) + x_move, 3, trim='-')
        destination_components[i].y = numpy.format_float_positional(float(source_components[i].y) + y_move, 3, trim='-')
        destination_components[i].rotation = numpy.format_float_positional(
            float(source_components[i].rotation.replace(',', '.')), 3, trim='-')

    destination_components = sorted(destination_components, key=lambda e: e.id)

    out_data = "X\tY\tRotation\n"
    for component in destination_components:
        out_data += component.x + '\t' + component.y + '\t' + component.rotation + '\n'

    if copy_mode:
        out_file_path = tempfile.gettempdir() + "\\out.txt"
    else:
        p = Path(args['d'])
        out_file_path = str(p.parent) + "\\out.txt"

    f = open(out_file_path, "w")
    f.write(out_data)
    f.close()
    os.system(out_file_path)


if __name__ == "__main__":
    main(sys.argv[1:])
