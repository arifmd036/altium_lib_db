import os, csv
import webbrowser
from altium_parser.AltiumDatabase.AltiumDatabase import AltiumDatabase
import pyperclip
import msvcrt
from search_octopart import octopart_search

elements_raw = \
"""
0402CS-1N0XJLW	1	0.045	1360
0402CS-1N2XJLW	1.2	0.09	740
0402CS-1N8XJLW	1.8	0.07	1040
0402CS-1N9XJLW	1.9	0.07	1040
0402CS-2N0XJLW	2	0.07	1040
0402CS-2N2XJLW	2.2	0.07	960
0402CS-2N4XJLW	2.4	0.068	790
0402CS-2N7XJLW	2.7	0.12	640
0402CS-3N3XJLW	3.3	0.066	840
0402CS-3N6XJLW	3.6	0.066	840
0402CS-3N9XJLW	3.9	0.066	840
0402CS-4N3XJLW	4.3	0.091	700
0402CS-4N7XJLW	4.7	0.13	640
0402CS-5N1XJLW	5.1	0.083	800
0402CS-5N6XJLW	5.6	0.083	760
0402CS-6N2XJLW	6.2	0.083	760
0402CS-6N8XJLW	6.8	0.083	680
0402CS-7N5XJLW	7.5	0.1	680
0402CS-8N2XJLW	8.2	0.1	680
0402CS-8N7XJLW	8.7	0.2	480
0402CS-9N0XJLW	9	0.1	681
0402CS-9N5XJLW	9.5	0.2	480
0402CS-10NXJLW	10	0.2	480
0402CS-11NXJLW	11	0.12	640
0402CS-12NXJLW	12	0.12	640
0402CS-13NXJLW	13	0.21	440
0402CS-15NXJLW	15	0.17	560
0402CS-16NXJLW	16	0.22	560
0402CS-18NXJLW	18	0.23	420
0402CS-19NXJLW	19	0.2	480
0402CS-20NXJLW	20	0.25	420
0402CS-22NXJLW	22	0.3	400
0402CS-23NXJLW	23	0.3	400
0402CS-24NXJLW	24	0.3	400
0402CS-27NXJLW	27	0.3	400
0402CS-30NXJLW	30	0.3	400
0402CS-33NXJLW	33	0.3	400
0402CS-36NXJLW	36	0.44	320
0402CS-39NXJLW	39	0.55	200
0402CS-40NXJLW	40	0.44	320
0402CS-43NXJLW	43	0.81	100
0402CS-47NXJLW	47	0.83	150
0402CS-51NXJLW	51	0.82	100
0402CS-56NXJLW	56	0.97	100
0402CS-68NXJLW	68	1.12	100
0402CS-82NXJLW	82	1.55	50
0402CS-R10XJLW	100	2	30
0402CS-R12XJLW	120	2.2	50

"""

elements_raw = elements_raw.strip()

with open('../_export/Inductors.csv', newline='') as f:
    reader = csv.reader(f)
    columns = next(reader)  # gets the first line

# elements = []
for line in elements_raw.split('\n'):
    name, value, resistance, current = line.split()

    resistance = float(resistance)
    resistance_suffix = ''
    if resistance < 1:
        resistance *= 1000
        resistance_suffix = 'm'
    if resistance < 1:
        resistance *= 1000
        resistance_suffix = 'u'

    element = {}
    element["Part Number"] = "L_" + name
    element["SCH_Footprint"] = "0603"
    element["Comment"] = name
    element["Voltage"] = ""
    element["Current"] = str(int(current)) + "mA"
    element["Resistance"] = str(resistance) + resistance_suffix + 'R'
    element["Value"] = value + 'nH'
    element["HelpURL"] = "https://www.coilcraft.com/0603cs.cfm"
    element["Library Ref"] = "L"
    element["Footprint Ref"] = "L_0603_1608"
    element["Manufacturer"] = "Coilcraft"
    element["Manufacturer Part Number"] = name
    element["Supplier 1"] = "TME"
    element["Supplier Part Number 1"] = ""
    element["Supplier 2"] = "RS Components"
    element["Supplier Part Number 2"] = ""
    element["Supplier 3"] = "Farnell"
    element["Supplier Part Number 3"] = ""
    element["Supplier 4"] = "Mouser"
    element["Supplier Part Number 4"] = ""
    element["Supplier 5"] = "Digi-Key"
    element["Supplier Part Number 5"] = ""    
    element["Supplier 6"] = "Coilcraft Direct"
    element["Supplier Part Number 6"] = ""

    offers = octopart_search(name)

    for i in range(1, 7):
        try:
            offer = offers[element["Supplier {}".format(i)]]
            if len(offer) > 1:
                print("More than one of {}!".format(name))
                print(offer)
            element["Supplier Part Number {}".format(i)] = next(iter(offer))
        except:
            pass

    element["Supplier 6"] = "Coilcraft"

    for i in element:
        if element[i] != "":
            element[i] = "\"" + element[i] + "\""

    print(",".join([element[c] for c in columns]))
