from enum import Enum


class TextJustification(Enum):
    left_bottom = 0
    middle_bottom = 1
    right_bottom = 2
    left_center = 3
    middle_center = 4
    right_center = 5
    left_top = 6
    middle_top = 7
    right_top = 8


class LineWidth(Enum):
    Smallest = 0
    Small = 1
    Medium = 2
    Large = 3


class LineStyle(Enum):
    Solid = 0
    Dashed = 1
    Dotted = 2
    DashDotted = 3


class LineShape(Enum):
    Nothing = 0 #None, but it cant be like that
    Arrow = 1
    SolidArrow = 2
    Tail = 3
    SolidTail = 4
    Circle = 5
    Square = 6


class Color:
    def __init__(self, dict, name):
        self.R = 0
        self.G = 0
        self.B = 0
        if name in dict:
            c = int(dict[name])
            self.R = c % 256
            self.G = (c >> 8) % 256
            self.B = (c >> 16) % 256

        self._key = str(self.R) + str(self.G) + str(self.B)


def parse_Color(self, dict):
    self.color = Color(dict, 'COLOR')


def parse_area_Color(self, dict):
    self.area_color = Color(dict, 'AREACOLOR')


def parse_XY(self, dict):
    self.X = 0
    self.Y = 0
    if 'LOCATION.X' in dict:
        self.X = 10. * int(dict['LOCATION.X'])
    if 'LOCATION.X_FRAC' in dict:
        self.X += int(dict['LOCATION.X_FRAC']) / 10000.

    if 'LOCATION.Y' in dict:
        self.Y = 10. * int(dict['LOCATION.Y'])
    if 'LOCATION.Y_FRAC' in dict:
        self.Y += int(dict['LOCATION.Y_FRAC']) / 10000.


def parse_multiple_XY(self, dict, points):
    self.X = []
    self.Y = []
    for i in range(1, points+1):
        if 'X'+str(i) in dict:
            self.X.append(10. * int(dict['X'+str(i)]))
        else:
            self.X.append(0)
        if 'X'+str(i)+"_FRAC" in dict:
            self.X[i-1] += int(dict['X'+str(i)+"_FRAC"]) / 10000.

        if 'Y'+str(i) in dict:
            self.Y.append(10. * int(dict['Y'+str(i)]))
        else:
            self.Y.append(0)
        if 'Y'+str(i)+"_FRAC" in dict:
            self.Y[i-1] += int(dict['Y'+str(i)+"_FRAC"]) / 10000.


def parse_corner_XY(self, dict):
    self.corner_X = 0
    self.corner_Y = 0
    if 'CORNER.X' in dict:
        self.corner_X = 10. * int(dict['CORNER.X'])
    if 'CORNER.X_FRAC' in dict:
        self.corner_X += int(dict['CORNER.X_FRAC']) / 10000.

    if 'CORNER.Y' in dict:
        self.corner_Y = 10. * int(dict['CORNER.Y'])
    if 'CORNER.Y_FRAC' in dict:
        self.corner_Y += int(dict['CORNER.Y_FRAC']) / 10000.


def parse_bezier_XY(self, dict, points):
    self.X = []
    self.Y = []
    for i in range(1, points+1):
        if 'X'+str(i) in dict:
            self.X.append(int(dict['X'+str(i)]))
        else:
            self.X.append(0)

        if 'Y'+str(i) in dict:
            self.Y.append(int(dict['Y'+str(i)]))
        else:
            self.Y.append(0)


def parse_linewith(self, dict):
    self.line_width = LineWidth(0)
    if 'LINEWIDTH' in dict:
        self.line_width = LineWidth(int(dict['LINEWIDTH']))


def parse_linestyle(self, dict):
    self.line_style = LineStyle(0)
    self.start_line_shape = LineShape(0)
    self.end_line_shape = LineShape(0)
    self.line_shape_size = LineWidth(0)
    if 'LINESTYLEEXT' in dict:
        self.line_style = LineStyle(int(dict['LINESTYLEEXT']))
    if 'STARTLINESHAPE' in dict:
        self.start_line_shape = LineShape(int(dict['STARTLINESHAPE']))
    if 'ENDLINESHAPE' in dict:
        self.end_line_shape = LineShape(int(dict['ENDLINESHAPE']))
    if 'LINESHAPESIZE' in dict:
        self.line_shape_size = LineWidth(int(dict['LINESHAPESIZE']))


def parse_angle(self, dict):
    self.start_angle = 0
    self.end_angle = 0
    if 'STARTANGLE' in dict:
        self.start_angle = float(dict['STARTANGLE'])
    if 'ENDANGLE' in dict:
        self.end_angle += float(dict['ENDANGLE'])


def parse_radius(self, dict):
    self.radius = 0
    if 'RADIUS' in dict:
        self.radius = 10. * int(dict['RADIUS'])
    if 'RADIUS_FRAC' in dict:
        self.radius += int(dict['RADIUS_FRAC']) / 10000.


def parse_solid(self, dict):
    if 'ISSOLID' in dict:
        self.solid = True
    else:
        self.solid = False


def parse_transparent(self, dict):
    if 'TRANSPARENT' in dict:
        self.transparent = True
    else:
        self.transparent = False


def parse_radius(self, dict):
    self.radius = 0
    if 'RADIUS' in dict:
        self.radius = 10. * int(dict['RADIUS'])
    if 'RADIUS_FRAC' in dict:
        self.radius += int(dict['RADIUS_FRAC']) / 10000.


def parse_secondary_radius(self, dict):
    self.secondary_radius = 0
    if 'SECONDARYRADIUS' in dict:
        self.secondary_radius = 10. * int(dict['SECONDARYRADIUS'])
    if 'SECONDARYRADIUS_FRAC' in dict:
        self.secondary_radius += int(dict['SECONDARYRADIUS_FRAC']) / 10000.


def parse_corner_XY_radius(self, dict):
    self.corner_X_radius = 0
    self.corner_Y_radius = 0
    if 'CORNERXRADIUS' in dict:
        self.corner_X_radius = 10. * int(dict['CORNERXRADIUS'])
    if 'CORNERXRADIUS_FRAC' in dict:
        self.corner_X_radius += int(dict['CORNERXRADIUS_FRAC']) / 10000.

    if 'CORNERYRADIUS' in dict:
        self.corner_Y_radius = 10. * int(dict['CORNERYRADIUS'])
    if 'CORNERYRADIUS_FRAC' in dict:
        self.corner_Y_radius += int(dict['CORNERYRADIUS_FRAC']) / 10000.