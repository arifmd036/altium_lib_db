from SchLib.Primitives.Primitive import *


class SchLib_SimpleLine(Primitive):
    def __init__(self, schlib, dict):
        Primitive.__init__(self, dict)

        parse_linewith(self, dict)
        parse_XY(self, dict)
        parse_corner_XY(self, dict)
        parse_Color(self, dict)

        #  convert to the same format as the normal line
        self.X = [self.X, self.corner_X]
        self.Y = [self.Y, self.corner_Y]

        #  defaults for simple line
        self.line_width = LineWidth.Small
        self.line_style = LineStyle.Solid

        schlib.primitives.append(self)


class SchLib_Line(Primitive):
    def __init__(self, schlib, dict):
        Primitive.__init__(self, dict)
        parse_linewith(self, dict)
        parse_linestyle(self, dict)
        parse_multiple_XY(self, dict, int(dict['LOCATIONCOUNT']))
        parse_Color(self, dict)

        schlib.primitives.append(self)
        