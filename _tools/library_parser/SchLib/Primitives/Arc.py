from SchLib.Primitives.Primitive import *


class SchLib_Arc(Primitive):
    def __init__(self, schlib, dict):
        Primitive.__init__(self, dict)
        parse_radius(self, dict)
        parse_angle(self, dict)
        parse_Color(self, dict)
        parse_linewith(self, dict)
        schlib.primitives.append(self)


class SchLib_EllipticalArc(Primitive):
    def __init__(self, schlib, dict):
        Primitive.__init__(self, dict)
        parse_radius(self, dict)
        parse_secondary_radius(self, dict)
        parse_angle(self, dict)
        parse_Color(self, dict)
        parse_linewith(self, dict)

        schlib.primitives.append(self)