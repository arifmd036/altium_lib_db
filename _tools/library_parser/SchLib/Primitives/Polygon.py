from SchLib.Primitives.Primitive import *


class SchLib_Polygon(Primitive):  # trzeba będzie troche poprawić
    def __init__(self, schlib, dict):
        Primitive.__init__(self, dict)
        parse_linewith(self, dict)
        parse_Color(self, dict)
        parse_area_Color(self, dict)
        parse_multiple_XY(self, dict, int(dict['LOCATIONCOUNT']))
        parse_solid(self, dict)
        parse_transparent(self, dict)

        schlib.primitives.append(self)