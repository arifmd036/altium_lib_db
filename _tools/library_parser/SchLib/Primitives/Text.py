from SchLib.Primitives.Primitive import *


def get_str(dict, name):
    if name in dict:
        return dict[name]
    return ''


class SchLib_TextTemplate:
    def __init__(self, dict):
        self.owner_part_id = int(dict['OWNERPARTID'])
        parse_XY(self, dict)
        parse_Color(self, dict)
        self.font_id = dict['FONTID']
        self.text = get_str(dict, 'TEXT')
        if 'ISHIDDEN' in dict:
            self.is_hidden = True
        else:
            self.is_hidden = False

        if 'JUSTIFICATION' in dict:
            self.justification = TextJustification(int(dict['JUSTIFICATION']))
        else:
            self.justification = TextJustification(0)


class SchLib_TextLabel(SchLib_TextTemplate, Primitive):
    def __init__(self, schlib, dict):
        SchLib_TextTemplate.__init__(self, dict)
        Primitive.__init__(self, dict)

        if 'JUSTIFICATION' in dict:
            self.justification = TextJustification(int(dict['JUSTIFICATION']))
        else:
            self.justification = TextJustification(0)
        if 'URL' in dict:
            self.url = dict['URL']
        else:
            self.url = ""

        schlib.text_labels.append(self)
        schlib.primitives.append(self)


class SchLib_Parameter(SchLib_TextTemplate, Primitive):
    def __init__(self, schlib, dict):
        SchLib_TextTemplate.__init__(self, dict)
        Primitive.__init__(self, dict)
        self.name = dict['NAME']

        if 'SHOWNAME' in dict:
            self.text = self.name + ': ' + self.text

        if self.name == 'Comment':
            self.index_in_sheet = dict['INDEXINSHEET']

        if 'READONLYSTATE' in dict:
            self.lock_parameter = True
        else:
            self.lock_parameter = False

        if 'NOTAUTOPOSITION' in dict:
            self.autoposition = False
        else:
            self.autoposition = True

        if 'NOTALLOWDATABASESYNCHRONIZE' in dict:
            self.allow_synchronization_with_database = False
        else:
            self.allow_synchronization_with_database = True

        schlib.parameters.append(self)


class SchLib_Designator(SchLib_TextTemplate, Primitive):
    def __init__(self, schlib, dict):
        SchLib_TextTemplate.__init__(self, dict)
        Primitive.__init__(self, dict)
        parse_Color(self, dict)
        self.text = dict['TEXT']
        assert dict['NAME'] == 'Designator'

        assert schlib.designator is None
        schlib.designator = self

        if 'READONLYSTATE' in dict and dict['READONLYSTATE'] == '3':
            self.lock_parameter = True
        else:
            self.lock_parameter = False

        if 'OVERRIDENOTAUTOPOSITION' in dict:
            self.autoposition = False
        else:
            self.autoposition = True
