from SchLib.Primitives.Primitive import *


class SchLib_Bezier(Primitive):
    def __init__(self, schlib, dict):
        Primitive.__init__(self, dict)
        parse_linestyle(self, dict)
        parse_linewith(self, dict)
        parse_Color(self, dict)
        parse_multiple_XY(self, dict, int(dict['LOCATIONCOUNT']))

        schlib.primitives.append(self)