from SchLib.Primitives.Primitive import *


class SchLib_Rectangle(Primitive):
    def __init__(self, schlib, dict):
        Primitive.__init__(self, dict)
        parse_corner_XY(self, dict)
        parse_Color(self, dict)
        parse_area_Color(self, dict)
        parse_linewith(self, dict)
        parse_solid(self, dict)
        parse_transparent(self, dict)
        schlib.primitives.append(self)


class SchLib_RoundedRectangle(SchLib_Rectangle):
    def __init__(self, schlib, dict):
        SchLib_Rectangle.__init__(self, schlib, dict)
        parse_corner_XY_radius(self, dict)
        schlib.primitives.append(self)