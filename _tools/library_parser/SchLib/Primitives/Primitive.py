from SchLib.parse import *


class TextJustification(Enum):
    left_bottom = 0
    middle_bottom = 1
    right_bottom = 2
    left_center = 3
    middle_center = 4
    right_center = 5
    left_top = 6
    middle_top = 7
    right_top = 8


class LineWidth(Enum):
    Smallest = 0
    Small = 1
    Medium = 2
    Large = 3


class Primitive:
    def __init__(self, dict):
        parse_XY(self, dict)

        # starts from 1
        self.owner_part_id = int(dict['OWNERPARTID'])

        # starts from 1
        if 'INDEXINSHEET' in dict:
            self.index_in_sheet = int(dict['INDEXINSHEET'])
        else:
            self.index_in_sheet = 0

        # alternate mode starts from 0
        if 'OWNERPARTDISPLAYMODE' in dict:
            self.owner_part_display_mode = int(dict['OWNERPARTDISPLAYMODE'])
        else:
            self.owner_part_display_mode = 0
