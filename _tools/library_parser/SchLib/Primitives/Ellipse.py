from SchLib.Primitives.Primitive import *


class SchLib_Ellipse(Primitive):
    def __init__(self, schlib, dict):
        Primitive.__init__(self, dict)
        parse_radius(self, dict)
        parse_secondary_radius(self, dict)
        parse_linewith(self, dict)
        parse_Color(self, dict)
        parse_area_Color(self, dict)
        parse_solid(self, dict)
        parse_transparent(self, dict)

        schlib.primitives.append(self)