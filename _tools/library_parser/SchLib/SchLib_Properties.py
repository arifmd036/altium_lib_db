from .common import parseKeyValueString

from .SchLib_Pin import Pin
from SchLib.Primitives.Arc import *
from SchLib.Primitives.Bezier import *
from SchLib.Primitives.Ellipse import *
from SchLib.Primitives.Line import *
from SchLib.Primitives.Polygon import *
from SchLib.Primitives.Rectangle import *
from SchLib.Primitives.Text import *


class SchLib_Component():
    def __init__(self, schlib, dict):
        schlib.name = dict['LIBREFERENCE']
        schlib.part_count = int(dict['PARTCOUNT']) - 1
        schlib.display_mode_count = int(dict['DISPLAYMODECOUNT'])


class SchLib_IEEE(Primitive):
    def __init__(self, schlib, dict):
        Primitive.__init__(self, dict)
        schlib.primitives.append(self)


class Empty:
    def __init__(self, schlib, dict):
        # print(dict)
        pass


def parse_primitive(schlib, record):
    if record.type == 0:
        dict = parseKeyValueString(record.data.data)
        typ = int(dict["RECORD"])

        mapping = {
            1: SchLib_Component,
            3: SchLib_IEEE,  # IEEE symbol TODO all
            4: SchLib_TextLabel,  # TODO 'URL'
            5: SchLib_Bezier,  # Bezier
            6: SchLib_Line,  # Line TODO start line, end line, shape
            7: SchLib_Polygon,  #
            # Polygon #tutaj jest ciekawa zależność bo jak polygon ma wiecej niż 50 punktów to wszystko ponad tą wartość jest przenoszone  do nowej zmmiennej, o indeksie na -
            8: SchLib_Ellipse,  # Ellipse TODO all
            10: SchLib_RoundedRectangle,  # Rounded Rectangle
            11: SchLib_EllipticalArc,  # Elliptical Arc [Deprecated] TODO all
            12: SchLib_Arc,  # Arc TODO all
            13: SchLib_SimpleLine,  # Line TODO all
            14: SchLib_Rectangle,  # Rectangle TODO all
            28: Empty,  # Text Frame
            30: Empty,  # Image
            34: SchLib_Designator,  #
            41: SchLib_Parameter,  #
            44: Empty  # PartEnd
            # 45: # Footprint model
        }
        # print(data.data)
        # dict = pwt(dict["RECORD"])
        if typ == 45:
            assert False, "Footprint model should not be included in SchLib File: " + schlib.name
        assert typ in mapping, "type " + str(typ) + " not mapped! " + str(dict) + ", report issue!"
        mapping[typ](schlib, dict)

    elif record.type == 1:
        pin = Pin(record.data)
        schlib.primitives.append(pin)
        schlib.pins.append(pin)

    else:
        assert False, "incorrect record " + record.type
