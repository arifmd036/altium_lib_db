import os

from olefile.olefile import OleFileIO

from .SchLib_Pin import Pin
from .SchLib_Properties import parse_primitive
from .common import parseKeyValueString
from .sch_data_pack import DataPack


class SchLib:
    def __init__(self, filename):
        self.filename = os.path.splitext(os.path.basename(filename))[0]
        self.libraries = []
        self.fonts = []
        self.show_comment_designator = False

        self._ole = OleFileIO(filename)

        libraries = self.parse_header()



        self.parse_libraries(libraries)

        self._ole.close()

    def parse_header(self):
        data = self._ole.openstream(['FileHeader']).read()
        data = DataPack(data)

        libraries = []

        dic = parseKeyValueString(data.subrecord().data.read())

        if 'FONTIDCOUNT' in dic:
            for i in range(1, int(dic['FONTIDCOUNT']) + 1):
                self.fonts.append(self.Font(dic, i))

        if 'ALWAYSSHOWCD' in dic and dic['ALWAYSSHOWCD'] == 'T':
            self.show_comment_designator = True

        if 'FONTIDCOUNT' in dic:
            for i in range(1, int(dic['FONTIDCOUNT']) + 1):
                self.fonts.append(self.Font(dic, i))

        for key in dic:
            if key.find("LIBREF") == 0:
                libraries.append(dic[key][:31])
        return libraries

    def parse_libraries(self, libraries):
        for name in libraries:
            data = DataPack(self._ole.openstream([name, 'Data']).read())
            self.libraries.append(self.Library(data))
        self.libraries.sort(key=lambda x: x.name)

    class Library:
        def __init__(self, data):
            self.name = ""
            self.part_count = 0
            self.display_mode_count = 0
            self.designator = None
            self.parameters = []
            self.pins = []
            self.primitives = []
            self.text_labels = []

            while len(data) > 0:
                parse_primitive(self, data.subrecord())

            self.pins.sort()

    class Font:
        def __init__(self, dict, id):
            self.id = id
            self.size = int(dict['SIZE' + str(id)])
            self.font_name = dict['FONTNAME' + str(id)]

            if 'ROTATION' + str(id) in dict:
                self.rotation = int(dict['ROTATION' + str(id)])
            else:
                self.rotation = 0

            if 'BOLD' + str(id) in dict:
                self.bold = True
            else:
                self.bold = False

            if 'UNDERLINE' + str(id) in dict:
                self.underline = True
            else:
                self.underline = False

            if 'ITALIC' + str(id) in dict:
                self.italic = True
            else:
                self.italic = False

            if 'STRIKEOUT' + str(id) in dict:
                self.strikeout = True
            else:
                self.strikeout = False
