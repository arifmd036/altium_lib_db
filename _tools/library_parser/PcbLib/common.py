#
# accept string, without preceeding 4 bytes of string length
#  |a=b|c=d|0x00 ...(trailing bytes ignored)
#
# return dictionary
#  { "a": "b", "c": "d" } 
#
def parseKeyValueString(s):
    s = s.decode('windows-1250')
    properties = s.strip('|').split('|')
    result = {}

    for prop in properties:
        x = prop.split('=')
        key = x[0]
        if len(x) > 1:
            value = x[1]
        else:
            value = ""
        result[key] = value

    return result
