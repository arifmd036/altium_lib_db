from .BinarySubRecord import *
from .common import parseKeyValueString


class Region:
    def __init__(self, data):
        data = data.subrecord()
        self.common = SubRecord_Common(data)

        data.check(5*b'\x00')

        properties = data.subrecord()
        self.dict = parseKeyValueString(properties.read_bytes(len(properties)-1))
        properties.check(b'\x00')
        assert len(properties) == 0
        self.layer_name = self.dict['V7_LAYER']

        nr_of_vertices = data.read_U32()
        self.vertices = []
        for _ in range(nr_of_vertices):
            x = data.read_Float64() / 10000.
            y = data.read_Float64() / 10000.
            self.vertices.append((x, y))

        assert len(data.data) == 0