import math
from .BinarySubRecord import *


class SubRecord_Arc:
    def __init__(self, data):
        self.common = SubRecord_Common(data)

        self.X = data.read_I32() / 10000.
        self.Y = data.read_I32() / 10000.
        self.Radius = data.read_I32() / 10000.
        self.StartAngle = data.read_Float64()
        self.EndAngle = data.read_Float64()
        self.width = data.read_I32() / 10000.
        data.skip(7)
        self.layer_id = data.read_U32()

        # calculate start end end points
        start_x = self.X
        start_y = -self.Y
        radius = self.Radius

        degree0 = self.StartAngle
        degree1 = self.EndAngle

        radians0 = math.radians(degree0)
        radians1 = math.radians(degree1)
        dx0 = radius * (math.sin(radians0))
        dy0 = radius * (math.cos(radians0))
        dx1 = radius * (math.sin(radians1))
        dy1 = radius * (math.cos(radians1))

        self.X1 = start_x + dy0
        self.Y1 = -(start_y - dx0)
        self.X2 = start_x + dy1
        self.Y2 = -(start_y - dx1)



class Arc:
    def __init__(self, data):
        SubRecord_Arc.__init__(self, data.subrecord())

    def __str__(self):
        return "@({}, {})".format(self.X, self.Y)
