from .BinarySubRecord import *


class SubRecord_Text:
    def __init__(self, data):
        self.common = SubRecord_Common(data)

        self.X = data.read_I32() / 10000.
        self.Y = data.read_I32() / 10000.
        self.Height = data.read_I32() / 10000.

        # Something about Stroke Font Number here
        data.skip(1)

        data.check(b'\x00')

        self.Rotation = data.read_Float64()
        self.Mirrored = data.read_U8()
        self.width = data.read_I32()

        data.skip(3)

        self.font_type = data.read_U8()
        self.font_bold = data.read_U8()
        self.font_italic = data.read_U8()
        self.font_name = ""

        for i in range(0, 99, 2):
            one_byte = data.read_U8()
            data.read_U8()
            if one_byte != 0:
                self.font_name += chr(one_byte)
            else:
                break

        font_name_length = len(self.font_name)
        data.skip(99 - (font_name_length * 2) - 2)
        self.barcode_LR_margin = data.read_I32() / 10000
        self.barcode_TB_margin = data.read_I32() / 10000

        self.barcode_min_width = data.read_I32() / 10000
        self.barcode_type = data.read_U8()  # 0 - Code 29, 1 - Code 128
        self.barcode_width_type = data.read_U8()  # 0 - Min Single Bar Width, 1 - Full BarCode Width
        self.inverted = bool(data.read_U8())

        data.skip(1)

        raw_byte_name = data.read_bytes(64)
        self.barcode_font_name = ""
        for i in range(0, 64, 2):
            if raw_byte_name[i] != 0:
                self.barcode_font_name += chr(raw_byte_name[i])
            else:
                break

        self.barcode_show_text = data.read_U8()

        self.layer_id = data.read_U32()


class Text:
    def __init__(self, data):
        SubRecord_Text.__init__(self, data.subrecord())
        self.text = SubRecord_String(data.subrecord())

    def __str__(self):
        return self.text + " @({}, {})".format(self.X, self.Y)