import sys, os, csv, re
from glob import glob


class Part:
    def __init__(self, row, category):
        self.row = row

        self.part_number = row['Part Number']
        self.schlib = row['Library Ref']
        self.pcblib = row['Footprint Ref']
        self.datasheet = row['HelpURL']

        self.supplier_part_number = 6*[None]
        self.supplier_part_number[0] = row['Supplier Part Number 1']
        self.supplier_part_number[1] = row['Supplier Part Number 2']
        self.supplier_part_number[2] = row['Supplier Part Number 3']
        self.supplier_part_number[3] = row['Supplier Part Number 4']
        self.supplier_part_number[4] = row['Supplier Part Number 5']
        self.supplier_part_number[5] = row['Supplier Part Number 6']

        self.help_url = row['HelpURL']
        self.sch_footprint = row['SCH_Footprint']

        self.manufacturer = row['Manufacturer']
        self.manufacturer_part_number = row['Manufacturer Part Number']

        self.parameters = {'category': category}

        if category == 'Resistors':
            self.parameters['Voltage'] = row['Voltage']
            self.parameters['Power'] = row['Power']
            self.parameters['Value'] = row['Value']
            self.parameters['Tolerance'] = row['Tolerance']

        if category == 'Capacitors':
            self.parameters['Voltage'] = row['Voltage']
            self.parameters['Value'] = row['Value']
            self.parameters['Dielectric'] = row['Dielectric']
            self.parameters['Tolerance'] = row['Tolerance (%)']

        if category == 'Inductors':
            self.parameters['Value'] = row['Value']


class AltiumDatabase:
    def __init__(self):
        files = glob(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  os.pardir, os.pardir, os.pardir, '_export', '[a-zA-Z]*.csv'))
        self.database = set()
        self.parts = dict()
        self.files = dict()

        for file in files:
            category = os.path.split(file)[1].replace('.csv', '')
            with open(file, 'r') as csvfile:
                try:
                    filename = os.path.splitext(os.path.basename(file))[0]
                    reader = csv.DictReader(csvfile)
                    self.files[filename] = []
                    for row in reader:
                        self.files[filename].append(row)
                        p = Part(row, category)
                        self.database.add(p)
                        self.parts[p.part_number] = p
                except:
                    print("Error during parsing file (check for unprintable characters): " + file)
                    raise


if __name__ == '__main__':
    _altium_database_class = AltiumDatabase()
    database = _altium_database_class.database
    altium_map = {}
    for i in database:
        altium_map[i.part_number] = i

    for part in altium_map:
        print(altium_map[part].parameters)

