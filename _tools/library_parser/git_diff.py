#!/usr/bin/env python
import sys
import os

from pprint import pprint
from termcolor import colored

from PcbLib.PcbLib import PcbLib
from SchLib.SchLib import SchLib
from deepdiff import DeepDiff


def diff_schlib(old, new):
    old = SchLib(old)
    new = SchLib(new)
    return DeepDiff(old, new, verbose_level=0, exclude_paths={"root._ole", "root.filename"}, ignore_order=True)


def diff_pcblib(old, new):
    old = PcbLib(old)
    new = PcbLib(new)
    return DeepDiff(old, new, verbose_level=0, exclude_paths={"root.OleFile", "root.filename", 'root.Properties'}, ignore_order=True)


if __name__ == '__main__':
    if len(sys.argv) == 10: #when rename
        print(colored(sys.argv[-1], 'cyan'))

    if len(sys.argv) == 8:
        OLD  = sys.argv[2]
        NEW = sys.argv[5]
        
        if NEW == "nul" or NEW == '/dev/null':
            print()
            print(colored("deleted: a/" + sys.argv[1], 'red'))
            print()
            exit(0)
        elif OLD == 'nul' or OLD == '/dev/null':
            print()
            print(colored("added: a/" + sys.argv[1], 'green'))
            print()
            exit(0)
        else:
            print(colored("--- a/" + sys.argv[1], 'red'))
            print(colored("+++ b/" + sys.argv[1], 'red'))

            if OLD.lower().find(".schlib") > 0:
                pprint(diff_schlib(OLD, NEW))
            elif OLD.lower().find(".pcblib") > 0:
                pprint(diff_pcblib(OLD, NEW))
            else:
                cmd = ['diff', OLD, NEW]
                os.system(' '.join(cmd))
