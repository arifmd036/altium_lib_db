from unittest import TestCase
from pprint import pprint
from PcbLib.PcbLib import PcbLib

import parse
from PcbLib.PcbLib_Body3D import Body3D
from PcbLib.PcbLib_Track import Track
from PcbLib.PcbLib_Pad import Pad
from PcbLib.PcbLib_Text import Text
from PcbLib.PcbLib_Arc import Arc

from for_all_elements import for_all_elements


for_all_pcblibs = for_all_elements(parse.pcblibs, lambda x: x.filename)


class TestPcbLibs(TestCase):
    @for_all_pcblibs
    def test_one_file_in_pcblib(self, f):
        self.assertEqual(1, len(f.Footprints))

    @for_all_pcblibs
    def test_footprint_name_same_as_filename(self, f):
        self.assertEqual(f.filename, f.Footprints[0].name)

    @for_all_pcblibs
    def test_no_duplicated_pads_designators(self, pcblib):
        for footprint in pcblib.Footprints:
            already = set()            
            for r in footprint.Pad:
                name = r.Designator
                if name == "DNC" or name[0:2] == "MH" or name == "GND" or name == "SHLD" or name[0:2] == "EP":
                    continue
                self.assertNotIn(name, already)
                already.add(name)

    @for_all_pcblibs
    def test_correct_layer_names(self, pcblib):
        self.assertIn('Top Layer', pcblib.layers)
        self.assertIn('Bottom Layer', pcblib.layers)
        self.assertIn('Top Paste', pcblib.layers)
        self.assertIn('Bottom Paste', pcblib.layers)
        self.assertIn('Top Solder', pcblib.layers)
        self.assertIn('Bottom Solder', pcblib.layers)
        self.assertIn('Top Overlay', pcblib.layers)
        self.assertIn('Bottom Overlay', pcblib.layers)
        self.assertIn('Multi-Layer', pcblib.layers)
        self.assertIn('Drill Guide', pcblib.layers)
        self.assertIn('Keep-Out Layer', pcblib.layers)
        self.assertIn('Drill Drawing', pcblib.layers)
        self.assertIn('Mechanical 13', pcblib.layers)
        self.assertIn('Mechanical 15', pcblib.layers)

    @for_all_pcblibs
    def test_pads_on_top_or_multi_layer(self, pcblib):
        for footprint in pcblib.Footprints:
            for r in footprint.Pad:
                id = r.SizeAndShape.layer_id
                if 'Top Layer' not in pcblib.layers or 'Bottom Layer' not in pcblib.layers or 'Multi-Layer' not in pcblib.layers:
                    continue

                self.assertEqual((id != pcblib.layers['Top Layer']) and (id != pcblib.layers['Bottom Layer']) and
                                 (id != pcblib.layers['Multi-Layer']), False,
                                 "Pad on wrong layer " + pcblib.filename + " -> " + pcblib.layers_by_id[id])

    def check_only_allowed_features_on_layer(self, pcblib, layer, allowed):
        if layer not in pcblib.layers:
            # layer does not exist in file
            return

        expected_layer_id = pcblib.layers[layer]
        for footprint in pcblib.Footprints:            
            for i in footprint.all:
                if type(i) not in allowed and i.layer_id == expected_layer_id:
                    self.fail("Unexpected " + str(type(i)) + " on layer " + layer)

    @for_all_pcblibs
    def test_empty_layers(self, pcblib):
        layers_to_be_empty = [
            '0', '1', '4', '5', '6', '7', '8', '9',
            '10', '11', '12', '14', '16', '17', '18', '19'
        ]

        for l in layers_to_be_empty:
            layer = 'Mechanical ' + l
            self.check_only_allowed_features_on_layer(pcblib, layer, [])

    @for_all_pcblibs
    def test_layer_13_only_tracks_and_3d_models(self, pcblib):
        self.check_only_allowed_features_on_layer(pcblib, 'Mechanical 13', [Track, Body3D, Arc])

    @for_all_pcblibs
    def test_layer_15_only_tracks(self, pcblib):
        self.check_only_allowed_features_on_layer(pcblib, 'Mechanical 15', [Track, Arc])

    @for_all_pcblibs
    def test_no_empty_names(self, pcblib):
        for footprint in pcblib.Footprints:
            self.assertNotEqual("", footprint.name)

    @for_all_pcblibs
    def test_3d_model_on_layer_13(self, pcblib):
        for footprint in pcblib.Footprints:
            x = footprint.Body3D
            for body3d in x:
                self.assertEqual('MECHANICAL13', body3d.layer_name)

    @for_all_pcblibs
    def test_correct_name_for_typical_packages(self, pcblib):
        for footprint in pcblib.Footprints:
            n = footprint.name

            def test_format(text, prefix):
                if n.startswith(prefix):
                    self.assertRegex(text, prefix + "\\d+_[\\d\\.]+x[\\d\\.]+(_EP)?$")

            test_format(n, "DFN")
            test_format(n, "LGA")
            test_format(n, "LQFP")
            test_format(n, "QFN")
            test_format(n, "TQFP")
            test_format(n, "TSSOP")
            test_format(n, "VQFN")
            test_format(n, "VSON")
            test_format(n, "VSSOP")
            test_format(n, "WQFN")
            test_format(n, "WSON")

    @for_all_pcblibs
    def test_pcblib_designator_name(self, pcblib):
        exposed_pads = []

        for footprint in pcblib.Footprints:
            for r in footprint.Pad:
                self.assertRegex(r.Designator, "^(EP[1-9]|SHLD|MH\\d?|\\d+|[A-Z]+\\d+|DNC|GND|IN|OUT)$")
                if r.Designator.find('EP') != -1:
                    exposed_pads.append(int(r.Designator[2:]))
        
        exposed_pads = list(set(sorted(exposed_pads)))
        for i in range(0, len(exposed_pads)):
            self.assertEqual(i+1, exposed_pads[i], "Exposed pads not numbered correctly (on pad EP{})".format(exposed_pads[i]))

    @for_all_pcblibs
    def test_no_jumpers(self, pcblib):
        for footprint in pcblib.Footprints:
            for p in footprint.Pad:
                self.assertEqual(0, p.SizeAndShape.jumper, "jumper on pin {}".format(p.Designator))


    @for_all_pcblibs
    def test_overlay_track_to_narrow(self, pcblib):
        for footprint in pcblib.Footprints:
            for t in footprint.Track:
                if t.layer_id == pcblib.layers['Top Overlay'] or t.layer_id == pcblib.layers['Bottom Overlay']:
                    self.assertGreaterEqual(t.width, 6, "Overlay track is too narrow")

    def get_tracks_for_layer(self, pcblib, layer_name):
        tracks = set()

        for footprint in pcblib.Footprints:
            for t in footprint.Track:
                if t.layer_id == pcblib.layers[layer_name]:
                    tracks.add(t)
            for t in footprint.Arc:
                if t.layer_id == pcblib.layers[layer_name]:
                    tracks.add(t)
        return tracks

    def get_layer_15_cross(self, pcblib):
        tracks = self.get_tracks_for_layer(pcblib, 'Mechanical 15')

        # find vertical lines
        vertical_tracks = []
        for t in tracks:
            if t.X1 == t.X2:
                vertical_tracks.append(t)

        # find horizontal lines
        horizontal_tracks = []
        for t in tracks:
            if t.Y1 == t.Y2:
                horizontal_tracks.append(t)
        
        # find crossing lines
        crossing_pairs = []
        crossing = set()
        for v in vertical_tracks:
            for h in horizontal_tracks:
                # v.X1 == v.X2
                # h.Y1 == h.Y2
                if ((h.X1 < v.X1 and v.X1 < h.X2) or (h.X2 < v.X1 and v.X1 < h.X1)) and \
                   ((v.Y1 < h.Y1 and h.Y1 < v.Y2) or (v.Y2 < h.Y1 and h.Y1 < v.Y1)):
                    crossing_pairs.append((h, v))
                    crossing.add(h)
                    crossing.add(v)
        
        return (tracks - crossing, crossing_pairs)

    @for_all_pcblibs
    def test_mech_15_cross(self, pcblib):
        _, crossing_pairs = self.get_layer_15_cross(pcblib)
        if len(crossing_pairs) > 1:
            self.fail("More than one cross?")
        if len(crossing_pairs) == 0:
            self.fail("No cross detected!")

        h, v = crossing_pairs[0]
        self.assertEqual(5, h.width, "Horizontal line on cross is not 5 mils wide")
        self.assertEqual(5, v.width, "Vertical line on cross is not 5 mils wide")

        self.assertEqual(20, h.X2 - h.X1, "Horizontal line on cross is not 20 mils long")
        self.assertEqual(20, v.Y2 - v.Y1, "Vertical line on cross is not 20 mils long")

        self.assertEqual(10, max(h.X1, h.X2), "Cross not on (0,0)")
        self.assertEqual(10, max(v.Y1, v.Y2), "Cross not on (0,0)")
        self.assertEqual(-10, min(h.X1, h.X2), "Cross not on (0,0)")
        self.assertEqual(-10, min(v.Y1, v.Y2), "Cross not on (0,0)")

    def check_width(self, outline, width):
        for t in outline:
            self.assertEqual(width, t.width, f"Track ({t.X1}, {t.Y1}) -> ({t.X2}, {t.Y2}) width is not {width} mils")

    def check_enclosed_outline(self, outline):
        #  make map location -> next locations (graph)
        for t in outline:
            t.X1 = int(10*t.X1 + 0.5)/10
            t.Y1 = int(10*t.Y1 + 0.5)/10
            t.X2 = int(10*t.X2 + 0.5)/10
            t.Y2 = int(10*t.Y2 + 0.5)/10
        graph = {}
        for t in outline:
            # dummy edge with zero length - do not add it
            if t.X1 == t.X2 and t.Y1 == t.Y2:
                continue

            if (t.X1, t.Y1) not in graph:
                graph[(t.X1, t.Y1)] = set()
            graph[(t.X1, t.Y1)].add((t.X2, t.Y2))

            if (t.X2, t.Y2) not in graph:
                graph[(t.X2, t.Y2)] = set()
            graph[(t.X2, t.Y2)].add((t.X1, t.Y1))

        # start location - first point of first line
        self.assertGreater(len(outline), 0, "No outline!")

        while len(graph) > 0:
            start_location = next(iter(graph))
            location = start_location

            vnext = None
            while True:
                if location not in graph:
                    self.fail(f"Dead end at {location}! not closed outline?")
                next_locations = graph[location]

                self.assertGreater(len(next_locations), 0, "No next locations for {}".format(location))
                vnext = next(iter(next_locations))

                # remove path from graph
                graph[location].remove(vnext)
                if len(graph[location]) == 0:
                    del graph[location]

                graph[vnext].remove(location)
                if len(graph[vnext]) == 0:
                    del graph[vnext]

                if vnext == start_location:
                    break

                location = vnext
            
            if vnext != start_location:
                self.fail(f"Path not closed! Last point: {vnext}")

        self.assertEqual(0, len(graph), "Some tracks not in outlines!")
        for l in graph:
            if len(graph[l]) > 0:
                self.fail(f"Unused track {l} -> {next(iter(graph[l]))}!")

    @for_all_pcblibs
    def test_mech_15_outline(self, pcblib):
        #  check if rest of the tracks are closing an outline
        outline, _ = self.get_layer_15_cross(pcblib)
        self.check_enclosed_outline(outline)
        self.check_width(outline, 5)

    @for_all_pcblibs
    def test_mech_13_outline(self, pcblib):
        tracks = self.get_tracks_for_layer(pcblib, 'Mechanical 13')
        self.check_enclosed_outline(tracks)
        self.check_width(tracks, 5)
