#!/usr/bin/python3

import unittest
import sys, os

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, "library_parser"))

# import altium_statistics

from test_PcbLib import *
from test_SchLib import *

from test_pcblibs import *
from test_schlibs import *
from tests_database import *
from tests_parts import *

if __name__ == '__main__':
    unittest.main()
