{..............................................................................}
{ Summary Demo how to create a new symbol in the library                       }
{ Copyright (c) 2004 by Altium Limited                                         }
{..............................................................................}

{..............................................................................}
Procedure CreateALibComponent;
Var
    CurrentLib   : ISch_Lib;
    SchComponent : ISch_Component;
    R            : ISch_Rectangle;
    L            : ISch_Line;
    P1           : ISch_Pin;
    Par          : ISch_Parameter;
    I            : Integer;
    Pins         : Integer;
    PinsIter     : Integer;
    Prefix       : String;
    AView           : IServerDocumentView;
    AServerDocument : IServerDocument;
    LibraryIterator : ISch_Iterator;
    LibComp   : ISch_Lib;
Begin
     If Client = Nil Then Exit;
    // Grab the current document view using the Client's Interface.
    AView := Client.GetCurrentView;

    // Grab the server document which stores views by extracting the ownerdocument field.
    AServerDocument := AView.OwnerDocument;

    If SchServer = Nil Then Exit;
    CurrentLib := SchServer.GetCurrentSchDocument;
    If CurrentLib = Nil Then Exit;

    // Check if the document is a Schematic Libary document first
    If CurrentLib.ObjectID <> eSchLib Then
    Begin
         ShowError('Please open schematic library.');
         Exit;
    End;

    // Define the LibReference and add the component to the library.
    Prefix := 'P';

    For PinsIter := 0 to 21 Do
    Begin
    Pins := 6 + 2*PinsIter;
    //Pins := 60 + 20*PinsIter;

    // Create a library component (a page of the library is created).
    SchComponent := SchServer.SchObjectFactory(eSchComponent, eCreate_Default);
    If SchComponent = Nil Then Exit;


    // Set up parameters for the library component.
    SchComponent.CurrentPartID := 1;
    SchComponent.DisplayMode   := 0;



    SchComponent.LibReference := Prefix + '_IDC_2x' + inttostr(Pins/2);

    // Create a rectangle object for the new library component.
    R := SchServer.SchObjectFactory(eRectangle, eCreate_Default);
    If R = Nil Then Exit;

    // Define the rectangle parameters.
    R.LineWidth := eSmall;
    R.Location  := Point(MilsToCoord(0), MilsToCoord(0));
    R.Corner    := Point(MilsToCoord(400), MilsToCoord(-Pins*50-100));
    R.LineWidth := eZeroSize;
    R.IsSolid   := True;
    R.OwnerPartId          := CurrentLib.CurrentSchComponent.CurrentPartID;
    R.OwnerPartDisplayMode := CurrentLib.CurrentSchComponent.DisplayMode;

    SchComponent.AddSchObject(R);

    // Create a rectangle object for the new library component.
    L := SchServer.SchObjectFactory(eLine, eCreate_Default);
    If L = Nil Then Exit;

    // Define the rectangle parameters.
    L.LineWidth := eLarge;
    L.Location  := Point(MilsToCoord(0), MilsToCoord(-Pins*25 + 25));
    L.Corner    := Point(MilsToCoord(0), MilsToCoord(-Pins*25-100 - 25));
    L.Color := 000000;
    L.OwnerPartId          := CurrentLib.CurrentSchComponent.CurrentPartID;
    L.OwnerPartDisplayMode := CurrentLib.CurrentSchComponent.DisplayMode;

    SchComponent.AddSchObject(L);

    I := 1;
    while I < Pins Do
    Begin
    // Create two pin o    bjects for the new library component.

        P1 := SchServer.SchObjectFactory(ePin, eCreate_Default);
        If P1 = Nil Then Exit;

        // Define the pin parameters.
        P1.Location             := Point(MilsToCoord(0), MilsToCoord(-I*50-50));
        P1.Orientation          := eRotate180;
        P1.Designator           := inttostr(I);
        P1.Name                 := inttostr(I);
        P1.OwnerPartId          := CurrentLib.CurrentSchComponent.CurrentPartID;
        P1.OwnerPartDisplayMode := CurrentLib.CurrentSchComponent.DisplayMode;
        P1.SetState_ShowDesignator(False);
        P1.SetState_PinLength(2000000);

        SchComponent.AddSchObject(P1);

        P1 := SchServer.SchObjectFactory(ePin, eCreate_Default);
        If P1 = Nil Then Exit;

        // Define the pin parameters.
        P1.Location             := Point(MilsToCoord(400), MilsToCoord(-I*50-50));
        P1.Orientation          := eRotate0;
        P1.Designator           := inttostr(I+1);
        P1.Name                 := inttostr(I+1);
        P1.OwnerPartId          := CurrentLib.CurrentSchComponent.CurrentPartID;
        P1.OwnerPartDisplayMode := CurrentLib.CurrentSchComponent.DisplayMode;
        P1.SetState_ShowDesignator(False);
        P1.SetState_PinLength(2000000);

        SchComponent.AddSchObject(P1);

        I := I+2;
    End;

    Par := SchServer.SchObjectFactory(eParameter, eCreate_Default);
    If Par = Nil Then Exit;

    Par.Name := 'SCH_Footprint';
    Par.Autoposition := False;
    Par.MoveToXY(0, 10000*(-Pins*50-200));
    SchComponent.AddSchObject(Par);

    Par := SchServer.SchObjectFactory(eParameter, eCreate_Default);
    If Par = Nil Then Exit;

    Par.Name := 'Value';
    Par.Autoposition := False;
    Par.MoveToXY(0, 10000*(-Pins*50-300));
    SchComponent.AddSchObject(Par);


    SchComponent.Designator.Text      := Prefix + '?';
    SchComponent.Designator.Autoposition := False;
    SchComponent.Designator.SetState_IsHidden := False;
    SchComponent.Designator.MoveToXY(0, 0);
    SchComponent.Comment.IsHidden := True;
    SchComponent.ComponentDescription := 'IDC Header';

    LibraryIterator := CurrentLib.SchLibIterator_Create;
    LibraryIterator.AddFilter_ObjectSet(MkSet(eSchComponent));
    Try
       LibComp := LibraryIterator.FirstSchObject;
       While LibComp <> Nil Do
       Begin
            CurrentLib.RemoveSchComponent(LibComp);
            LibComp := LibraryIterator.NextSchObject;
       End;
    Finally
           CurrentLib.SchIterator_Destroy(LibraryIterator);
    End;


    CurrentLib.AddSchComponent(SchComponent);


    LibraryIterator := CurrentLib.SchLibIterator_Create;
    LibraryIterator.AddFilter_ObjectSet(MkSet(eSchComponent));
    Try
       LibComp := LibraryIterator.FirstSchObject;
       While LibComp <> Nil Do
       Begin
            if LibComp.LibReference <> SchComponent.LibReference Then
            Begin
            //Change the LibComp parameters here
                     CurrentLib.RemoveSchComponent(LibComp);
            End;
            LibComp := LibraryIterator.NextSchObject;
       End;
    Finally
           CurrentLib.SchIterator_Destroy(LibraryIterator);
    End;


    // Send a system notification that a new component has been added to the library.
    SchServer.RobotManager.SendMessage(nil, c_BroadCast, SCHM_PrimitiveRegistration, SCHComponent.I_ObjectAddress);
    CurrentLib.CurrentSchComponent := SchComponent;

    // Refresh library.
    CurrentLib.GraphicallyInvalidate;

    AServerDocument.SetFileName('D:\out\' + SchComponent.LibReference + '.SchLib');
    AServerDocument.DoFileSave('SchLib');

    End;
End;
{..............................................................................}

{..............................................................................}
